<?php
/**
 * APP
 */

$config ['latest_versions'] = array (
	'android' => array (
		'latest_version' => '1.0.0',
		'whats_new_short' => '',
		'whats_new' => '</p>！上线啦<p>',
		'must_update' => '< 1.0.0', // 填写true/false，或者填写条件，例如 '< 2.3.0'
		'url' => '', //官网自主渠道的下载地址
	),
	'ios' => array (
		'latest_version' => '1.0.0',
		'whats_new_short' => '',
		'whats_new' => '</p>！上线啦<p>',
		'must_update' => '< 1.0.0', // 填写true/false，或者填写条件，例如 '< 2.3.0'
		'url' => 'https://itunes.apple.com/cn/app/shi-ke-lian-meng-wang/id1541126558'
	)
);
