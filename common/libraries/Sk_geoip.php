<?php
/**
 *  GeoIP
 *
 */
class Sk_geoip
{
    public $error;

    protected $_method = 'lc';
    protected $_baidu_ak = 'j4W9qQ9HR6pBTrRatoekFwlz';

    protected $_ip;
    protected $_ip_num;

    protected $_provinces;
    protected $_cities;

    protected $_db;

    public function __construct ()
    {
        $method = config_item ( 'geoip_method' );
        if ( $method ) {
            $this->_method = $method;
        }
        else if ( ! in_array ( $this->_method, array ('lc', 'taobao', 'baidu',))) {
            trigger_error ( 'invalid geoip method specified.' );
            exit;
        }
        if ( $this->_method == 'baidu' ) {
            $baidu_ak = config_item ( 'baidu_ak' );
            if ( $baidu_ak ) {
                $this->_baidu_ak = $baidu_ak;
            }
            if ( ! $this->_baidu_ak ) {
                trigger_error ( 'baidu ak is not configured.' );
                exit;
            }
        }
    }

    /**
     * 读取省市列表
     * @return void
     */
    protected function _read_provinces_cities ( $use_cache = TRUE )
    {
        if ( $use_cache && $this->_provinces && $this->_cities ) {
            return;
        }
        // 读取省市列表
        if ( $use_cache ) {
            $this->_provinces = cache ( 'geo_provinces' );
            $this->_cities = cache ( 'geo_cities' );
        }

        if ( ! $use_cache || ! $this->_provinces ) {
            $this->_db or $this->_db = get_instance ()->load->database ( 'slave', TRUE );
            $provinces = $this->_db->select ( 'id, name' )->get ( 'common_geo_province' )->result_array ();
            if ( $provinces ) {
                $this->_provinces = array_column ( $provinces, NULL, 'id' );
                //名称到ID的映射
                $this->_provinces ['name_to_id'] = array_column ( $provinces, 'id', 'name' );
                cache ( 'geo_provinces', $this->_provinces, 86400 );
            }
        }

        if ( ! $use_cache || ! $this->_cities ) {
            $this->_db or $this->_db = get_instance ()->load->database ( 'slave', TRUE );
            $cities = $this->_db->select ( 'id, name, province_id' )->get ( 'common_geo_city' )->result_array ();
            if ( $cities ) {
                $this->_cities = array_column ( $cities, NULL, 'id' );
                $this->_cities ['name_to_id'] = array_column ( $cities, 'id', 'name' );
                cache ( 'geo_cities', $this->_cities, 86400 );
            }
        }
    }

    /**
     * 获取省列表
     */
    public function get_provinces ()
    {
        $this->_read_provinces_cities (0);
        $provinces = $this->_provinces;
        unset ( $provinces ['name_to_id'] );
        return $provinces;
    }

    /**
     * 获取市列表
     */
    public function get_cities ()
    {
        $this->_read_provinces_cities (0);
        $cities = $this->_cities;
        unset ( $cities ['name_to_id'] );
        return $cities;
    }

    /**
     * 获取一个省
     */
    public function get_province ( $province_id )
    {
        $province_id = ( int ) $province_id;
        $this->_read_provinces_cities ();
        if ( isset ( $this->_provinces [$province_id] )) {
            return $this->_provinces [$province_id];
        }
        return FALSE;
    }

    /**
     * 获取一个市
     */
    public function get_city ( $city_id )
    {
        $city_id = ( int ) $city_id;
        $this->_read_provinces_cities ();
        if ( isset ( $this->_cities [$city_id] )) {
            return $this->_cities [$city_id];
        }
        return FALSE;
    }

    /**
     * 检查省、市是否有效（空为有效）
     * @param int
     * @param int
     * @return bool
     */
    public function check_province_city ( $province_id = 0, $city_id = 0 )
    {
        $this->_read_provinces_cities ();
        if ( $province_id == 0 ) {
            return $city_id == 0;
        }
        if ( ! isset ( $this->_provinces [$province_id] )) {
            return FALSE;
        }
        if ( $city_id != 0 ) {
            if ( ! isset ( $this->_cities [$city_id] )) {
                return FALSE;
            }
            if ( $this->_cities [$city_id]['province_id'] != $province_id ) {
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * 设置错误代码和消息
     *
     * @param string
     * @param string
     * @return void
     */
    protected function _set_error ( $code, $data )
    {
        $this->error = array ( 'code' => $code, 'data' => $data );
    }

    /**
     * 执行数据库查找之前的预检查
     * @param string
     * @return array/bool
     */
    protected function _precheck ()
    {
        // 0.0.0.0
        if ( $this->_ip_num == 0 ) {
            return array (
                'province_id'     => 0,
                'province'     => '',
                'city_id'       => 0,
                'city'       => '',
            );
        }
        // 检查是否是局域网
        if (( 3232235520 <= $this->_ip_num && $this->_ip_num <= 3232301055 ) // 192.168.0.0 - 192.168.255.255
            || ( 2886729728 <= $this->_ip_num && $this->_ip_num <= 2887778303 ) // 172.16.0.0 - 172.31.255.255
            || (167772160 <= $this->_ip_num && $this->_ip_num <= 184549375) // 10.0.0.0 - 10.255.255.255
        ) {
            return array (
                'province_id'     => 0,
                'province'     => '',
                'city_id'       => 0,
                'city'       => '',
            );
        }
        return FALSE;
    }

    /**
     * 获取IP地址的地理信息
     *
     * @param int/string
     * @return array/bool
     */
    public function get_info ( $ip )
    {
        if ( strstr ( $ip, '.' ) === FALSE ) {
            $this->_ip_num = ( float ) $ip;
            $this->_ip = long_ip ( $this->_ip_num );
        }
        else {
            $this->_ip = $ip;
            $this->_ip_num = ( float ) sprintf ( '%u', ip2long ( $ip ));
        }
        if ( $ret = $this->_precheck ()) {
            return $ret;
        }
        $result = $this->{ '_' . $this->_method } ();
        if ( ! $result ) {
            return array (
                'province_id'     => 0,
                'province'     => '',
                'city_id'       => 0,
                'city'       => '',
            );
        }
        return $result;
    }

    /**
     * 用试客数据库查询IP
     * http://ip.taobao.com/
     *
     * @return array/bool
     */
    protected function _shikee () {
        $this->_db or $this->_db = get_instance ()->load->database ( 'slave', TRUE );
        $this->_db->select ( 'province_id, province, city_id, city, end_ip_int' )->where ( 'begin_ip_int <=', $this->_ip_num )->order_by ( 'begin_ip_int', 'DESC')
            ->limit ( 1 );
        $result = $this->_db->get ( 'common_geo_ip' )->row_array ();
        if ( ! $result || $result ['end_ip_int'] < $this->_ip_num ) {
            return FALSE;
        }
        if ( ! $result ['province_id'] ) {
            return FALSE;
        }
        return array (
            'province_id' => $result['province_id'],
            'province' => $result['province'],
            'city_id' => $result['city_id'],
            'city' => $result['city'],
        );
    }

    /**
     * 淘宝IP地址库 REST API
     * http://ip.taobao.com/
     *
     * @return array/bool
     */
    protected function _taobao () {
        /*
        淘宝IP地址库 REST API（每秒10次请求）
        接口说明
        1. 请求接口（GET）：
        http://ip.taobao.com/service/getIpInfo.php?ip=[ip地址字串]

        2. 响应信息：
        （json格式的）国家 、省（自治区或直辖市）、市（县）、运营商

        3. 返回数据格式：
        {"code":0,"data":{"ip":"210.75.225.254","country":"\u4e2d\u56fd","area":"\u534e\u5317",
        "region":"\u5317\u4eac\u5e02","city":"\u5317\u4eac\u5e02","county":"","isp":"\u7535\u4fe1",
        "country_id":"86","area_id":"100000","region_id":"110000","city_id":"110000",
        "county_id":"-1","isp_id":"100017"}}
        其中code的值的含义为，0：成功，1：失败。
        */
        $api = 'http://ip.taobao.com/service/getIpInfo.php?ip=' . urlencode ( $this->_ip );
        if ( ! $data = $this->_get_json ( $api )) {
            return FALSE;
        }
        if ( ! isset ( $data->code ) || $data->code != 0 ) {
            return FALSE;
        }
        $info = $data->data;
        $geo = array (
            'province' => $info->region,
            'city' => $info->city,
        );
        $geo = $this->_fix_names ( $geo );
        return array_merge ( $geo, $this->get_ids ( $geo ));
    }

    /**
     * 百度IP定位API
     * http://developer.baidu.com/map/wiki/index.php?title=webapi/ip-api
     *
     * @return array/bool
     */
    protected function _baidu () {
        /*
        百度IP定位API（需申请ak，每天100万次请求）
        接口参数
        ip 	ip地址 	string 	可选，ip不出现，或者出现且为空字符串的情况下，会使用当前访问者的IP地址作为定位参数
        ak 	用户密钥 	string 	必选，在lbs云官网注册的access key，作为访问的依据
        sn 	用户的权限签名 	string 	可选，若用户所用ak的校验方式为sn校验时该参数必须。（sn生成算法）
        coor 	输出的坐标格式 	string 	可选，coor不出现时，默认为百度墨卡托坐标；coor=bd09ll时，返回为百度经纬度坐标 

        返回结果
        {  
            address: "CN|北京|北京|None|CHINANET|1|None",   #地址  
            content:       #详细内容  
            {  
                address: "北京市",   #简要地址  
                address_detail:      #详细地址信息  
                {  
                    city: "北京市",        #城市  
                    city_code: 131,       #百度城市代码  
                    district: "",           #区县  
                    province: "北京市",   #省份  
                    street: "",            #街道  
                    street_number: ""    #门址  
                },  
                point:               #百度经纬度坐标值  
                {  
                    x: "116.39564504",  
                    y: "39.92998578"  
                }  
            },  
            status: 0     #返回状态码  
        }
        */
        $api = 'http://api.map.baidu.com/location/ip?ak=' . $this->_baidu_ak . '&ip=' . urlencode ( $this->_ip );
        if ( ! $data = $this->_get_json ( $api )) {
            return FALSE;
        }
        if ( ! isset ( $data->status ) || $data->status != 0 ) {
            return FALSE;
        }
        $info = $data->content->address_detail;
        $geo = array (
            'province' => $info->province,
            'city' => $info->city,
        );
        $geo = $this->_fix_names ( $geo );
        return array_merge ( $geo, $this->get_ids ( $geo ));
    }

    /**
     * 修正地理名字以符合数据库规范（使用第三方IP库时需要）
     *
     * @param array
     * @return array
     */
    protected function _fix_names ( $names )
    {
        if ( $names ['province'] ) {
            // 直辖市不带'市'字
            if ( mb_substr ( $names ['province'], -1, 1 ) == '市' ) {
                $names ['province'] = mb_substr ( $names ['province'], 0, mb_strlen ( $names ['province'] ) -1 );
            }
            else {
                switch ( $names ['province'] ) {
                case '广西':
                    $names ['province'] = '广西壮族自治区';
                    break;
                case '新疆':
                    $names ['province'] = '新疆维吾尔自治区';
                    break;
                case '西藏':
                    $names ['province'] = '西藏自治区';
                    break;
                case '内蒙古':
                    $names ['province'] = '内蒙古自治区';
                    break;
                case '宁夏':
                    $names ['province'] = '宁夏回族自治区';
                    break;
                case '香港':
                case '中国香港':
                    $names ['province'] = '香港特别行政区';
                    break;
                case '中国澳门':
                case '澳门':
                    $names ['province'] = '澳门特别行政区';
                    break;
                }
            }
        }
        return $names;
    }

    /**
     * 根据省、市的ID获取名字
     * @param array array ( 'province_id' => 510000, 'city_id' => 510010 );
     * @return array
     */
    public function get_names ( $ids )
    {
        $this->_read_provinces_cities ();
        $result = array ();
        if ( ! empty ( $ids ['province_id'] )) {
            if ( isset ( $this->_provinces [$ids ['province_id']] )) {
                $result ['province'] = $this->_provinces [$ids ['province_id']]['name'];
            }
        }
        if ( ! empty ( $ids ['city_id'] )) {
            if ( isset ( $this->_cities [$ids ['city_id']] )) {
                $result ['city'] = $this->_cities [$ids ['city_id']]['name'];
            }
        }

        isset ( $result ['province'] ) or $result ['province'] = '';
        isset ( $result ['city'] ) or $result ['city'] = '';
        
        return $result;
    }

    /**
     * 根据省、市的名字获取ID（使用第三方IP库时需要）
     * @param array array ( 'province' => '广东省', 'city' => '深圳市' );
     * @return array
     */
    public function get_ids ( $names )
    {
        $this->_read_provinces_cities ();
        $result = array ();
        if ( ! empty ( $names ['province'] )) {
            if ( isset ( $this->_provinces ['name_to_id'][$names ['province']] )) {
                $result ['province_id'] = $this->_provinces ['name_to_id'][$names ['province']];
            }
        }
        if ( ! empty ( $names ['city'] )) {
            if ( isset ( $this->_cities ['name_to_id'][$names ['city']] )) {
                $result ['city_id'] = $this->_cities ['name_to_id'][$names ['city']];
            }
        }

        isset ( $result ['province_id'] ) or $result ['province_id'] = 0;
        isset ( $result ['city_id'] ) or $result ['city_id'] = 0;
        
        return $result;
    }

    /**
    * HTTP请求获取JSON
    *
    * @param string
    * @param bool
    * @return object/bool
     */
    protected function _get_json ( $url, $retry = TRUE )
    {
        $result = file_get_contents ( $url );
        if ( $result === FALSE || ! $object = json_decode ( $result )) {
            if ( ! $retry ) {
                $this->_set_error ( 'NETWORK_ERROR', '网络错误' );
                return FALSE;
            }
            $result = file_get_contents ( $url );
            if ( $result === FALSE || ! $object = json_decode ( $result )) {
                $this->_set_error ( 'NETWORK_ERROR', '网络错误' );
                return FALSE;
            }
        }
        return $object;
    }
}
