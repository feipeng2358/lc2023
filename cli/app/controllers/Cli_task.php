<?php


define('SRCPATH', PROJECT_ROOT.'/');

/**
 * CLI 任务程序
 * blue
 * 2019 9
 */
class Cli_task extends Cli_controller {

    const DB_WAIT_TIMEOUT = 18; // 同mysql默认8小时
    public function __construct()
    {
		parent::__construct ();
		$this->load->database ();
		set_time_limit(0);

    }

    //回收内存
    function _oom(){
        echo 'Mem:'.round(memory_get_peak_usage()/1024/1024,2), "M\n";
        gc_collect_cycles();
    }

}
