<?php 
/**
 * redis分布式锁
 * @author shenpeiliang
 * 20171016
 */
class Sk_redis_locks{
	/**
	 * 错误信息
	 * @var unknown
	 */
	public $error;

	/**
	 * 锁名前缀
	 * @var unknown
	 */
	protected $prefix_lock = 'lock_';
	
	/**
	 * CI操作对象
	 * @var unknown
	 */
	protected $ci = NULL;

	/**
	 * Redis对象
	 * @var object
	 */
	public $redis = NULL;

	/**
	 * 初始化
	 * @param unknown $config
	 */
	public function __construct(){
	    // 加载key前缀
		$this->ci = &get_instance();
		$this->ci->config->load('redis');
		$config = $this->ci->config->item ( 'default' );
	}

	/**
	 * 获得Redis
     *  递延redis连接
	 */
	public function get_redis(){
	    // 如果未连接，则连接
	    if($this->redis === NULL) {
            $this->ci->load->library('Sk_redis');
            $this->redis = $this->ci->sk_redis->connect();
            if (!($this->redis instanceof Redis)) {
                $this->error = array(
                    'code' => 'CONNETC_REDIS_ERR',
                    'data' => '连接redis错误'
                );
                $this->redis = FALSE;
            }
        }
        return $this->redis;
	}

	/**
	 * 加锁
	 *
	 * @param string $lock_key 锁的键
     * @param int $lock_time 锁住时间，单位秒
	 * @return boolean
	 */
	public function lock($lock_key, $lock_time = 5){
        $redis = $this->get_redis();
        $lock_key = $this->prefix_lock . $lock_key;

        // 锁不住直接false
        if(!$redis->setnx($lock_key, 1)){
        	// 处理设置过期时间失败的情况：直接删锁，下一个请求就正常了
            if($redis->ttl($lock_key) === -1){
                $redis->del($lock_key);
            }
            return FALSE;
        }

        // 锁n秒，注：此时可能进程中断，导致设置过期时间失败，则ttl = -1
        $redis->expire($lock_key, $lock_time);
        return TRUE;
	}

	/**
	 * 解锁
	 * @param string $lock_key 锁的键
	 */
	public function unlock($lock_key){
        $redis = $this->get_redis();
        $lock_key = $this->prefix_lock . $lock_key;
        $redis->del($lock_key);
	}

}
?>
