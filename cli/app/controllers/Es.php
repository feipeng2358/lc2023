<?php


use Basemkhirat\Elasticsearch\Connection;
use Illuminate\Support\Arr;

class Es extends Cli_controller
{
	/**
	 * ES object
	 * @var object
	 */
	protected $es;

	protected $ci;

	protected $connection;

	protected $es_config;

	public function __construct()
	{
		parent::__construct();

		$this->ci = &get_instance();

		$this->ci->config->load('es', TRUE);
		$config = $this->ci->config->item('es');

		$this->es_config = $config;

		$this->es = new Connection();
	}

	public function get_client()
	{
		pl('获取配置项：');
		if(!$this->es_config){
			$this->ci->config->load('es', TRUE);
			$config = $this->ci->config->item('es');
			$this->es_config = $config;
		}

		pl('创建客户端实例：');
		try{
			$connection = $this->connection ? $this->connection : $this->es_config["default"];
			$client = $this->es->connection($connection)->raw();
		} catch (\Exception $e) {
			pl('异常：' . $e->getMessage());
		}

		return $client;
	}

	public function create_index()
	{

		pl('设置index属性：');
		$index = $this->_arguments ? $this->_arguments [0] : false;
		$indices = ($index) ?
			[$index] :
			array_keys($this->es_config['indices']);


		$this->do_create_index($indices);

		pl('end');
		exit;
	}

	public function drop_index()
	{
		$client = $this->get_client();
		pl('设置index属性：');

		$index = $this->_arguments ? $this->_arguments [0] : false;

		$indices = ($index) ?
			[$index] :
			array_keys($this->es_config['indices']);

		try {
			foreach ($indices as $index) {
				if (!$client->indices()->exists(['index' => $index])) {
					pl("Index: {$index} is not exist!");
					continue;
				}

				pl("Dropping index: {$index}");
				$client->indices()->delete(['index' => $index]);

			}
		} catch (\Exception $e) {
			pl('异常：' . $e->getMessage());
		}

		pl('end');
		exit;
	}

	public function list_index()
	{
		$client = $this->get_client();
		pl('获取信息');
		$indices = $client->cat()->indices();

		if (is_array($indices)) {
			$indices = $this->getIndicesFromArrayResponse($indices);
		} else {
			$indices = $this->getIndicesFromStringResponse($indices);
		}

		if (count($indices)) {
			$header = [
				"configured (es.php)",
				"health",
				"status",
				"index",
				"uuid",
				"pri",
				"rep",
				"docs.count",
				"docs.deleted",
				"store.size",
				"pri.store.size"
			];
//			pl(explode('|',$header));
			var_dump($indices);

		} else {
			pl('No indices found.');
		}
	}

	/**
	 * Get a list of indices data
	 * Match newer versions of elasticsearch/elasticsearch package (5.1.1 or higher)
	 * @param $indices
	 * @return array
	 */
	function getIndicesFromArrayResponse($indices)
	{
		$data = [];
		foreach ($indices as $row) {
			$data[] = $row;
		}

		return $data;
	}

	/**
	 * Get list of indices data
	 * Match older versions of elasticsearch/elasticsearch package.
	 * @param $indices
	 * @return array
	 */
	function getIndicesFromStringResponse($indices)
	{
		$lines = explode("\n", trim($indices));
		$data = [];
		foreach ($lines as $line) {
			$row = [];
			$line_array = explode(" ", trim($line));
			foreach ($line_array as $item) {
				if (trim($item) != "") {
					$row[] = $item;
				}
			}
			$data[] = $row;
		}
		return $data;
	}


	public function rebuild_index()
	{
		//TODO
	}

	public function update_index()
	{
		$client = $this->get_client();

		pl('设置index属性：');
		$index = $this->_arguments ? $this->_arguments [0] : false;
		$indices = ($index) ?
			[$index] :
			array_keys($this->es_config['indices']);

		try{
			foreach ($indices as $index) {
				$config = $this->es_config["indices"][$index];
				if (!$config) {
					pl("Missing configuration for index: {$index}");
					continue;
				}

				if (!$client->indices()->exists(['index' => $index])) {
					return $this->do_create_index([$index]);
				}
				// The index is already exists. update aliases and setting
				// Remove all index aliases
				pl("Removing aliases for index: {$index}");
				$client->indices()->updateAliases([
					"body" => ['actions' => [['remove' => [
									'index' => $index,
									'alias' => "*"
								]]]],
					'client' => ['ignore' => [404]]
				]);

				if (isset($config['aliases'])) {
					// Update index aliases from config
					foreach ($config['aliases'] as $alias) {
						pl("Creating alias: {$alias} for index: {$index}");
						$client->indices()->updateAliases([
							"body" => ['actions'=>[['add' => [
											'index' => $index,
											'alias' => $alias
										]]]]]);
					}
				}
				if (isset($config['mappings'])) {
					foreach ($config['mappings'] as $type => $mapping) {
						// Create mapping for type from config file
						pl("Creating mapping for type: {$type} in index: {$index}");
						$client->indices()->putMapping([
							'index' => $index,
							'type' => $type,
							'body' => $mapping
						]);
					}
				}
			}
		} catch (\Exception $e) {
			pl('异常：' . $e->getMessage());
		}

		pl('end');
		exit;
	}

	/**
	 * @param array $indices
	 * @param Connection|null $client
	 */
	public function do_create_index(array $indices)
	{
		$client = $this->get_client();
		try {
			foreach ($indices as $index) {
				$config = $this->es_config['indices'][$index];
				if (is_null($config)) {
					pl("Missing configuration for index: {$index}");
					continue;
				}
				if ($client->indices()->exists(['index' => $index])) {
					pl("Index {$index} is already exists!");
					continue;
				}
				// Create index with settings from config file
				pl("Creating index: {$index}");
				$client->indices()->create([
					'index' => $index,
					'body' => ["settings" => $config['settings']]
				]);
				if (isset($config['aliases'])) {
					foreach ($config['aliases'] as $alias) {
						$this->info("Creating alias: {$alias} for index: {$index}");
						$client->indices()->updateAliases([
							"body" => ['actions' => [['add' => [
								'index' => $index,
								'alias' => $alias
							]]]]]);
					}
				}
				if (isset($config['mappings'])) {
					foreach ($config['mappings'] as $type => $mapping) {
						// Create mapping for type from config file
						pl("Creating mapping for type: {$type} in index: {$index}");
						$client->indices()->putMapping([
							'index' => $index,
							'type' => $type,
							'body' => $mapping,
							"include_type_name" => true
						]);
					}
				}
			}
		} catch (\Exception $e) {
			pl('异常：' . $e->getMessage());
		}
		return true;
	}

}

