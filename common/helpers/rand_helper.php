<?php
/**
 * 抽奖相关的随机算法
 */

/**
 * 抽奖
 *   从一个概率数组中，根据概率来抽中某个值
 * @param array $ratios 奖品比率，key是奖品标识，value是中奖概率
 * @return 奖品标识
 */
function array_draw(array $ratios){
    // 汇总奖品概率
    $sum = array_sum($ratios);

    // 随机数
    $rand = mt_rand(1, $sum);

    // 找到随机数对应的奖品
    $curr = 0;
    foreach($ratios as $key => $ratio){
        $curr += $ratio;
        if($rand < $curr)
            return $key;
    }

    // 不可达
    return FALSE;
}
