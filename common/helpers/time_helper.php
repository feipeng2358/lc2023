<?php
/**
 * 验证一个字符串表示的时间是否合法
 * @param  string $time_str  时间字符串 eg：07:00:00
 * @param  [type] $delimiter 时间字符串的时,分,秒数字之间的分隔符
 * @return array   不合法返回false；合法返回('hour'=>'小时数','minute'=>'分钟数','second'=>'秒数')的数组
 */
 function valid_time($time_str,$delimiter){
    $valid_time=preg_match("/^\d{1,2}($delimiter\d{1,2}){0,2}$/",$time_str);
    if(!$valid_time){
    	return false;
    }

    $time_arr=explode($delimiter,$time_str);
    $time=array();
    //时
    if(valid_hour($time_arr[0])){
    	$time['hour']=$time_arr[0];
    }else{
    	return false;
    }
    //分
    if(isset($time_arr['1'])){
    	$time['minute']=valid_minute($time_arr['1'])?$time_arr['1']:0;
    }else{
    	$time['minute']=0;
    }
    //秒
    if(isset($time_arr['2'])){
    	$time['second']=valid_second($time_arr['2'])?$time_arr['2']:0;
    }else{
    	$time['second']=0;
    }

    return $time;
 }

/**
 * 时间辅助函数；用于对时间有效验证
 */
 function valid_hour($hour){
 	if(preg_match('/\d{1,2}/',$hour) && $hour<=24){
 		return true;
 	}

 	return false;
 }

 function valid_minute($minute){
 	if(preg_match('/\d{1,2}/',$minute) && $minute<=59){
 		return true;
 	}

 	return false;
 }

  function valid_second($second){
 	if(preg_match('/\d{1,2}/',$second) && $second<=59){
 		return true;
 	}

 	return false;
 }

/**
 * 获取某天开始时间和结束时间时间戳
 * @param $time 默认当天，可以是日期格式时间
 * @return array
 */
function get_day_time_range($time = NULL){
	//默认当天
	$day = time();

	//日期格式转为时间戳
	if(!is_null($time) && !preg_match('/^([0-9]{4})-([0-9]{2})-([0-9]{2})\s*([0-9]{2}):([0-9]{2}):([0-9]{2})$/', $time)){
		$day = strtotime($time);
	}

	$stime = mktime(0, 0, 0, date('m', $day), date('d', $day), date('Y', $day));
	$etime = mktime(23, 59, 59, date('m', $day), date('d', $day), date('Y', $day));

	return [$stime, $etime];
}

if ( ! function_exists('get_diff_month_num'))
{
	/**获取开始时间和结束时间相差几个月
	 * @param $start_time 开始时间戳
	 * @param $end_time 结束时间戳
	 * @return int
	 */
	function get_diff_month_num($start_time, $end_time){
		if($start_time > $end_time)
			return false;

		$month_num = 0;
		while(true){
			$new_start_time = add_month($start_time, 1, true);
			if($new_start_time  > $end_time)
				break;

			$start_time = $new_start_time;
			$month_num++;
		}

		return $month_num;
	}
}


/**
 * 计算+n个月后的日期
 * @param string|int $start_time 开始日期，字符串或时间戳, 例如2017-11-31
 * @param int $month 增加的月份，比如6
 * @param bool $return_ts 是否返回时间戳
 * @return string|int 返回增加月份的日期
 */
if ( ! function_exists('add_month'))
{
	function add_month($start_time, $month, $return_ts = false){
		if(is_numeric($start_time)){ // 转字符串
			$start_time = date('Y-m-d', $start_time);
		}

		$dt = new DateTime($start_time);
		$day = $dt->format('j');
		// +n月, 但是1号
		$dt->modify('first day of +'.$month.' month');
		// +n日, 最大为本月的最后一日, 如2月是28/29号
		$dt->modify('+' . (min($day, $dt->format('t')) - 1) . ' days');

		$ts = $dt->getTimestamp();
		if ($return_ts){ // 返回时间戳
			return $ts;
		}

		// 返回字符串
		return date('Y-m-d H:i:s',$ts);
	}
}

if ( ! function_exists('get_month_by_days')){
	/**
	 * 给定开始时间和天数获取最近的月数
	 * @param $start_time
	 * @param int $days
	 * @return int
	 */
	function get_month_by_days($start_time, $days = 0){
		//距离最小的月数
		$month = 0;

		//转字符串
		if(is_numeric($start_time)){
			$start_time = date('Y-m-d', $start_time);
		}
		//结束时间
		$end_time = date('Y-m-d', strtotime("{$start_time} +{$days} days"));

		//开始时间的年月日
		list($year_s, $month_s, $day_s) = explode('-', $start_time);

		//结束时间的年月日
		list($year_e, $month_e, $day_e) = explode('-', $end_time);

		if($year_e > $year_s){
			$month = ($year_e - $year_s)*12 + $month_e - $month_s;
		}else{
			$month = $month_e - $month_s;
		}

		//小于一个月的被认为是最小的一个月
		if($month == 0 && $days > 0)
			$month = 1;

		return $month;

	}
}

if ( ! function_exists('format_date_period'))
{

    /**
     * 格式化单个服务的周期时间
     * @param $item
     * @param array $format_fields ['dateline','start_time|Y-m-d',..]
     * @return mixed
     */
    function format_date_period(&$item,  $format_fields = [])
    {
        if ($item) {
            foreach ($format_fields as $value) {
                $format_field = explode('|', $value);

                //数组字段
                $field = $format_field[0];
                //时间格式
                $date_format = isset($format_field[1]) ? $format_field[1] : 'Y-m-d H:i:s';

                if (!empty($item[$field])) {
                    $item[$field] = date($date_format, $item[$field]);
                }
            }
        }
        return $item;
    }
}

if ( ! function_exists('format_date_periods'))
{
    /**
     * 格式化多个周期时间
     * @param $items
     * @param array $format_fields
     * @return mixed
     */
    function format_date_periods($items, $format_fields = ['dateline', 'start_time|Y-m-d', 'end_time|Y-m-d']){
        // 格式化服务的周期时间
        foreach ($items as &$item){
            format_date_period($item, $format_fields);
        }

        return $items;
    }
}