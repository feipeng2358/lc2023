<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model 
{
	private $_db = NULL;
	
	private $_slave_db = NULL;
	
	/**
	 * 获得主库
	 */
	protected function db()
	{
		if (!$this->_db)
		{
			//加载数据库
			$this->_db = $this->load->database('default', TRUE);
		}
		
		return $this->_db;
	}
	
	/**
	 * 获得从库
	 */
	public function slave_db()
	{
		if (!$this->_slave_db)
		{
			//加载数据库
			$this->_slave_db = $this->load->database('slave', TRUE);
		}
		
		return $this->_slave_db;
	}
}