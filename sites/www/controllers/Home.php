<?php

/**
 * 网站首页
 */
class Home extends CI_Controller
{

	/**
	 * 网站首页
	 */
	public function index()
	{
		$data = [];

		$this->load->view('index', $data);
	}

}
