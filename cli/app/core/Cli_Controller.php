<?php
class Cli_Controller extends CI_Controller {
    /**
     * options
     */
    protected $_options = [];
    /**
     * arguments
     */
    protected $_arguments = [];

    /**
     * slave_db
     */
    private $slave_db = NULL;

    public function __construct() {
        parent::__construct ();

        $argc = count($_SERVER['argv']);
        if ($argc > 1) {
            // CI会把命令行参数中的某些字符去掉，放到$URI->rsegments中，所以解析命令行参数时不能直接取$URI->rsegments
            // 而要从$_SERVER['argv']中取
            global $URI;
            $segcount = count($URI->rsegments);
            $offset = $segcount > 2 ? 2 - $segcount : 3;
            $args = array_slice($_SERVER['argv'], $offset);

            // 处理成_options 和_arguments
            $option_name = NULL;
            foreach ($args as $argument) {
                if ($argument [0] === '-') {
                    $option_name = ltrim ( $argument, '-' );
                    $this->_options [$option_name] = TRUE;
                }
                else if ($option_name) {
                    $this->_options [$option_name] = $argument;
                    $option_name = NULL;
                } else {
                    $this->_arguments [] = $argument;
                }
            }
        }
    }

    /**
     * retrive an option by its name
     * 
     * @param string|array $names names
     * @param mixed $default default value when the option is not set
     * @return mixed
     */
    protected function _option($names, $default = FALSE) {
        if (! is_array ( $names )) {
            $names = [$names];
        }

        foreach ($names as $name) {
            if (isset ( $this->_options [$name] )) {
                return $this->_options [$name];
            }
        }

        return $default;
    }

    /**
     * 获得从库
     */
    public function slave_db()
    {
        if (!$this->slave_db)
        {
            //加载数据库
            $this->slave_db = $this->load->database('slave', TRUE);
        }

        return $this->slave_db;
    }
}
