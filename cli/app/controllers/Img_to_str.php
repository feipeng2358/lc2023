<?php
/**
 * Bluehood
 * Date: 2020/10/29
 * Time: 14:04
 */
class Img_to_str extends Cli_controller
{
    public function __construct()
    {
        parent::__construct();
    }

    //调用Ocr库进行图像解析
    public function test (){
        //cli 参数传递图片地址
        $img_name = '';
        if( $this->_arguments ) {
            $this->_arguments [0] ? $img_name = $this->config->item ( 'upload_image_save_path' ).'ocr/'.$this->_arguments [0] : '';
        }

        //ocr库路径
        $ocr_lib_path = '/home/htdocs/PaddleOCR/';

        //解析的图片路径
        $img_src = $img_name =='' ? $this->config->item ( 'upload_image_save_path' ).'ocr/2.jpg' : $this->config->item ( 'upload_image_save_path' ).'/ocr/'.$img_name.'.jpg';


        //orc 参数
        $ocr_params =
            '/usr/local/bin/python3 '.$ocr_lib_path.'tools/infer/predict_system.py'.
            ' --image_dir="'.$img_src.'" '.
            '--det_model_dir="'.$ocr_lib_path.'inference/ch_ppocr_mobile_v1.1_det_infer/" '.
            '--rec_model_dir="'.$ocr_lib_path.'inference/ch_ppocr_mobile_v1.1_rec_infer/" '.
            '--cls_model_dir="'.$ocr_lib_path.'inference/ch_ppocr_mobile_v1.1_cls_infer/" '.
            '--use_angle_cls=True '.
            '--use_space_char=True '.
            '--use_gpu=False';

        //拼接命令
        pl($ocr_params);
        $output = exec(
        //先cd进入目录，否则执行python脚本会报路径错误 , 然后指定lang编码为utf8，避免php接收乱码
            "cd {$ocr_lib_path} ;  export LANG=en_US.UTF-8 ; {$ocr_params}  2>&1",
            $out ,
            $res
        );
        pl('output :');
        var_dump($output);

        pl('out :');
        var_dump($out);

        pl('res :');
        var_dump($res);

        //方式2 试用system命令
//        $output_2 = system(
//            "cd {$ocr_lib_path}; $ocr_params"
//        );
//        var_dump($output_2);
    }

}
?>