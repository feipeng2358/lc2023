<?php
/**
 * 公共模型类
 */
class Pub_model extends CI_Model {

	private $uc_db=NULL;
    private $slave=NULL;

	public function __construct() {
		parent::__construct ();
		$this->load->database ( 'default' );//shikee数据库
        $this->slave = $this->load->database ( 'slave', TRUE );
	}

	public function insert($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    /**
     * 基本查找
     * @param $table
     * @param array $where
     * @param null $fields
     * @param null $order
     * @return bool
     */
    public function get_one($table, $where = array(), $fields = null, $order = null) {
		if ($fields != null) {
			$this->db->select ( $fields );
		}
		if ($order != null) {
			$this->db->order_by ( $order );
		}
		$query = $this->db->get_where ( $table, $where );
		if ($query != false) {
			return $query->row_array ();
		} else {
			return false;
		}
	}
	
	 /**
	  * 基本多条查找
	  * @param  $table
	  * @param  $fields
	  * @param  $where
	  * @param  $order
	  * @param  $limit
	  * @param  $offset
	  * @return boolean
	  */
	public function get_all($table, $fields = null, $where = array(), $order = null, $limit = null, $offset = null) {
		if ($fields != null) {
			$this->db->select ( $fields );
		}
		if ($order != null) {
			$this->db->order_by ( $order );
		}
		if ($limit != null) {
			$query = $this->db->get_where ( $table, $where, $limit, $offset );
		} else {
			$query = $this->db->get_where ( $table, $where );
		}

		if ($query != false) {
			return $query->result_array ();
		} else {
			return false;
		}
	}
	
	 /**
	  * 统计总数
	  * @param  $table
	  * @param  $field
	  * @param  $where
	  * @return number
	  */
	public function count_table_sum($table, $field, $where = NULL) {
		$this->db->select_sum ( $field );
		$query = $this->db->get_where ( $table, $where );
		if ($query != false) {
			$array =  (array) $query->row();
			if ($array [$field] == ''){
				$array [$field] = 0;
			}
			return $array [$field];
		} else {
			return 0;
		}
	}
	
	/**
	 * 统计数量
	 * @param  $table
	 * @param  $where
	 */
	public function count_table($table, $where = null) {
		if ($where != null) $this->db->where ( $where );
		$this->db->from ( $table );
		return $this->db->count_all_results ();
	}

    /**
     * 更新shikee
     * @param $table
     * @param $data
     * @param null $where
     * @return mixed
     */
    public function update($table,$data, $where = null){
		return  $this->db->update($table,$data, $where);
	}

    /**
     * 更新uc
     * @param $table
     * @param $data
     * @param null $where
     * @return mixed
     */
    public function update_uc($table,$data, $where = null){
		return  $this->db->update($table,$data, $where);
	}

    /**
     * 批量更新
     * @param $table
     * @param $data
     * @param $filed
     */
    public function update_batch($table,$data,$filed){
        return $this->db->update_batch($table,$data,$filed);
    }

    /**
     * @param $array
     */
    public function get_all_where_in($table,$col,$array,$where = [],$filed = '*'){
        $this->slave->from($table)->select($filed)->where_in($col,$array);
        if (!empty($where)){
            $this->slave->where($where);
        }
        $query = $this->slave->get();
        if ($query)
            return $query->result_array();
        return false;
    }


    /**
     * js跳转
     * @param $url
     * @param string $alert
     */
    public function _js_href($url, $alert = "") {
		header ( "Content-type: text/html; charset=utf-8" );
		$alerts = "";
		if ($alert !== "") {
			$alerts = 'alert("' . $alert . '");';
		} else {
			$alerts = "";
		}
		exit('<script>' . $alerts . 'window.location.href="' . $url . '"</script>');
	}
	
	
	/**
	 * @name 注销登录
	 */
	public function base_logout() {
		$user = get_user();
		if ($user) {
			$this->uc_db -> where('uid', $user['id']) -> delete('onlineusers');
		}
		setcookie(COOKIE_NAME, '', time() - 3600, '/',COOKIE_DOMAIN);
	}

    /**
     * 检查上传文件
     * @param $ext
     * @param $target
     * @return bool
     */
    function check_img_file($ext,$target){
		if(in_array($ext, array('jpg', 'jpeg', 'gif', 'png', 'bmp')) && function_exists('getimagesize') && !getimagesize($target))
		{
			return true;
		}else{
			return false;
		}
	}


    /**
     * 基本查找:从库读取
     * @param $table
     * @param array $where
     * @param null $fields
     * @param null $order
     * @return bool
     */
    public function slave_get_one($table, $where = array(), $fields = null, $order = null) {
        if ($fields != null) {
            $this->slave->select ( $fields );
        }
        if ($order != null) {
            $this->slave->order_by ( $order );
        }
        $query = $this->slave->get_where ( $table, $where );
        if ($query != false) {
            return $query->row_array ();
        } else {
            return false;
        }
    }

	/**
     * 基本多条查找:从库读取
     * @param $table
     * @param array $where
     * @param null $fields
     * @param null $order
     * @return bool
     */
	public function slave_get_all($table, $fields = null, $where = array(), $order = null, $limit = null, $offset = null) {
		if ($fields != null) {
			$this->slave->select ( $fields );
		}
		if ($order != null) {
			$this->slave->order_by ( $order );
		}
		if ($limit != null) {
			$query = $this->slave->get_where ( $table, $where, $limit, $offset );
		} else {
			$query = $this->slave->get_where ( $table, $where );
		}

		if ($query != false) {
			return $query->result_array ();
		} else {
			return false;
		}
	}

    /**
     * trans
     * @return mixed
     */
    public function trans_begin(){
        return $this->db->trans_begin();
    }
    public function trans_commit(){
        return $this->db->trans_commit();
    }
    public function trans_status(){
        return $this->db->trans_status();
    }
    public function trans_rollback(){
        return $this->db->trans_rollback();
    }
    public function insert_id(){
        return $this->db->insert_id();
    }
    public function last_query(){
        return $this->db->last_query();
    }
}
