<?php
/*
 *全局公共配置
 *
 */

//session配置
$config['sess_driver'] = 'native';

// 各站点域名（若定义其它站点，则格式为：domain_主域名_二级域名）
$config['domain_root']            = 'lc.com'; // 根域名
$config['domain_www']             = 'http://www.lc.com/';
$config['domain_static']          = 'http://static.lc.com/';
$config['domain_admin']           = 'http://xadmin.lc.com/';
$config['domain_api']             = 'http://api.lc.com/';
$config['domain_user']             = 'http://user.lc.com/';

// 图片上传
if(strncasecmp ( PHP_OS, 'WIN', 3 ) == 0)
    $config['upload_image_save_path'] = 'd:/code/img.lc.com/';
else
    $config['upload_image_save_path'] = '/data/htdocs/img.lc.com/';

// 图片服务器
$config['image_servers'] = array('http://img.lc.com/');
