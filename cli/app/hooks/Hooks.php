<?php
function ci_pre_system_hook_global() {
	if (! IS_WIN) return;
	foreach (['SERVER_NAME', 'REQUEST_URI', 'REMOTE_ADDR'] as $item) {
		if (! isset ( $_SERVER [$item] )) {
			$_SERVER [$item] = '';
		}
	}
	foreach ($_SERVER ['argv'] as &$arg) {
        if (SHELL_ENCODING !== 'cp65001') {
            $arg = iconv ( SHELL_ENCODING, 'utf-8', $arg );
        }

	}
}
function ci_post_system_hook_global() {
	return;
}
function ci_display_override_hook_global() {
	if ($output = get_instance()->output->get_output () && $output = rtrim ( strip_tags ( $output ), PHP_EOL )) {
		echo $output . PHP_EOL;
	}
}
