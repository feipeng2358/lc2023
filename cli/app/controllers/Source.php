<?php
class Source extends Cli_controller {
	protected $_static_root;
	protected $_static_root_relative;
	public function __construct() {
		parent::__construct ();
		$this->_static_root_relative = 'sites/static';
		$this->_static_root = PROJECT_ROOT . '/' . $this->_static_root_relative;
		$this->load->library ( 'git' );
	}

    /**
     * 检测含有 UTF-8 BOM 的文件并打印文件名
     */
	public function detect_bom()
	{
		dir_walk ( '.', function ($path, $is_dir) {
			 if (has_bom ( $path ) ) pl ( $path );
		}, TRUE, ['libs'], '/\.(php|js|css|less|html|htm)$/', TRUE );
	}

	protected function _get_single_file_version_with_git($path) {
		return $this->git->last_commit_of_file ( $path );
	}

    protected function _get_single_file_version_with_sha1($path) {
        $sha1 = sha1(file_get_contents($path));
        return $sha1;
    }

    protected function _get_single_file_version_with_time() {
	    static $version;
	    if (! $version) {
	        $version = sha1(microtime(true));
        }
        return $version;
    }

    protected function _get_asset_files() {
        $offset = strlen ( $this->_static_root ) + 1;
        $static_root = $this->_static_root;

        $files = array();
        dir_walk (
            $this->_static_root,
            function ($path, $is_dir) use ($offset, &$files) {
                $files []= substr ( $path, $offset );
            },
            TRUE,
            array('img', 'less'),
            '/(css|js|png|jpg|gif|bmp)$/'
        );

        return $files;
    }

	protected function _get_versions_all($method) {
		$manifest = [];

		foreach ($this->_get_asset_files() as $file) {
            $version = $this->{'_get_single_file_version_with_' . $method} ( $this->_static_root . '/' . $file );
            $manifest [$file] = $version;
        }
		return $manifest;
	}

	private function _get_versions_with_git($old_versions, $show_changed) {
        $current_version = $this->_get_single_file_version_with_git ( $this->_static_root );

        if ($old_versions) {
            if (! empty ( $old_versions['/'] )) {
                $previous_version = $old_versions['/'];
                if ($current_version === $previous_version) {
                    pl ( '自从上次运行该命令以来，没有文件被更改' );
                    return true;
                }
                $changed_files = $this->git->changed_files_of_commit_range ( $previous_version, $current_version, $this->_static_root );
                if (! $changed_files) {
                    pl ( '自从上次运行该命令以来，没有文件被更改' );
                    return true;
                }
                $new_versions = $old_versions;
                $len = strlen ( $this->_static_root_relative ) + 1;
                foreach ($changed_files as $file) {
                    $file = substr ( $file, $len );
                    $version = $this->_get_single_file_version_with_git ( $file );
                    if ($show_changed) pl($version . "\t" . $file);
                    $new_versions [$file] = $version ? $version : time ();
                }

                if ($show_changed) pl ( '有 ' . count ( $changed_files ) . ' 个文件已发生变更' );
            }
        }

        if (! isset ( $new_versions )) {
            pl ( '正在获取全部文件的版本，可能需要一点时间...' );
            $new_versions = $this->_get_versions_all('git');
        }

        $new_versions ['/'] = $current_version;

        return $new_versions;
    }

    private function _get_versions_with_sha1($old_versions, $show_changed) {
        $new_versions = $this->_get_versions_all('sha1');
        $new_versions['/'] = sha1(join('', $new_versions));
        if ($show_changed) {
            $count = 0;
            foreach ($new_versions as $file => $version) {
                if (!isset($old_versions[$file]) || $old_versions[$file] !== $version) {
                    $count++;
                    pl($version . "\t" . $file);
                }
            }
            pl ( '有 ' . $count . ' 个文件已发生变更' );
        }
        return $new_versions;
    }

    private function _get_versions_with_time($old_versions, $show_changed) {
        $new_versions = $this->_get_versions_all('time');
        $new_versions['/'] = $this->_get_single_file_version_with_time();
        if ($show_changed) {
            $count = 0;
            foreach ($new_versions as $file => $version) {
                if (!isset($old_versions[$file]) || $old_versions[$file] !== $version) {
                    $count++;
                    pl($version . "\t" . $file);
                }
            }
            pl ( '有 ' . $count . ' 个文件已发生变更' );
        }
        return $new_versions;
    }

    /**
     * 更新静态文件版本
     * 选项：
     *     -r, --rebuild
     *         重新获取全部文件的版本
     *     -s, --show-filename
     *         显示变更文件的文件名
     *     -m <git|sha1|time>, --method <git|sha1|time>
     *         指定获取版本号的方法。
     */
	public function version() {
		$rebuild = $this->_option ( ['r', 'rebuild'], FALSE ) !== FALSE;
		$show_changed = $this->_option ( ['s', 'show-changed'], FALSE ) !== FALSE;
		$method = $this->_option(['m', 'method'], 'sha1');
		if (! is_string($method) || ! in_array($method, array('git', 'sha1', 'time'))) {
		    pl("无效选项：--method {$method}");
		    exit(-1);
        }

		$filename = PROJECT_ROOT . '/common/config/' . ENVIRONMENT . '/static_file_versions.php';

        if (! $rebuild && is_file ( $filename )) {
            include($filename);
        }
        $old_method = ! isset($config['method']) ? null : $config['method'];
        $old_versions = $method !== $old_method || empty($config['manifest']) ? array() : $config['manifest'];

        $new_versions = $this->{'_get_versions_with_' . $method}($old_versions, $show_changed);

        // true表示不需要更新
        if ($new_versions === true) {
            exit(0);
        }

		ksort ( $new_versions );

		$file_content = "<?php\n";
		$file_content .= "// 此文件由程序生成，不需手动更改\n";
		$file_content .= '$config ["manifest"] = ' . var_export ( $new_versions, TRUE );
		$file_content .= ";\n";
        $file_content .= '$config ["method"] = ' . var_export ( $method, TRUE );
        $file_content .= ";\n";
		file_put_contents ( $filename, $file_content );
	}
}
