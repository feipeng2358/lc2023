<?php
/**
 *复制文件
 * @param  [type] $source 源文件路径
 * @param  [type] $dest   目标文件路径
 * @return [type]         bool
 */
function copy_file($source,$dest){
	if(!file_exists($source)){
		return false;
	}
	if(!file_exists($dest)){
		$dir=mkdir(dirname($dest),0775,true);
		if(!$dir){
			return false;
		}
		return copy($source,$dest);
	}
}

/**
 * 移动文件
 * @param  [type] $source 源文件路径
 * @param  [type] $dest   目标文件路径
 * @return [type]         bool
 */
function move_file($source,$dest){
	if(!file_exists($source)){
		return false;
	}
	if(!file_exists($dest)){
		$dir=mkdir(dirname($dest),0775,true);
		if(!$dir){
			return false;
		}
		return rename($source,$dest);
	}
}