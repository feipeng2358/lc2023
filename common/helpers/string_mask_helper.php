<?php
/**
 * 字符串遮罩函数
 */

/**
 * 处理邮箱地址遮罩
 */
function mask_email($str) {
	list ( $name, $host ) = array_pad ( explode ( '@', $str ), 2, '' );
	$len = mb_strlen ( $name );
	if ($len >= 6) {
		$start = 2;
		$star_len = $len - 4;
	} else if ($len >= 3) {
		$start = 1;
		$star_len = $len - 2;
	} else {
		$start = $len;
		$star_len = 4;
	}
	return substr_replace ( $name, str_repeat ( '*', 4 ), $start, $star_len ) . '@' . $host;
}

/**
 * 处理手机号码遮罩
 */
function mask_mobile($str) {
	$start = 3;
	$star_len = 4;
	return substr_replace ( $str, str_repeat ( '*', $star_len ), $start, $star_len );
}

/**
 * 用星号隐藏中间部分的用户账号
 * @param string $uname
 * @param int $star_limit 限制星星数
 * @return string
 */
function mask_uname($uname, $star_limit = 0) {
    $len = mb_strlen ( $uname );
    if ($len < 3)
        return str_repeat ( '*', $len );

    if($star_limit == 0)
        $star_limit = $len - 2;

    return mb_substr ( $uname, 0, 1 ) . str_repeat ( '*', $star_limit ) . mb_substr ( $uname, - 1, 1 );
}