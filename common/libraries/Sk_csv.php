<?php

/**
 * A RFC 4180 compliant CSV library
 *
 * @author xwn
 * @link http://tools.ietf.org/html/rfc4180
 */
class Sk_csv
{

    /* config */
    protected $_config;

    /* is header sent? */
    protected $_header_sent = FALSE;

    /**
     * Error
     *
     * @var unknown
     */
    public $error;

    public function __construct()
    {
        // default configuration
        $this->_config = array ( 
            'line_break' => "\r\n", /* 换行符 */
			'quote' => TRUE, /* 每个值是否用双引号括起,若此项为FALSE,则值中的双引号将被去除 */
			'max_length' => 10000, /* 最大行长度 */
            'delimiter' => ',', /* 列分隔符 */
            'enclosure' => '"', /* 字段环绕符 */
			'encoding' => 'UTF-8', /* 文件编码 */
			'trim' => TRUE, /* 是否对每个值进行trim操作 */
			'rows' => NULL, /* 读取行数 */
			'has_header' => NULL, /* 是否有表头行 */
            'columns' => NULL, /* 数据列定义 */
			'header_required' => NULL, /* 表头不存在时报错？ */
			'output_filename' => NULL, /* 下载文件名 */
            'handle_equal' => FALSE, /* 是否去除 单元格值前的=号，仅当单元格值为 ="..." 的形式时处理，此为RFC4180未规范格式  */
		);
    }

    /**
     * Set configuration
     *
     * @chainable
     *
     * @param unknown $encoding            
     */
    public function config($config)
    {
        $this->_config = array_merge ( $this->_config, $config );
        return $this;
    }

    /**
     *
     * @param unknown $str            
     * @return string
     */
    protected function _convert($str)
    {
        // fix bug: iconv() 遇特殊字符则转码失败
        //return iconv ( $this->_config ['encoding'], "UTF-8//TRANSLIT//IGNORE", $str );
        return mb_convert_encoding($str,'UTF-8', $this->_config ['encoding']);
    }

    /**
     *
     * @param unknown $str            
     * @return string
     */
    protected function _handle_cell($str)
    {
        $this->_config ['trim'] and $str = trim ( $str );
        if ($this->_config ['handle_equal'] && ( $len = mb_strlen ( $str ) ) >= 3 ) {
            if (mb_substr ( $str, 0, 2 ) === '="' && mb_substr ( $str, -1, 1 ) === '"') {
                $str = mb_substr ( $str, 2, -1 );
            }
        }
        return $str;
    }

    /**
     *
     * @param resource $fp            
     * @return multitype: boolean
     */
    protected function _get_row($fp)
    {
        if (( $line = fgets ( $fp, $this->_config ['max_length'] ) ) !== FALSE) {
            $line = $this->_convert ( $line );
            $row = $this->str_get_csv($line);
            return array_map ( array ( 
                $this, 
                "_handle_cell" 
            ), $row );
        }
        return FALSE;
    }

    /**
     *
     * @param unknown $header            
     * @param unknown $column_config            
     * @return multitype:unknown
     */
    protected function _parse_header($header, $column_config)
    {
        $parsed_config = array ();
        foreach ($column_config as $key => $val) {
            if (is_int ( $key )) {
                $parsed_config [$key] = $val;
            }
            elseif (( $i_key = array_search ( $key, $header ) ) !== FALSE) {
                $parsed_config [$i_key] = $val;
            }
            elseif ($this->_config ['header_required']) {
                $this->error = array ( 
                    'code' => 'MISSING_HEADER', 
                    'data' => "解析数据出错：缺少列 “{$key}”。" 
                );
                return FALSE;
            }
        }
        return $parsed_config;
    }

    /**
     *
     * @param unknown $row            
     * @param unknown $column_config            
     * @return unknown multitype:NULL
     */
    protected function _parse_row($row, $column_config)
    {
        if (! $column_config) {
            return $row;
        }
        $parsed_row = array ();
        foreach ($column_config as $key => $val) {
            if (isset ( $row [$key] )) {
                $parsed_row [$val] = $row [$key];
            }
            else {
                $parsed_row [$val] = NULL;
            }
        }
        return $parsed_row;
    }
	/**
	 * 检查提供 的表字段信息是否完整存在
	 * @author shenpeiliang
	 * @param unknown $filename
	 * @param string $columns
	 * @param unknown $file_encoding
	 * @return boolean
	 */
    public function check_columns($filename,$columns=NULL,$file_encoding){
    	if(empty($columns)){
    		$this->error = array (
    				'code' => 'NO_FIELDS_IS_DETECTED',
    				'data' => '没有检测到表字段信息。'
    		);
    		return FALSE;
    	}elseif(!is_array($columns)){
    		$this->error = array (
    				'code' => 'INVALID_FORMAT',
    				'data' => '表字段格式配置错误。'
    		);
    		return FALSE;
    	}
    	$fp = @fopen ( $filename, 'r' );
	    $row = [];
    	if ($fp && ( $line = fgets ( $fp, $this->_config ['max_length'] ) ) !== FALSE) {
    		//编码格式
    		$this->config(array('encoding'=>$file_encoding));
            $line = $this->_convert ( $line );
            $row = $this->str_get_csv($line);
        }
    	@fclose ( $fp );
    	//去掉多余的空格
    	$needle=array_map('trim',$row);
    	//var_dump($needle);
    	$arr_field=array_keys($columns);
    	//var_dump($arr_field);
    	foreach ($arr_field as $val){
    		if(!in_array($val, $needle)){
    			$this->error = array (
    				'code' => 'FIELDS_NOT_ALLOWED_TO_CHANGE',
    				'data' => "表格字段[$val]不允许修改。"
    			);
    			return FALSE;
    		}
    	}
    	return TRUE;
    }
    /**
     *
     * @param unknown $filename            
     * @param array $config            
     * @return boolean multitype:Ambigous multitype:NULL, multitype:NULL unknown
     *         >
     */
    public function to_array($filename)
    {
        $config = $this->_config;
        $parse_header = FALSE;
        if ($config ['columns']) {
            if ($config ['has_header'] === NULL) {
                foreach ($config ['columns'] as $key => $val) {
                    if (! is_int ( $key )) {
                        $has_header = TRUE;
                        $parse_header = TRUE;
                    }
                }
            }
        }
        elseif ($config ['has_header'] === NULL) {
            $config ['has_header'] = TRUE;
        }

		///---------------------
		/// wht str_getcsv方式解析无法处理表格中包含逗号，使用phpexcel处理
		$phpexcel = $this->read_by_phpexcel($filename);
        if ($phpexcel === FALSE) {
            $this->error = array ( 
                'code' => 'CANNOT_OPEN_FILE', 
                'data' => '无法打开文件。' 
            );
            return FALSE;
        }
		$sheet = $phpexcel->getActiveSheet();

		if ($has_header) {
            $header = $this->read_head_by_phpexcel ( $sheet );
            if ($parse_header && $config ['columns']) {
                $config ['columns'] = $this->_parse_header ( $header, $config ['columns'] );
                if ($config ['columns'] === FALSE) {
                    return FALSE;
                }
            }
        }

        $data = array ();
		foreach ($sheet -> getRowIterator(2) as $row){
			$temp=[];
			foreach ($row->getCellIterator() as $col =>$cell){
				$index=PHPExcel_Cell::columnIndexFromString($col);
				if(!isset($config['columns'][$index-1])){
					continue;
				}
				$cell_value = $cell->getFormattedValue();
				if ($cell->getDataType() == PHPExcel_Cell_DataType::TYPE_NUMERIC){
					$cell_value = number_format($cell_value,0,'.','');
				}
				if ($cell->getDataType() == PHPExcel_Cell_DataType::TYPE_FORMULA){
					$cell_value = number_format($cell_value,2,'.','');
				}

				$temp[$config['columns'][$index-1]] = trim(nl2br($cell_value));
			}
			$data[] = $temp;
		}
        return $data;
    }

    public function read_by_phpexcel($filename){
		require_once SRCPATH . '/libs/phpexcel/Classes/PHPExcel/IOFactory.php';
		$objReader = PHPExcel_IOFactory::createReader('CSV')
			->setDelimiter(',')
			->setInputEncoding('GBK')//不设置将导致中文列内容返回boolean(false)或乱码
			->setEnclosure('"')
			->setSheetIndex(0);
		$objPHPExcel  = $objReader->load($filename);
		return $objPHPExcel;
	}

	public function read_head_by_phpexcel($sheet){

		$highestColumn = PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
		$header =[];

		for ($column=0;$column<$highestColumn;$column++){
			$header[] = trim(nl2br($sheet->getCellByColumnAndRow($column, 1)->getValue()));
		}
		return $header;
	}

    /**
     * send download header
     *
     * @chainable
     *
     * @param string $filename
     *            download filename
     */
    public function send_header($filename = NULL)
    {
        if (! $this->_header_sent) {
            if ($filename === NULL) {
                $filename = $this->_config ['output_filename'] ? $this->_config ['output_filename'] : time () . '.csv';
            }
            $cdfn = $filename ? 'filename="' . $filename . '"' : '';
            header ( 'Content-type: text/csv' );
            header ( 'Content-Disposition: attachment; ' . $cdfn );
            header ( 'Pragma: no-cache' );
            header ( 'Expires: 0' );
            $this->_header_sent = TRUE;
        }
        return $this;
    }

    /**
     * send one row
     *
     * @chainable
     *
     * @param array $row
     *            one row of data
     */
    public function send_row($row)
    {
        $this->_header_sent or $this->send_header ();
        if ($this->_config ['quote']) {
            array_walk ( $row, 
                    function (&$value, $index)
                    {
                        // escape double-quotes
                        $value = '"' . str_replace ( '"', '""', $value ) . '"';
                    } );
        }
        else {
            array_walk ( $row, 
                    function (&$value, $index)
                    {
                        // remove double-quotes and line-breaks
                        $value = str_replace ( '"', '', $value );
                        $value = str_replace ( "\n", '', $value );
                        $value = str_replace ( "\r", '', $value );
                    } );
        }
        echo iconv ( 'UTF-8', $this->_config ['encoding'] . '//TRANSLIT//IGNORE', implode ( $this->_config ['delimiter'], $row ) ) .
                 $this->_config ['line_break'];
        return $this;
    }

    /**
     * send multiple rows
     *
     * @chainable
     * @pararm array $data multiple rows of data
     */
    public function send_data($data)
    {
        foreach ($data as $row) {
            $this->send_row ( $row );
        }
        return $this;
    }

    /**
     * 解析含有中文+双引号的 CSV 格式字段字符串，并返回包含所读取字段的数组。
     * 
     * php7下，解析csv的函数str_getcsv()、fgetcsv()遇到含有中文的csv行，用默认解析方式都会不成功
     * 将环绕符换成 “ 的方式处理能够正确解析
     * 
     * @param [type] $str
     * @param [type] $delimiter
     * @return void
     */
    public function str_get_csv($str, $delimiter=null) {
        if(!$delimiter) $delimiter = $this->_config ['delimiter'];

        //判断是否含有 中文+双引号
        if(preg_match('/[\x{4e00}-\x{9fa5}]/u', $str)===1 && strpos($str, '"')!==FALSE ) {

            //特殊解析
            $row = str_getcsv($str, $delimiter, '“');

            //用此法解析的字符值会多出 '"'，需要trim处理
            foreach($row as &$_str) {
                //遇到单元格值为 ="..." 的形式时就不需要处理，由$this->_handle_cell()来处理
                if (mb_substr ( $_str, 0, 2 ) === '="' && mb_substr ( $_str, -1, 1 ) === '"') {
                    continue;
                }
                $_str = trim($_str, '"');
            }
            
        }else {
            //常规解析
            $row = str_getcsv($str, $delimiter, $this->_config ['enclosure']);
        }

        return $row;
    }
}

?>
