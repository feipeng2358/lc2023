<?php

/**
 * 数据列表接口控制器
 */
class Data_list extends My_controller
{
	public $comm_information_model;
	const  INFORMATION_TYPE_TEXT = 1; // 文字资料
	const  INFORMATION_TYPE_IMG = 2; // 图片资料
	public function __construct()
	{
		parent::__construct();
		$this->load_model('comm_information_model');
	}

	//获取图库类型列表
	public function picture_type_list()
	{
		$pictureName = $this->_param('pictureName', '');
		$list = $this->comm_information_model->get_information_img( ['pictureName' => $pictureName]);
		die($this->_json_result(10000, '操作成功', $list));
	}

	// 图库详情
	public function picture_detail()
	{
		$pictureId = $this->_param('pictureId', '');
		$list = $this->comm_information_model->get_picture_detail(['pictureId' => $pictureId]);
		for ($i = 2023; $i > 2018; $i--) {
			$years[$i]['id'] = $i;
			$years[$i]['name'] = $i . '年';
		}
		$list['yearList'] = $years;
		die($this->_json_result(10000, '操作成功', $list));
	}

	// 文章类型列表
	public function article_type_list()
	{
		$articleTypeName = $this->_param('articleTypeName', '');
		$list = $this->comm_information_model->get_information_text( [ 'articleTypeName' => $articleTypeName]);
		die($this->_json_result(10000, '操作成功', $list));
	}

	// 文章列表 
	public function article_list()
	{
		$articleTypeId = $this->_param('articleTypeId');
		$year = $this->_param('year');
		$list = $this->comm_information_model->get_information_text(['year' => $year, 'articleTypeId' => $articleTypeId]);
		$years = [];

		for ($i=2023; $i > 2018; $i--) {
			$years[$i]['id'] = $i;
			$years[$i]['name'] = $i.'年';
		}
		$list['yearList'] = $years;
		die($this->_json_result(10000, '操作成功', $list));
	}

	// 文章详情
	public function article_detail()
	{
		$articleId = $this->_param('articleId');
		$list = $this->comm_information_model->get_article_detail([ 'articleId' => $articleId]);
		die($this->_json_result(10000, '操作成功', $list));
	}

	// json化返回结果
	function _json_result($code, $message = '', $data = array())
	{
		header('Content-type:application/json;charset=utf-8');
		return json_encode(array('code' => $code, 'msg' => $message, 'data' => $data ? $data : array()));
	}
}
