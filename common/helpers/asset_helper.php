<?php
/**
 *
 */

function get_shikee_domains() {
    $domains = array(
        'www' => '',
        'static' => '',
    );

    foreach ($domains as $key => &$val) {
        $val = config_item('domain_' . $key);
    }

    $domains['image_servers'] = config_item('image_servers')[0];

    return $domains;
}

/**
 * @param $files
 * @return string
 */
function asset_version($files) {
    static $file_versions;
    if ($file_versions === NULL) {
        get_instance ()->load->config ( 'static_file_versions', true, true );
        $config = config_item ( 'static_file_versions' );
        $file_versions = isset($config['manifest']) ? $config['manifest'] : array();
    }
    if (is_array ( $files )) {
        $versions = array();
        foreach ($files as $item) {
            $versions [] = asset_version($item);
        }
        return hash('sha1', implode('|', $versions), FALSE);
    } else {
        return isset ( $file_versions [$files] ) ? $file_versions [$files] : '';
    }
}

/**
 * 生成静态文件链接
 *
 * @param string $file 文件
 */
function asset($file) {
    return config_item ( 'domain_static' ) . $file;
}

/**
 * 加载JS
 *
 * @param string|array $files 文件
 * @return string html
 */
function load_js( $files ) {
    if (! $files) {
        return '';
    }
    if (!is_array ( $files )) {
        $files = array($files);
    }
    $html = '';
    foreach ($files as $file) {
        $html .= '<script src="' . htmlspecialchars(asset($file) . '?v=' . asset_version($file)) . '"></script>' . "\n";
    }

    return $html;
}

/**
 * 加载CSS
 *
 * @param string|array $files 文件
 * @return string html
 */
function load_css( $files ) {
    if (! $files) {
        return '';
    }
    if (!is_array ( $files )) {
        $files = array($files);
    }
    $html = '';
    foreach ($files as $file) {

        $html .= '<link  type="text/css" rel="stylesheet" href="' . htmlspecialchars(asset($file) . '?v=' . asset_version($file)) . '"/>' . "\n";
    }
    return $html;
}

function load_global_vars() {
    $domains = get_shikee_domains();
    $vars = array(
        'CDN_ROOT' => $domains['static'],
        'JS_VERSION' => asset_version('/'), // 读取整个static.shikee.com目录的版本
        'CSS_VERSION' => asset_version('/'),
        'DOMAINS' => $domains,
    );

    return '<script type="text/javascript">var GV = ' . json_encode($vars) . ';</script>' . "\n";
}

/**
 * 使用JS代码执行页面跳转（终止执行）
 *
 * @param string $url 跳转地址；-1 表示返回上一个页面
 * @param string $message 提示信息
 * @param string $message_type 提示方式 alert/text
 * @return void
 */
function js_redirect($url, $message = '', $message_type = 'text') {
    echo '<meta charset="utf-8">';
    if ($message) {
        switch ($message_type) {
            case 'alert':
                echo '<script>alert(' . json_encode ( $message ) . ');</script>';
                break;
            case 'text':
            default:
                echo '<p>' . htmlspecialchars ( $message ) . '</p>';
                break;
        }
    }
//    echo $url == -1 ? '<script>history.go(-1);</script>' : '<script>location.href=' . json_encode($url) . ';</script>';
    //兼容浏览器
    echo $url == -1 ? '<script>window.location.href=document.referrer;</script>' : '<script>location.href=' . json_encode($url) . ';</script>';
    exit;

}

/**
 * 加载 文案配置文件
 * @param string $name
 * @return array ['lang']['page_copyright'];
 */
function load_lang( $name = '' ){

    get_instance ()->load->config ( 'lang', true, true );
    $lang_config = config_item ( 'lang' );
    if( $name !== '') {
        return isset($lang_config['lang'][$name]) ? $lang_config['lang'][$name] : '' ;
    } else {
        return $lang_config;
    }


}
