<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2019, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license	https://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Array Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/helpers/array_helper.html
 */

// ------------------------------------------------------------------------

if ( ! function_exists('element'))
{
	/**
	 * Element
	 *
	 * Lets you determine whether an array index is set and whether it has a value.
	 * If the element is empty it returns NULL (or whatever you specify as the default value.)
	 *
	 * @param	string
	 * @param	array
	 * @param	mixed
	 * @return	mixed	depends on what the array contains
	 */
	function element($item, array $array, $default = NULL)
	{
		return array_key_exists($item, $array) ? $array[$item] : $default;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('random_element'))
{
	/**
	 * Random Element - Takes an array as input and returns a random element
	 *
	 * @param	array
	 * @return	mixed	depends on what the array contains
	 */
	function random_element($array)
	{
		return is_array($array) ? $array[array_rand($array)] : $array;
	}
}

// --------------------------------------------------------------------

if ( ! function_exists('elements'))
{
	/**
	 * Elements
	 *
	 * Returns only the array items specified. Will return a default value if
	 * it is not set.
	 *
	 * @param	array
	 * @param	array
	 * @param	mixed
	 * @return	mixed	depends on what the array contains
	 */
	function elements($items, array $array, $default = NULL)
	{
		$return = array();

		is_array($items) OR $items = array($items);

		foreach ($items as $item)
		{
			$return[$item] = array_key_exists($item, $array) ? $array[$item] : $default;
		}

		return $return;
	}
}

/**
 * 按 $key_selector 指定的key来分组数据
 * @param array $input 数组
 * @param function $key_selector key的选择函数
 * @param function $value_selector value的选择函数
 * @param bool $unique value是否去重
 * @return array
 */
if ( ! function_exists('array_group_by')) {
    function array_group_by(array $input, $key_selector, $value_selector = null, $unique = false)
    {
        $result = array();
        foreach ($input as $it){
            $key = call_user_func($key_selector, $it);
            $value = $value_selector ? call_user_func($value_selector, $it) : $it;
            if(empty($result[$key]))
                $result[$key] = array();
            if(!$unique || !in_array($value, $result[$key]))
                array_push($result[$key], $value);
        }
        return $result;
    }
}

/**
 * 按 $key_field 指定的key来分组数据
 * @param array $input 二维数组, 每个元素是关联数组
 * @param function $key_field key字段
 * @param function $value_field value字段
 * @return array
 */
if ( ! function_exists('array_group_by_field')) {
    function array_group_by_field(array $input, $key_field, $value_field = null, $unique = false)
    {
        return array_group_by(
            $input,
            function (array $it) use ($key_field) { return $it[$key_field]; }, // key选择函数
            function (array $it) use ($value_field) { return $value_field ? $it[$value_field] : $it; }, // value选择函数
            $unique
        );
    }
}

/**
 * 构建树形
 * @param array $rows
 * @param string $id_field id字段
 * @param string $pid_field 父id字段
 * @return array 树形的数组, 子节点放到children中
 */
if ( ! function_exists('build_tree')) {
    function build_tree(array $rows, $id_field = 'id', $pid_field = 'pid')
    {
        $rows = array_column($rows, null, $id_field);

        // 构建树形
        foreach ($rows as &$row) {
            // 父id
            $pid = $row[$pid_field];
            // 挂到父节点
            if ($pid) {
                if(!isset($rows[$pid]))
                    throw new Exception("无法引用父节点: $id_field=$pid");
                $rows[$pid]['children'][] = &$row;
            }
        }

        // 过滤无pid的
        return array_filter($rows, function ($row) use ($pid_field) {
            return empty($row[$pid_field]);
        });
    }
}

/**
 * Tests if an array is associative or not. 是否关联数组
 *
 *     // Returns TRUE
 *     is_assoc(array('username' => 'john.doe'));
 *
 *     // Returns FALSE
 *     is_assoc('foo', 'bar');
 *
 * @param   array   $array  array to check
 * @return  boolean
 */
if ( ! function_exists('is_assoc'))
{
    function is_assoc_array(array $array)
    {
        // Keys of the array
        $keys = array_keys($array);

        // If the array keys of the keys match the keys, then the array must
        // not be associative (e.g. the keys array looked like {0:0, 1:1...}).
        return array_keys($keys) !== $keys;
    }
}

/**
 * 从数据中获得多级属性
 */
if ( ! function_exists('array_path'))
{
    function array_path($path, $array, $default = FALSE)
    {
        // 多级属性名
        $keys = explode('.', $path);
        do
        {
            // 当前级属性名
            $key = array_shift($keys);
            if (!isset($array[$key]))
                break;

            // 属性值
            $array = $array[$key];

            // 最后的属性
            if(empty($keys))
                return $array;

            // 非最后的属性, 但没有下一级array
            if(!is_array($array))
                break;
        }
        while ($keys);

        return $default;
    }
}
