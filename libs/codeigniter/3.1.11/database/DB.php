<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2018, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Initialize the database
 *
 * @category	Database
 * @author	EllisLab Dev Team
 * @link	https://codeigniter.com/user_guide/database/
 *
 * @param 	string|string[]	$params
 * @param 	bool		$query_builder_override
 *				Determines if query builder should be used or not
 */
function &DB($params = '', $query_builder_override = NULL)
{
    $CI =& get_instance();

    $arg_list = func_get_args();
    $cur_model = isset($arg_list[2]) ? $arg_list[2] : '';

    // Load the DB config file if a DSN string wasn't passed
    if (is_string($params) && strpos($params, '://') === FALSE)
    {
        // Is the config file in the environment folder?
        if ( ! file_exists($file_path = COMPATH.'config/'.ENVIRONMENT.'/database.php')
                && ! file_exists($file_path = APPPATH.'config/database.php'))
        {
            show_error('The configuration file database.php does not exist.');
        }
        
        include($file_path);
        
        // Make packages contain database config files,
        // given that the controller instance already exists
        if (class_exists('CI_Controller', FALSE))
        {
            foreach (get_instance()->load->get_package_paths() as $path)
            {
                if ($path !== COMPATH)
                {
                    if (file_exists($file_path = $path.'config/'.ENVIRONMENT.'/database.php'))
                    {
                        include($file_path);
                    }
                    elseif (file_exists($file_path = $path.'config/database.php'))
                    {
                        include($file_path);
                    }
                }
            }
        }
        
        if ( ! isset($db) OR count($db) === 0)
        {
            show_error('No database connection settings were found in the database config file.');
        }
        
        if ($params !== '')
        {
            $active_group = $params;
        }
        
        if ( ! isset($active_group))
        {
            show_error('You have not specified a database connection group via $active_group in your config/database.php file.');
        }
        elseif ( ! isset($db[$active_group]))
        {
            show_error('You have specified an invalid database connection group ('.$active_group.') in your config/database.php file.');
        }

        $params = $db[$active_group];
    }
    elseif (is_string($params))
    {
        /**
         * Parse the URL from the DSN string
         * Database settings can be passed as discreet
         * parameters or as a data source name in the first
         * parameter. DSNs must have this prototype:
         * $dsn = 'driver://username:password@hostname/database';
         */
        if (($dsn = @parse_url($params)) === FALSE)
        {
            show_error('Invalid DB Connection String');
        }
        
        $params = array(
                'dbdriver'	=> $dsn['scheme'],
                'hostname'	=> isset($dsn['host']) ? rawurldecode($dsn['host']) : '',
                'port'		=> isset($dsn['port']) ? rawurldecode($dsn['port']) : '',
                'username'	=> isset($dsn['user']) ? rawurldecode($dsn['user']) : '',
                'password'	=> isset($dsn['pass']) ? rawurldecode($dsn['pass']) : '',
                'database'	=> isset($dsn['path']) ? rawurldecode(substr($dsn['path'], 1)) : ''
        );
        
        // Were additional config items set?
        if (isset($dsn['query']))
        {
            parse_str($dsn['query'], $extra);
            
            foreach ($extra as $key => $val)
            {
                if (is_string($val) && in_array(strtoupper($val), array('TRUE', 'FALSE', 'NULL')))
                {
                    $val = var_export($val, TRUE);
                }
                
                $params[$key] = $val;
            }
        }
    }
    
    // No DB specified yet? Beat them senseless...
    if (empty($params['dbdriver']))
    {
        show_error('You have not selected a database type to connect to.');
    }
    
    // Load the DB classes. Note: Since the query builder class is optional
    // we need to dynamically create a class that extends proper parent class
    // based on whether we're using the query builder class or not.
    if ($query_builder_override !== NULL)
    {
        $query_builder = $query_builder_override;
    }
    // Backwards compatibility work-around for keeping the
    // $active_record config variable working. Should be
    // removed in v3.1
    elseif ( ! isset($query_builder) && isset($active_record))
    {
        $query_builder = $active_record;
    }
    
    require_once(BASEPATH.'database/DB_driver.php');
    
    if ( ! isset($query_builder) OR $query_builder === TRUE)
    {
        require_once(BASEPATH.'database/DB_query_builder.php');
        if ( ! class_exists('CI_DB', FALSE))
        {
            /**
             * CI_DB
             *
             * Acts as an alias for both CI_DB_driver and CI_DB_query_builder.
             *
             * @see	CI_DB_query_builder
             * @see	CI_DB_driver
             */
            class CI_DB extends CI_DB_query_builder 
            {
                public function reset_select()
                {
                    $this->_reset_select();
                }
                
                function compile_sql($select_override = FALSE)
                {
                	return $this->_compile_select($select_override);
                }
                
                function for_update($table = '')
                {
                	if ($table !== '')
                	{
                		$this->_track_aliases($table);
                		$this->from($table);
                	}
                	$sql = $this->_compile_select();

                	$result = $sql != '' ? $this->query($sql.' FOR UPDATE') : array();
                	$this->_reset_select();
                	
                	return $result;
                }
                
                /**
                 * 重写count_all_results方法，保留查询构造器缓存里的order_by
                 * {@inheritDoc}
                 * @see CI_DB_query_builder::count_all_results()
                 */
                function count_all_results($table = '', $reset = TRUE)
                {
					//保留查询构造器缓存里的order_by
                	$qb_cache_orderby = $this->qb_cache_orderby;

					//选择不重置查询构造器的时候保留order_by
					if($reset === FALSE) {
						$qb_orderby = $this->qb_orderby;
					}

                	$count = parent::count_all_results($table, $reset);
                	
                	$this->qb_cache_orderby = $qb_cache_orderby;
		
					if($reset === FALSE) {
						$this->qb_orderby = $qb_orderby;
					}

                	return $count;
                }
                
                /**
                 * 重写order_by函数，支持在order_by排序中写IF判断语句，兼容以前代码
                 * ORDER BY
                 *
                 * @param	string	$orderby
                 * @param	string	$direction	ASC, DESC or RANDOM
                 * @param	bool	$escape
                 * @return	CI_DB_query_builder
                 */
                public function order_by($orderby, $direction = '', $escape = NULL)
                {
                    $direction = strtoupper(trim($direction));
                
                    if ($direction === 'RANDOM')
                    {
                        $direction = '';
                
                        // Do we have a seed value?
                        $orderby = ctype_digit((string) $orderby)
                        ? sprintf($this->_random_keyword[1], $orderby)
                        : $this->_random_keyword[0];
                    }
                    elseif (empty($orderby))
                    {
                        return $this;
                    }
                    elseif ($direction !== '')
                    {
                        $direction = in_array($direction, array('ASC', 'DESC'), TRUE) ? ' '.$direction : '';
                    }
                
                    is_bool($escape) OR $escape = $this->_protect_identifiers;
                
                    if ($escape === FALSE)
                    {
                        $qb_orderby[] = array('field' => $orderby, 'direction' => $direction, 'escape' => FALSE);
                    }
                    else
                    {
                        $qb_orderby = array();
                        
                        //修改部分，判断排序中有括号，则不以逗号分割
                        if(preg_match('/\(/', $orderby))
                        {
                            $qb_orderby[] = ($direction === '' && preg_match('/\s+(ASC|DESC)$/i', rtrim($orderby), $match, PREG_OFFSET_CAPTURE))
                            ? array('field' => ltrim(substr($orderby, 0, $match[0][1])), 'direction' => ' '.$match[1][0], 'escape' => TRUE)
                            : array('field' => trim($orderby), 'direction' => $direction, 'escape' => TRUE);
                        }
                        else
                        {
                            foreach (explode(',', $orderby) as $field)
                            {
                                $qb_orderby[] = ($direction === '' && preg_match('/\s+(ASC|DESC)$/i', rtrim($field), $match, PREG_OFFSET_CAPTURE))
                                ? array('field' => ltrim(substr($field, 0, $match[0][1])), 'direction' => ' '.$match[1][0], 'escape' => TRUE)
                                : array('field' => trim($field), 'direction' => $direction, 'escape' => TRUE);
                
                            }
                        }
                    }
                
                    $this->qb_orderby = array_merge($this->qb_orderby, $qb_orderby);
                    if ($this->qb_caching === TRUE)
                    {
                        $this->qb_cache_orderby = array_merge($this->qb_cache_orderby, $qb_orderby);
                        $this->qb_cache_exists[] = 'orderby';
                    }
                
                    return $this;
                }
                
                function __get($key)
                {
                	$ret = NULL;
                	$qb_key = preg_replace('/^ar_/', 'qb_', $key);
                	if($qb_key == 'qb_store_array'){
                		$ret = property_exists($this, $qb_key) ? $this->{$key} : array();
                	}elseif(property_exists($this, $key)){
                		$ret = $this->{$key};
                	}elseif($qb_key != $key && property_exists($this, $qb_key)){
                		$ret = $this->{$qb_key};
                	}
                	return $ret;
                }
                
                function __set($key, $val)
                {
                	$qb_key = preg_replace('/^ar_/', 'qb_', $key);
                	if(property_exists($this, $key)){
                		$this->{$key} = $val;
                	}elseif($qb_key == 'qb_store_array' && is_array($val)){
                		foreach ($val as $k=>$item) {
               				$str = preg_replace('/^ar_/', 'qb_', $item);
               				if(in_array($item, array('ar_where', 'ar_wherein', 'ar_like'))){
               					$val['qb_where'] = 'qb_where';
               				}else{
                				$val[$str] = $str;
                			}
                		}
                		$this->{$qb_key} = $val;
                	}elseif($qb_key != $key && property_exists($this, $qb_key)){
                		$this->{$qb_key} = $val;
                	}
                }

				/**
				 * 解决 Commands out of sync; you can't run this command now 的问题
				 */
                public function free_all_result()
                {
                	if( ! $this->conn_id) return ;
                	while($this->conn_id->more_results())
                	{
                		$this->conn_id->next_result();
                		if($res = $this->conn_id->store_result())
                		{
                			$res->free();
                		}
                	}
                }
                
                /**
                 * 解决连表查询,LIKE条件不加表前缀和字段引号的问题
                 * 
                 * Returns the SQL string operator
                 *
                 * @param	string
                 * @return	string
                 */
                protected function _get_operator($str)
                {
                    static $_operators;
                
                    if (empty($_operators))
                    {
                        $_les = ($this->_like_escape_str !== '')
                        ? '\s+'.preg_quote(trim(sprintf($this->_like_escape_str, $this->_like_escape_chr)), '/')
                        : '';
                        $_operators = array(
                            '\s*(?:<|>|!)?=\s*',             // =, <=, >=, !=
                            '\s*<>?\s*',                     // <, <>
                            '\s*>\s*',                       // >
                            '\s+IS NULL',                    // IS NULL
                            '\s+IS NOT NULL',                // IS NOT NULL
                            '\s+EXISTS\s*\(.*\)',        // EXISTS(sql)
                            '\s+NOT EXISTS\s*\(.*\)',    // NOT EXISTS(sql)
                            '\s+BETWEEN\s+',                 // BETWEEN value AND value
                            '\s+IN\s*\(.*\)',            // IN(list)
                            '\s+NOT IN\s*\(.*\)',        // NOT IN (list)
                            '\s+LIKE\s+\S.*('.$_les.')?',    // LIKE 'expr'[ ESCAPE '%s']
                            '\s+NOT LIKE\s+\S.*('.$_les.')?', // NOT LIKE 'expr'[ ESCAPE '%s']
                            '\s+LIKE\s*' //新增代码
                        );
                
                    }
                
                    return preg_match('/'.implode('|', $_operators).'/i', $str, $match)
                    ? $match[0] : FALSE;
                }
                
                /**
                 * 重写UPDATE，支持 连表更新
                 *
                 * Compiles an update string and runs the query.
                 *
                 * @param	string	$table
                 * @param	array	$set	An associative array of update values
                 * @param	mixed	$where
                 * @param	int	$limit
                 * @return	bool	TRUE on success, FALSE on failure
                 */
                public function update($table = '', $set = NULL, $where = NULL, $limit = NULL)
                {
                	// Combine any cached components with the current statements
                	$this->_merge_cache();
                
                	if ($set !== NULL)
                	{
                		$this->set($set);
                	}
                	
                	if ($this->_validate_update($table) === FALSE)
                	{
                		return FALSE;
                	}
                
                	if ($where !== NULL)
                	{
                		$this->where($where);
                	}
                
                	if ( ! empty($limit))
                	{
                		$this->limit($limit);
                	}
                
                	//$sql = $this->_update($this->qb_from[0], $this->qb_set);
                	$sql = $this->_update($this->qb_from[0].(count($this->qb_join) ? ' '.implode(" ", $this->qb_join) : ''), $this->qb_set);// 修改行
                	$this->_reset_write();
                	return $this->query($sql);
                }

				/**
				 * 自定义一个调用存储过程的入口，原因：在CI里调用存储过程需要手动释放资源，在这里统一处理
				 */
				public function call_procedure($procedure, $param = FALSE, $return_object = NULL) {
					$sql = 'CALL '. $procedure;
					$result = $this->query ( $sql, $param,  $return_object)->row_array ();
					$this->free_all_result();
					return $result;
				}

            }
        }
    }
    elseif ( ! class_exists('CI_DB', FALSE))
    {
        /**
         * @ignore
         */
        class CI_DB extends CI_DB_driver { }
    }
    
    // Load the DB driver
    $driver_file = BASEPATH.'database/drivers/'.$params['dbdriver'].'/'.$params['dbdriver'].'_driver.php';
    
    file_exists($driver_file) OR show_error('Invalid DB driver');
    require_once($driver_file);
    
    // Instantiate the DB adapter
    $driver = 'CI_DB_'.$params['dbdriver'].'_driver';

    $in_trans_model = property_exists($CI, 'trans_models') && count($CI->trans_models) && in_array($cur_model, $CI->trans_models) ? TRUE : FALSE;
    if($in_trans_model){
    	$params['db_debug'] = FALSE;
    }
    $DB = new $driver($params);
    
    // Check for a subdriver
    if ( ! empty($DB->subdriver))
    {
        $driver_file = BASEPATH.'database/drivers/'.$DB->dbdriver.'/subdrivers/'.$DB->dbdriver.'_'.$DB->subdriver.'_driver.php';
        
        if (file_exists($driver_file))
        {
            require_once($driver_file);
            $driver = 'CI_DB_'.$DB->dbdriver.'_'.$DB->subdriver.'_driver';
            $DB = new $driver($params);
        }
    }
    
    $DB->initialize();
    return $DB;
}
