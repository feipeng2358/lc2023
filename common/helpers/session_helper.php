<?php
/**
 * session辅助函数
 */

/**
 * 开启session
 * @param string $session_id session id
 * @return bool
 */
function start_session($session_id = null, $use_cookies = true) {
	if (isset ( $_SESSION )) {
	    if ($session_id === null) {
            return true;
        }
        if (session_id() === $session_id) {
            return TRUE;
        }
        session_write_close();
    }
	ini_set('session.use_cookies', $use_cookies);
	if ($use_cookies) {
        session_set_cookie_params(0, '/', COOKIE_DOMAIN);
    }
    $session_id and session_id($session_id);
	return session_start();
}
