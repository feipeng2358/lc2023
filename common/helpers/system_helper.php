<?php
function show_config($key, $config, $default = '') {
    return isset($config[$key]) ? $config[$key] : $default;
}

function show_empty($val) {
    return $val ? $val : '-';
}

/**
 * 页面跳转并终止
 */
function go($url = '') {
    $url = $url ? $url : $_SERVER['HTTP_REFERER'];
    header('Location: ' . $url);
    exit ;
}

