<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home_new";
$route['404_override'] = 'home';

$route['get_stat'] = "home_new/get_stat/";

$route['default.html'] = "home";
$route['index.html'] = "home_new";
$route['us/contact.html'] = "/us/contact";
$route['us/about.html'] = "/us/about";
$route['us/adservers.html'] = "/us/adservers";
$route['us/adservers2.html'] = "/us/adservers2";
$route['us/adservers3.html'] = "/us/adservers3";
$route['us/confer.html'] = "/us/confer";
$route['us/job.html'] = "/us/job";
$route['us/bloc_intro.html'] = "/us/bloc_intro";

$route['us/link.html'] = "/us/link";

$route['us.html'] = "/us";
/* End of file routes.php */
/* Location: ./application/config/routes.php */
