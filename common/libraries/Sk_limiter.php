<?php

/**
 * Created by PhpStorm.
 * User: xwn
 * Date: 6/27/17
 * Time: 5:04 PM
 */

/**
 * 通用频率限制实现
 */
class Sk_limiter {
    /**
     * 配置
     * @var array|null
     */
    private $config = null;
    /**
     * 存储key
     * @var null|string
     */
    private $cache_key = null;
    /**
     * 当前计数
     * @var int
     */
    private $count = null;
    /**
     * 当前计数统计开始时间
     * @var null
     */
    private $begin_time = null;

    /**
     * Sk_limiter constructor.
     * @param array $config array(id: 限制项标识, threshold: 次数阈值, keep_time: 次数保存时间, exceed_keep_time: 超出阈值时，次数保存时间)
     */
    public function __construct($config = array()) {
        foreach (array( 'id', 'threshold', 'keep_time', 'exceed_keep_time') as $field) {
            if (! isset($config[$field])) {
                show_error(__CLASS__ . "错误：缺少配置字段 $field");
            }
        }

        $this->config = $config;
        $this->cache_key = 'sk_limiter_' . $config['id'];
    }

    /**
     * @return array|null
     */
    public function get_config() {
        return $this->config;
    }

    /**
     *
     */
    private function _get_data() {
        $data = cache($this->cache_key);
        $now = time();

        // 没有数据，计数置为0
        $count = 0;
        $begin_time = $now;
        if ($data) {
            // 有数据
            $count = isset($data['count']) ? (int) $data['count'] : 0;
            $begin_time = $count && isset($data['begin_time']) ? (int) $data['begin_time'] : $now;
            if ($now - $begin_time > ($count >= $this->config['threshold'] ? $this->config['exceed_keep_time'] : $this->config['keep_time'])) {
                // 超时，重置计数为0
                $count = 0;
                $begin_time = $now;
            }
        }

        $this->count = $count;
        $this->begin_time = $begin_time;
    }

    /**
     * 次数+1
     *
     * @return int 递增后的值
     */
    public function increase() {
        $this->_get_data();

        $this->count++;

        if ($this->count >= $this->config['threshold']) {
            $time = $this->config['exceed_keep_time'] ?: $this->config['keep_time'];
            $this->begin_time = time();
        } else {
            $time = $this->config['keep_time'];
        }

        cache ( $this->cache_key, array(
            'count' => $this->count,
            'begin_time' => $this->begin_time,
        ), $time);

        return $this->count;
    }

    /**
     * 次数是否已超过阈值
     * @return bool
     */
    public function is_exceeded() {
        return $this->get_count() >= $this->config['threshold'];
    }

    /**
     * 获取当前计数
     * @return int
     */
    public function get_count() {
        $this->_get_data();
        return $this->count;
    }

    /**
     * 获取当前计数统计的起始时间
     * @return int
     */
    public function get_begin_time() {
        $this->_get_data();
        return $this->begin_time;
    }

    /**
     * 清除计数
     * @return void
     */
    public function clear() {
        cache($this->cache_key, false);
    }
}
