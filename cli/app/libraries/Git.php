<?php

class Git {
	private $_pretty_format = '';
	protected $_git_executable = 'git';
	protected $_git_dir = '.git';
	protected function _exec($git_cmd, $arguments, $lines_as_array = FALSE) {
		if (! is_array ( $arguments )) {
			$arguments = [$arguments];
		}

		$cmd = $this->_git_executable . ' '
			. '--git-dir=' . $this->_git_dir . ' '
			. $git_cmd . ' '
			. implode ( ' ', $arguments );

		// echo $cmd;

		exec ( $cmd, $output, $return_var );
		if ($return_var) echo $cmd;
		return $lines_as_array ? $output : implode ( "\n", $output );
	}

	public function set_git_dir($new_dir) {
		$this->_git_dir = $new_dir;
	}

	public function last_commit_of_file($path, $sort = TRUE) {
		$args = [
			'-n1',
			'--oneline',
			'--',
			escapeshellarg ( $path )
		];
		$output = $this->_exec ( 'log', $args );
		$regex = '/^\w+/';
		if (preg_match ( $regex, $output, $matches )) {
			return $matches [0];
		}
		else {
			return FALSE;
		}
	}

	public function changed_files_of_commit_range($hash1, $hash2, $path) {
		$args = [
			'--name-only',
			$hash1,
			$hash2,
			'--',
			escapeshellarg ( $path )
		];
		$output = $this->_exec ( 'diff', $args, TRUE );
		return $output;
	}
}
