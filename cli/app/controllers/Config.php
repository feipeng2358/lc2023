<?php
class Config extends Cli_controller {
    public function __construct()
    {
        parent::__construct ();
    }

    /**
     * 检查数据库和配置文件中保存的业务配置的一致性
     */
    public function check() {
        $this->load->model('business_config_model');
        $diff = $this->business_config_model->diff();

        pl('只存在于数据库的配置项：');
        $diff['keys_only_in_db'] or pl('    (无)');
        foreach ($diff['keys_only_in_db'] as $key) {
            pl('    ' . $key);
        }

        pl('只存在于配置文件的配置项：');
        $diff['keys_only_in_file'] or pl('    (无)');
        foreach ($diff['keys_only_in_file'] as $key) {
            pl('    ' . $key);
        }

        pl('数据库和配置文件的值不一致的配置项：');
        $diff['keys_with_different_values'] or pl('    (无)');
        foreach ($diff['keys_with_different_values'] as $key) {
            pl('    ' . $key);
        }
    }

	/**
	 * 复制配置文件 lc.php
	 */
    public function cp_lc(){
        $file = 'lc.php';
        $src = COMPATH."config/lc0.com/$file"; // 源文件
        $content = file_get_contents($src);
        // 1. src_1 到 src_9
        for ($i = 1; $i <= 9; $i++) {
            $new_content = str_replace('lc0', "lc$i", $content); // 修改内容
            $dest = "/data/htdocs/src_$i/common/config/lc$i.com/$file"; // 目标文件
            file_put_contents($dest, $new_content); // 直接覆盖
        }
        // 2. src
        $new_content = str_replace('lc0', "lc", $content); // 修改内容
        $dest = "/data/htdocs/src/common/config/lc.com/$file"; // 目标文件
        file_put_contents($dest, $new_content); // 直接覆盖
        echo "done";

    }
}
