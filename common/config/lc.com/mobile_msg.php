<?php
//发送短信服务配置
$config['mobile_msg'] = array(
	// 发送短信服务 主帐号,对应开官网发者主账号下的 ACCOUNT SID
	'SMS_ACCOUNT_SID'=>'8a48b5514f1702fd014f1ae34d2203ef',
	// 发送短信服务 主帐号令牌,对应官网开发者主账号下的 AUTH TOKEN
	'SMS_AUTH_TOKEN'=>'5fc327e60d714954b01c4e12843333f8',
	// 发送短信服务 开发调试的时候，可以使用官网自动为您分配的测试Demo的APP ID//应用Id，在官网应用列表中点击应用，对应应用详情中的APP ID
	'SMS_APP_ID'=> '8a48b5514f1702fd014f1b5932dd04fe',
	// 请求地址:沙盒环境（用于应用开发调试）：sandboxapp.cloopen.com 生产环境（用户应用上线使用）：app.cloopen.com
	'SMS_SERVER_IP'=>'sandboxapp.cloopen.com',
	//请求端口，生产环境和沙盒环境一致
	'SMS_SERVER_PORT'=>8883,
	//REST版本号，在官网文档REST介绍中获得。
	'SMS_SOFT_VERSION'=>'2013-12-26',
);
