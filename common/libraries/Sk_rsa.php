<?php
/**
 *RSA 加密类
 *@author blue
 */
class Sk_rsa   {

    const  RSA_ENCRYPT_BLOCK_SIZE = 117; //最大加密字符长度（如果生成的是 1024 位的 KEY，那最大的可加密字节数为 1024 / 8 - 11 = 117） 个字节
    const  RSA_DECRYPT_BLOCK_SIZE = 128;
    public $error;

    private $_config = [
        'public_key' => '',
        'private_key' => '',
    ];

    public function __construct($_config = array()) {
        if( $_config ){
            $this->_config['private_key'] = $this->_get_contents( $_config['private_path']);
            $this->_config['public_key'] = $this->_get_contents( $_config['public_path']);
        } else {
            $this->_config['private_key'] = $this->_get_contents(COMPATH.'config/'.ENVIRONMENT.'/rsa_key/app_data_private.pem');
            $this->_config['public_key'] = $this->_get_contents(COMPATH.'config/'.ENVIRONMENT.'/rsa_key/app_data_public.pem');
        }

    }

    /**
     * @uses 获取文件内容
     * @param $file_path string
     * @return bool|string
     */
    private function _get_contents($file_path) {
        file_exists($file_path) or die ('密钥或公钥的文件路径错误');
        return file_get_contents($file_path);
    }

    /**
     * @uses 获取私钥
     * @return bool|resource
     */
    private function _get_private_key() {
        $priv_key = $this->_config['private_key'];
        return openssl_pkey_get_private($priv_key);
    }

    /**
     * @uses 获取公钥
     * @return bool|resource
     */
    private function _get_public_key() {
        $public_key = $this->_config['public_key'];
        return openssl_pkey_get_public($public_key);
    }

    /**
     * @uses 私钥加密
     * @param string $data
     * @return null|string
     */

    public function priv_encrypt($data = '', $new_priv_key = "") {
        if (!is_string($data)) {
            return null;
        }

        $priv_key = $new_priv_key !="" ? $new_priv_key : $this->_get_private_key();

        $blocks = str_split($data, RSA_ENCRYPT_BLOCK_SIZE);

        $encrypted_string = '';
        foreach ($blocks as $block) {
            openssl_private_encrypt($block, $encrypted, $priv_key);
            $encrypted_string .= $encrypted;
        }

        return base64_encode($encrypted_string);
    }

    /**
     * @uses 公钥加密
     * @param string $data
     * @return null|string
     */
    public function public_encrypt($data = '', $new_puble_key = "") {
        if (!is_string($data)) {
            return null;
        }
        $public_key = $new_puble_key !="" ? $new_puble_key : $this->_get_public_key();

        $blocks = str_split($data, self::RSA_ENCRYPT_BLOCK_SIZE);//分段处理
        $encrypted_string = '';
        foreach ($blocks as $block) {
            if(openssl_public_encrypt($block, $encrypted, $public_key)){
                $encrypted_string .= $encrypted;
            } else {
              return null;
            }
        }

        return base64_encode($encrypted_string);
    }

    /**
     * @uses 私钥解密
     * @param string $encrypted
     * @return null
     */
    public function priv_decrypt($data = '', $new_priv_key = "") {
        if (!is_string($data)) {
            return null;
        }

        $priv_key = $new_priv_key !=="" ? $new_priv_key : $this->_get_private_key();

        $blocks = str_split( base64_decode($data) , self::RSA_DECRYPT_BLOCK_SIZE);//分段处理
        $decrypted_string = '';
        foreach ($blocks as $block) {
            if(openssl_private_decrypt($block, $decrypted, $priv_key)){
                $decrypted_string .= $decrypted;
            } else {
                return null;
            }
        }

        return $decrypted_string;
    }

    /**
     * @uses 公钥解密
     * @param string $encrypted
     * @return null
     */
    public function public_decrypt($encrypted = '') {
        if (!is_string($encrypted)) {
            return null;
        }
        return (openssl_public_decrypt(base64_decode($encrypted), $decrypted, $this->_getPublicKey())) ? $decrypted : null;
    }

    /**
     * 根据私钥生成公钥
     */
    public function create_public_key(){
        return openssl_pkey_get_details($this->_get_private_key())['key'];
    }

	/**
	 * JS专用 公钥加密
	 * @param string 明文
	 * @param string 证书文件（.crt）
	 * @return string 密文（base64编码）
	 */
	public function publickey_encodeing($sourcestr, $fileName) {
		if(!file_exists($fileName)){
			$this->error =  array ('data' => "路径 {$fileName} 无法找到公钥证书文件。");
			return false;
		}
		$key_content = file_get_contents ( $fileName );
		$pubkeyid = openssl_get_publickey ( $key_content );

		if (openssl_public_encrypt ( $sourcestr, $crypttext, $pubkeyid )) {
			return base64_encode ( "" . $crypttext );
		}else{
			$this->error =  array ( 
				'data' => "加密失败。" 
			);
			return  false;
		}
	}


	/**
	 * JS专用 私钥解密
	 * @param string 密文（二进制格式且base64编码）
	 * @param string 密钥文件（.pem / .key）
	 * @param string 密文是否来源于JS的RSA加密
	 * @return string 明文
	 */
	public function privatekey_decodeing($crypttext, $fileName, $fromjs = false) {
		if(!file_exists($fileName)){
			$this->error =  array ('data' => "路径 {$fileName} 无法找到私钥证书文件。");
			return false;
		}
		$key_content = file_get_contents ( $fileName );
		$prikeyid = openssl_pkey_get_private ( $key_content );
		$crypttext = base64_decode ( $crypttext );
		$padding = $fromjs ? OPENSSL_NO_PADDING : OPENSSL_PKCS1_PADDING;
		if (openssl_private_decrypt ( $crypttext, $sourcestr, $prikeyid, $padding )) {
			// 			return $fromjs ? rtrim ( strrev ( $sourcestr ), "/0" ) : "" . $sourcestr;
			return $fromjs ? rtrim ( strrev ( $sourcestr ) ) : "" . $sourcestr;
		}else{
			$this->error =  array (
				'data' => "解密失败。"
			);
			return false;
		}
	}


}
