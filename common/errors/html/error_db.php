<?php
header("Content-type: text/html; charset=utf-8");
include COMPATH.'config/'.ENVIRONMENT. '/lc.php';
include COMPATH.'config/'.ENVIRONMENT.'/lang.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <style type="text/css">
        body,h1,h2,h3,h4,h5,h6,p,dl,dt,dd,ul,li,img,form,button,input{ margin: 0; padding: 0; }
        html,body{height: 100%; font: 12px/1.5 tahoma, \5b8b\4f53, arial; background-color:#FBFBFB;background:#FBFBFB;}/*font-family:\5fae\8f6f\96c5\9ed1;*/
        h1 { font-size: 100%; font-weight: normal; }
        a { text-decoration: none; color: #3e3e3e; }
        a:hover { text-decoration: underline; color: #c92a33; }
        .loginInfo,.top-nav-other,.Copyright{float: left;}
        .top-menu,.top-navMenu,.Contact{float: right;}
        .Prompt,.back-home,.back-previous,.Phone,.Mail{background: url(<?php echo $config['domain_static'] ?>common/img/404/bg.png) no-repeat;}
        /*==main==*/
        .header{background: url(<?php echo $config['domain_static'] ?>common/img/404/header-bg.png) repeat-x; height: 30%; min-width: 1000px;}
        .Prompt{width: 580px; height:208px;  margin:auto; padding-left:395px; padding-top:20px; position: relative; overflow: hidden;}
        .Prompt h1{ font:30px \5fae\8f6f\96c5\9ed1; padding-bottom:10px; color: #534f47;}
        .Prompt p{ font:14px \5b8b\4f53; line-height: 26px; color: #645f56;}
        .Prompt a{padding:2px 22px; 1height: 30px; display:inline; color: #cc4d08; font-size: 12px; float: left; margin-top: 5px;}
        .back-home{background-position:0 -230px;}
        .back-previous{background-position:0 -262px; width:80px;}
        /*==END main==*/
        /*==footer==*/
        .footer{ height: 44px; width: 100%;min-width: 1000px; background:#EAEAEA; line-height: 44px; position: absolute; bottom:0;border-top: dotted 1px #d4d3d3; color: #858585;}
        .footer a{color: #666; padding: 0 10px; height: 35px;}
        .Copyright{ padding-left:15px;}
        .Contact{ padding-right:15px; padding-left: 32px;}
        .Contact strong{ padding:10px 25px; font-weight: normal;}
        .Phone{background-position:-175px -222px;}
        .Mail{background-position:-175px -260px; }
        .tips{ color: #fff; font-size:25px;left: 83px; position: absolute; top: 35px; width: 200px; text-align: center; font-weight:700;font-family: arial,\5b8b\4f53  }
        /*==END footer==*/
    </style>
</head>
<body>
<div class="header"></div>
<div class="Prompt">
<div class="tips">system busy</div>
    <h1>系统繁忙</h1>
    <div style="height:130px; overflow:hidden;color:#999;">
		<?php echo !in_array(ENVIRONMENT,[
    		'taofaxian.com.cn',
		]) && isset($message)? $message : '有可能我们的网页正在维护或者您输入的网址不正确哦!'; ?>
    </div>
        <!-- 有可能我们的网页正在维护或者您输入的网址不正确哦!
        <br>-->

        <a class="back-home" href="<?php echo $config['domain_www']?>">返回主页</a>
        <?php if(isset($_SERVER['HTTP_REFERER'])): ?><a class="back-previous" href="<?php echo $_SERVER['HTTP_REFERER'] ?>">返回上一页</a> <?php endif; ?>
    
</div>
<div class="footer">
        <span class="Copyright">
           <?php echo $config['lang']['page_copyright'];?>
        </span>
</div>
</body>
