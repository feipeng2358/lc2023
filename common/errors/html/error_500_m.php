<?php
header("Content-type: text/html; charset=utf-8");
include COMPATH.'config/'.ENVIRONMENT. '/lc.php';
include COMPATH.'config/'.ENVIRONMENT.'/lang.php';
?>
<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $config['lang']['page_title_500_m'];?></title>
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport"/>
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta content="black" name="apple-mobile-web-app-status-bar-style"/>
    <meta content="telephone=no" name="format-detection"/>
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        body ,html{
            height: 100%;
        }
        a {
            text-decoration: none;
            color: inherit;
        }
        .content {
            display: flex;
            flex-direction: column;
            height: 100%;
        }
        .header {
            flex: 1;
            display: flex;
            align-items: center;
            flex-direction: column;
            justify-content: center;
            font-size: 13px;
            color: #666;
            padding: 0 30px;
        }
        .header > p {
            font-size: 16px;
            margin-bottom: 20px;
            text-align: center;
        }
        .header > p > img {
            width: 60%;
            display: inline-block;
        }
        .footer {
            padding: 20px;
            color: #999;
            line-height: 18px;
        }
        .copyright {
            font-size: 10px;
            text-align: center;
        }
        .contact {
            text-align: center;
        }
        .contact strong {
            font-weight: normal;
            display: block;
            font-size: 10px;
        }
    </style>
</head>
<body>
<div class="content">
    <div class="header">
        <p><img src="<?php echo config_item('domain_static'); ?>common/img/404/404.png" /></p>
        <div><?php echo $message?></div>
    </div>
    <div class="footer">
        <div class="copyright">
            <p><?php echo $config['lang']['page_copyright_m'];?></p>
        </div>
        <span class="contact">
            <strong class="Mail"><?php echo $config['lang']['page_contact_mail'];?></strong>
        </span>
    </div>
</div>
</body>
</html>
