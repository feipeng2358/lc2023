<?php


use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class  JwtAuth
{
	private $key = 'tuokejwtkey';         # 签发key
	private $alg = 'HS256';         # 加密方式
	private $exp = 86400;# 过期时间 +86400s = + 1天

	public $error;

	protected function _error($errcode, $errmsg, $data = [], $code = 400)
	{
		if (is_array($errcode) || is_object($errcode)) {
			$this->error = (array)$errcode;
		} else {
			$this->error = compact('errcode', 'errmsg', 'code', 'data');
		}

		return false;
	}


	# 生成token
	public function create_token($payload)
	{
		$token = [
			'iss' => 'com.lc.tuoke',           # 签发人 可以为空
			'aud' => 'com.lc.tuoke',          # 接收人 可以为空
			'iat' => time(),            # 签发时间
			'nbf' => time(),            # 生效时间
			'exp' => time() + $this->exp,      # 过期时间
			'data' => $payload,
		];
		return JWT::encode($token, $this->key, $this->alg);
	}

	public function check($token)
	{
		# 生成的token
		try {
			# 当前时间-60 留点时间
			JWT::$leeway = 60;
			$key = new Key($this->key, $this->alg);# new key(生成token时的key , 生成token时的加密模式)
			$decoded = JWT::decode($token, $key);

//			$headers = [
//				'x-forwarded-for' => 'www.google.com'
//			];
//
//			$decoded_array = (array) $decoded;
			$decoded_array = json_decode(json_encode($decoded), true);
			return $decoded_array;

		} catch (\Exception $exception) {
			# Firebase定义了很多异常捕获
//			use Firebase\JWT\SignatureInvalidException;
//			use Firebase\JWT\BeforeValidException;
//			use Firebase\JWT\ExpiredException;
//			use DomainException;
//			use InvalidArgumentException;
//			use UnexpectedValueException;

//			try {
//				$decoded = JWT::decode($payload, $keys);
//			} catch (InvalidArgumentException $e) {
//				// provided key/key-array is empty or malformed.
//			} catch (DomainException $e) {
//				// provided algorithm is unsupported OR
//				// provided key is invalid OR
//				// unknown error thrown in openSSL or libsodium OR
//				// libsodium is required but not available.
//			} catch (SignatureInvalidException $e) {
//				// provided JWT signature verification failed.
//			} catch (BeforeValidException $e) {
//				// provided JWT is trying to be used before "nbf" claim OR
//				// provided JWT is trying to be used before "iat" claim.
//			} catch (ExpiredException $e) {
//				// provided JWT is trying to be used after "exp" claim.
//			} catch (UnexpectedValueException $e) {
//				// provided JWT is malformed OR
//				// provided JWT is missing an algorithm / using an unsupported algorithm OR
//				// provided JWT algorithm does not match provided key OR
//				// provided key ID in key/key-array is empty or invalid.
//			}
			return $this->_error('EX', $exception->getMessage());
		}
	}
}
