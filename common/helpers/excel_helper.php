<?php
/**
 * 生成Excel表格
 *
 * @param array|callable 要导出的数据 ，可以是回调函数，每次调用返回一行数据，返回FALSE终止
 * @param array 表格头
 * @param string 文件名
 * @return void
 */
function make_xls($data_source, $head = NULL, $filename = NULL)
{
	if (!$is_array = is_array($data_source) || !$is_callable = is_callable($data_source))
	{
		trigger_error('指定的数据源无效。');
		return;
	}

	if (!$filename)
	{
		$filename = time() . '.xls';
	}

	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment; filename=' . $filename);
	header('Pragma: no-cache');
	header('Expires: 0');

	//TODO 用别的方法

	if ($head && is_array($head))
	{
		echo iconv('utf-8', 'gbk', implode("\t", $head)) . "\n";
	}

	if ($is_array)
	{
		foreach ($data_source as $row)
		{
			echo iconv('utf-8', 'gbk', implode("\t", $row)) . "\n";
		}
	} else if ($is_callable)
	{
		while ($row = $data_source ())
		{
			echo iconv('utf-8', 'gbk', implode("\t", $row)) . "\n";
		}
	}

	exit;
}

/**
 * 生成csv表格
 *
 * @param array|callable 要导出的数据 ，可以是回调函数，每次调用返回一行数据，返回FALSE终止
 * @param array 表格头
 * @param string 文件名
 * @return void
 */
function make_csv($data_source, $head = NULL, $filename = NULL)
{
	if (!$is_array = is_array($data_source) || !$is_callable = is_callable($data_source))
	{
		trigger_error('指定的数据源无效。');
		return;
	}

	if (!$filename)
	{
		$filename = time() . '.csv';
	}

	header('Content-Type: text/csv');
	header('Content-Disposition: attachment; filename=' . $filename);
	header('Pragma: no-cache');
	header('Expires: 0');

	//TODO 用别的方法

	if ($head && is_array($head))
	{
		echo iconv('utf-8', 'gbk', implode(",", $head)) . "\n";
	}

	if ($is_array)
	{
		foreach ($data_source as $row)
		{
			echo iconv('utf-8', 'gbk', implode(",", $row)) . "\n";
		}
	} else if ($is_callable)
	{
		while ($row = $data_source ())
		{
			echo iconv('utf-8', 'gbk', implode(",", $row)) . "\n";
		}
	}

	exit;
}