<?php
/**
 * Redis 缓存
 */
class Sk_redis{
	public $error;
	
	public $handle = NULL;	//存储Redis对象
	
	protected $config = array(
			'host'=>'127.0.0.1',
			'port'=>6379,
			'auth'=>'root',    //是否有用户验证，默认无密码验证。如果不是为null，则为验证密码
			'timeout'=>0,   //连接超时,0为不超时
			'reserved'=>null,
			'retry_interval'=>100,  //单位是 ms 毫秒
			'reconnect'=>FALSE, //连接超时是否重连  默认不重连
			'prefix'=>'sk_' //键值前缀
	);
	/**
	 * 初始化
	 * @param unknown $config
	*/
	public function __construct(){
		$this->ci = &get_instance();
		$this->ci->config->load('redis');
		$config = $this->ci->config->item ( 'default' );
		if($config)
		    $this->config = array_merge($this->config,$config);
	}
	/**
	 * 解析连接
	 */
	public function connect(){
        //创建Redis对象：可能抛出异常 RedisException
        $this->handle = new Redis();

        // 连接
        if(!$this->handle->pconnect($this->config['host'], $this->config['port'], $this->config['timeout'])){
            $this->error = array(
                'code' => 1001,
                'data' => 'REDIS_SERVER_WENT_AWAY'
            );
            return false;
        }

        // 已设置密码的需要验证
        if($this->config['auth']){
            $this->handle->auth($this->config['auth']);
        }

		return $this->handle;
	}
	/**
	 * 获取 Redis 实例，用于复杂操作
	 * @return string
	 */
	public function getInstance(){
		return $this->handle;
	}

	/**
	 * 缓存数据
	 * @param unknown $key
	 * @param string $data
	 * @param number $expire
	 */
	public function cache($key, $data = NULL, $expire = 1800) {
		$key = $this->config['prefix'] . $key;
		if ($data === NULL) {
			return $this->get ( $key );
		}

		if ($data === FALSE) {
			return $this->del ( $key );
		}

		return $this->set ( $key, $data, $expire );
	}
	/**
	 * 缓存数据 Hash数据类型
	 * @param unknown $key
	 * @param string $data
	 * @param number $expire
	 */
	public function hCache($key, $data = NULL, $expire = 1800) {
		$key = $this->config['prefix'] . $key;
		if ($data === NULL) {
			return $this->hGet ( $key );
		}

		if ($data === FALSE) {
			return $this->del ( $key );
		}

		return $this->hSet ( $key, $data, $expire );
	}
	/**
	 * 获取key值
	 * @param unknown $key
	 */
	public function hGet($key){
		return $this->handle->hGetAll($key);
	}
	/**
	 * 设置key值 Hash
	 * @param unknown $key
	 * @param unknown $data
	 * @param unknown $expire
	 */
	public function hSet($key,$data,$expire){
		$this->handle->hMset($key,$data);
		return $this->handle->expireAt($key,time()+$expire);
	}

	/**
	 * 设置key值
	 * @param unknown $key
	 * @param unknown $data
	 * @param unknown $expire
	 */
	public function set($key,$data,$expire){
		return $this->handle->setex($key,$expire,$data);
	}
	/**
	 * 获取key值
	 * @param unknown $key
	 */
	public function get($key){
		return $this->handle->get($key);
	}
	/**
	 * 删除key值
	 * @param unknown $key
	 */
	public function del($key){
		return $this->handle->delete($key);
	}
	/**
	 * 返回key的剩余的过期时间（单位s）
	 * @param unknown $key
	 */
	public function ttl($key){
		return $this->handle->ttl($key);
	}

	/**
	* 使用原生api
	*
	* @param [type] $method
	* @param [type] $parameters
	* @return void
	*/
	public function __call($method, $parameters)
	{
		return $this->handle->{$method}($parameters);
	}
}
