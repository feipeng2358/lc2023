<?php
/**
 * App_login_model
 */
class App_login_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database('default');
        $this->slave_db = $this->load->database('slave', TRUE);
    }

    public function wx_user_reg($uname, $mobile, $login_sign, $openid, $nickname, $avatar)
    {
        $this->db->trans_begin();

        $this->db->insert('user', $user = [
            'uname' => $uname,
            //'type' => 1,
            'real_name' => $uname,
            //'password' => $password,
            //'salt' => $salt,
            'mobile' => $mobile,
            'dateline' => time(),
            'lastloginip' => ip(),
            'lastlogintime' => time()
        ]);
        $uid = $this->db->insert_id();
        $user['uid'] = $uid;
        $this->db->insert('wx_user', [
            'uid' => $uid,
            'uname' => $uname,
            'user_type' => 1,
            'openid' => $openid,
            'nickname' => $nickname,
            'avatar' => $avatar,
            'dateline' => time(),
            'reg_ip' => ip(),
            'reg_time' => time(),
            'last_login_ip' => ip(),
            'last_login_time' => time()
        ]);

        $this->db->insert('app_sign', [
            'uid' => $uid,
            'sign' => $login_sign,
            'uname' => $uname,
            'client_type' => 3,
            'dateline' => time()
        ]);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            $user['sign'] = $login_sign;
            return $user;
        }
    }

    public function seller_reg_wx_user($user, $login_sign, $openid, $nickname, $avatar)
    {
        $this->db->trans_begin();

        $this->db->where(['uid' => $user['uid']])->update('user', $data = [
            'lastloginip' => ip(),
            'lastlogintime' => time()
        ]);

        $this->db->insert('wx_user', [
            'uid' => $user['uid'],
            'uname' => $user['uname'],
            'user_type' => 1,
            'openid' => $openid,
            'nickname' => $nickname,
            'avatar' => $avatar,
            'dateline' => time(),
            'reg_ip' => ip(),
            'reg_time' => time(),
            'last_login_ip' => ip(),
            'last_login_time' => time()
        ]);

        $row = $this->db->where(['uid' => $user['uid'], 'client_type' => 3])->get('app_sign')->row_array();
        if(empty($row)){
            $this->db->insert('app_sign', [
                'uid' => $user['uid'],
                'sign' => $login_sign,
                'uname' => $user['uname'],
                'client_type' => 3,
                'dateline' => time()
            ]);
        }else{
            $this->db->where(['uid' => $user['uid'], 'client_type' => 3])->update('app_sign',[
                'sign' => $login_sign,
                'dateline' => time()
            ]);
        }
        

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            $user['sign'] = $login_sign;
            $user['lastloginip'] = $data['lastloginip'];
            $user['lastlogintime'] = $data['lastlogintime'];
            return $user;
        }
    }

    public function login_state($user, $login_sign, $nickname, $avatar)
    {
        $this->db->trans_begin();

        $this->db->where(['uid' => $user['uid']])->update('user', $data = [
            'lastloginip' => ip(),
            'lastlogintime' => time()
        ]);
        $this->db->where(['uid' => $user['uid']])->update('wx_user', [
            'nickname' => $nickname,
            'avatar' => $avatar,
            'last_login_ip' => ip(),
            'last_login_time' => time()
        ]);

        $row = $this->db->where(['uid' => $user['uid'], 'client_type' => 3])->get('app_sign')->row_array();
        if(empty($row)){
            $this->db->insert('app_sign', [
                'uid' => $user['uid'],
                'sign' => $login_sign,
                'uname' => $user['uname'],
                'client_type' => 3,
                'dateline' => time()
            ]);
        }else{
            $this->db->where(['uid' => $user['uid'], 'client_type' => 3])->update('app_sign',[
                'sign' => $login_sign,
                'dateline' => time()
            ]);
        }

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return $user;
        }
    }
}
