<?php
/**
 * 定时刷新app server的缓存
 *
 *   1 刷新app活动列表缓存的命令： php sk/cli/bootstrap.php app_cache  refresh_trys_cache
 *
 * User: shi
 * Date: 7/29/17
 * Time: 1:02 PM
 */

class App_cache extends Cli_controller {
    // 参数名
    protected $keys = [
        'join_state',
        'per_page',
        'sort',
        'order_by',
        'type',
        'source',
        'coin_only',
        'page',
    ];
    // 参数值
    protected $params = [
        ['all'], // is_join
        [10, 20], // per_page
        ['asc', 'desc'], // sort
        ["start_time", "price", "quantity_remain"], // order_by
        [0,5,6,7,8,11], // type
        [0,1,2,5,7], // source
        [0, 1], // coin_only
        [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30], // page
    ];
    // 使用栈来迭代
    // 栈中元素，表示所有参数值的一个序列
    // 遍历所有参数值，就是构建所有序列
    protected $idx_stack = [];

    /**
     * 刷新app活动列表的缓存
     *    遍历各种参数场景，来构建并请求url，url中带参数__refresh_cache_by_timer_from_bug_6168 来标识定时任务的强制刷新缓存
     */
    public function refresh_trys_cache(){
        // 初始化栈
        $this->fill_idx_stack();
        $i = 0;
        $start_time = time();
        while(true){
            // 构建并请求url
            $url = $this->build_url();
            //die("curl '$url'\n");
            exec("curl '$url'", $ret);
            if(isset($ret[0]) && is_numeric($ret[0])) { // 请求成功，返回活动个数
                $msg = '';
                if($ret[0] == 0) { // 如果活动为空，直接跳到最后一页
                    $this->move_last_page();
                    $msg = ', 跳到最后一页';
                }
                log_message('info', $url.': '.$ret[0].$msg);
            }else{
                // 请求失败
                log_message('error', "curl '$url' : " .print_r($ret));
            }
            unset($ret);
            $i++;
            // 生成下一个序列
            if(!$this->next_idx_sequeue())
                break;
        }
        $cost_time = time() - $start_time;
        $msg = date('Y-m-d H:i:s')."刷新app活动列表缓存: 请求 $i 条url, 耗时 $cost_time 秒";
        log_message('info', $msg);
        die($msg);
    }

    /**
     * 向右填充0
     */
    protected function fill_idx_stack(){
        $level = count($this->idx_stack);
        $total = count($this->keys);
        for($i = $level; $i < $total; $i++){
            array_push($this->idx_stack, 0);
        }
    }

    /**
     * 生成下一个序列
     */
    protected function next_idx_sequeue(){
        // 构建下一个序列
        $val_count = $this->last_val_count(); // 元素个数
        $idx = array_pop($this->idx_stack); // 出栈，元素索引
        // 尝试下一个索引
        if(++$idx >= $val_count){ // 如果已满，则向左进位
            if(empty($this->idx_stack)) // 进位失败，代表序列迭代完毕了
                return false;

            return $this->next_idx_sequeue();
        }else{ // 否则，入栈
            array_push($this->idx_stack, $idx);
            // 向右填充0
            $this->fill_idx_stack();
            return true;
        }
    }

    /**
     * page直接跳到最后一页
     * 栈顶序号直接跳到最后一个序号
     */
    protected function move_last_page(){
        $level = count($this->idx_stack) - 1; // 从0开始
        $last = count($this->params[$level]) - 1;
        $this->idx_stack[$level] = $last;
    }

    /**
     * 获得栈最后一个元素对应的值的个数
     * @return int
     */
    protected function last_val_count(){
        $level = count($this->idx_stack) - 1; // 从0开始
        return count($this->params[$level]);
    }

    /**
     * 拼接url, 要拼接所有的参数
     * @return string
     */
    protected function build_url()
    {
        $url = config_item('domain_appserver') . '/trys/try_list?client_type=1&version=3.0.1&__refresh_cache_by_timer_from_bug_6168=1';
        foreach ($this->idx_stack as $i => $idx) {
            $key = $this->keys[$i];
            $val = $this->params[$i][$idx];
            $url .= "&$key=$val"; // 拼接单个参数
        }
        return $url;
    }
}