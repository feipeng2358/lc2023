<?php
/**
 * Created by PhpStorm.
 * User: xwn
 * Date: 6/27/17
 * Time: 4:45 PM
 */

/**
 * CAPTCHA管理器
 */
class Captcha_manager
{
	/**
	 * 验证码用途标识
	 * @var mixed|string
	 */
	private $for = '';
	/**
	 * 验证码有效时间
	 * @var int
	 */
	private $valid_time = 1800;
	/**
	 * 验证码id
	 * @var null|string
	 */
	private $captcha_id = null;

	/**
	 * 生成的验证码
	 * @var null
	 */
	private $code = null;
	/**
	 * 是否使用session保存和读取验证码
	 * @var bool|mixed
	 */
	private $use_session = true;
	/**
	 * 刷新验证码频率限制器
	 * @var mixed|null
	 */
	private $show_limiter = null;
	/**
	 * 检查验证码频率限制器
	 * @var mixed|null
	 */
	private $check_limiter = null;

	/**
	 * 验证码渲染器
	 * @var null
	 */
	private $captcha_renderer = null;

	/**
	 * @var null
	 */
	public $error = null;


	/**
	 * captcha_manager constructor.
	 * @param array $config
	 */
	function __construct($config = [])
	{
		$this->for = isset($config['for']) ? $config['for'] : '';
		$this->length = isset($config['length']) ? $config['length'] : 2;
		$this->captcha_id = !empty($config['captcha_id']) ? $config['captcha_id'] : $this->gen_id();
		$this->use_session = isset($config['use_session']) ? $config['use_session'] : true;

		if (isset($config['show_limiter'])) {
			$this->show_limiter = $config['show_limiter'];
		}
		if (isset($config['check_limiter'])) {
			$this->check_limiter = $config['check_limiter'];
		}

		$ci = get_instance();

		if (isset($config['renderer'])) {
			$this->captcha_renderer = $config['renderer'];
		} else {
			$ci->load->library('captcha/Captcha_renderer', isset($config['renderer_config']) ? $config['renderer_config'] : []);
			$this->captcha_renderer = $ci->captcha_renderer;
		}

		if ($this->use_session && !isset($_SESSION)) {
			$ci->load->helper('session');
			start_session();
		}
	}

	/**
	 * @return null|string
	 */
	public function captcha_id()
	{
		return $this->captcha_id;
	}

	/**
	 * 生成验证码id
	 * @return string
	 */
	private function gen_id()
	{
		$chars = '01234556789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$chars_num = strlen($chars);
		$id = ''; // 验证码
		for ($i = 0; $i < 20; $i++) {
			$id .= $chars [mt_rand(0, $chars_num - 1)];
		}
		return $id;
	}

	/**
	 * 生成验证码
	 * @return string 验证码字符串
	 */
	private function gen_code()
	{
		$chars = '2346789abcdefghkmnpqrstuvwxyz';
		$chars_num = strlen($chars);
		$code = ''; // 验证码
		for ($i = 0; $i < $this->length; $i++) {
			$code .= $chars [mt_rand(0, $chars_num - 1)];
		}

		$data = [
			'id' => $this->captcha_id,
			'for' => $this->for,
			'code' => $code,
			'valid_until' => time() + $this->valid_time,
		];

		$this->save($data);

		$this->code = $code;

		return $code;
	}

	public function get_code()
	{
		return $this->code;
	}

	/**
	 * 输出或返回验证码
	 * @param bool $return 是否返回验证码图片
	 * @return bool|resource
	 */
	public function show($return = false)
	{

		if ($this->show_limiter) {
			if ($this->show_limiter->is_exceeded()) {
				$this->error = [
					'code' => 'VCODE_SHOW_LIMIT_EXCEEDED',
					'data' => '您操作过于频繁，请15分钟后再试',
				];
				return false;
			}
			$this->show_limiter->increase();
		}

		$code = $this->gen_code();

		return $this->captcha_renderer->render($code, $return); // 输出或返回图片
	}


	/**
	 * @param $data
	 */
	private function save($data)
	{
		if ($this->use_session) {
			$_SESSION[__CLASS__] = $data;
		} else {
			$cache_key = 'captcha_id_' . $data['id'];
			cache($cache_key, $data, $data['valid_until'] - time());
		}
	}

	/**
	 * @return bool
	 */
	private function load()
	{
		if ($this->use_session) {
			$index = __CLASS__;
			return isset($_SESSION[$index]) ? $_SESSION[$index] : false;
		} else {
			if (!$this->captcha_id) {
				return false;
			}
			$cache_key = 'captcha_id_' . $this->captcha_id;
			return cache($cache_key);
		}
	}

	/**
	 * 删除验证码，避免暴力破解
	 */
	public function remove()
	{
		if ($this->use_session) {
			unset($_SESSION[__CLASS__]);
		} else {
			if ($this->captcha_id) {
				$cache_key = 'captcha_id_' . $this->captcha_id;
				cache($cache_key, false);
			}
		}
	}

	/**
	 * 判断验证码
	 *
	 * @param
	 *            $vcode
	 * @return boolean
	 */
	public function check($vcode)
	{
		if ($this->check_limiter) {
			if ($this->check_limiter->is_exceeded()) {
				$this->remove();
				$this->check_limiter->clear();
				$this->error = [
					'code' => 'VCODE_CHECK_LIMIT_EXCEEDED',
					'data' => '验证码检查次数超限',
				];
				return false;
			}
		}

		$data = $this->load();

		if (
			!$data
			|| $data['for'] !== $this->for
			|| strtolower($data['code']) !== strtolower($vcode)
			|| $data['valid_until'] < time()
		) {
			if ($this->check_limiter) {
				$this->check_limiter->increase();
			}
			return false;
		}

		return true;
	}
}
