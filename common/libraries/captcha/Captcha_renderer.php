<?php
/**
 * 验证码渲染器
 * @author BLUE 
 * @version 2013.9
 */
class Captcha_renderer {
	/**
	 * 配置
	 * @var array
	 */
    private $config = array(
        'useImgBg' => false, // 使用背景图片
        'fontSize' => 20, // 验证码字体大小(px)
        'useCurve' => false, // 是否画混淆曲线
        'useNoise' => false, // 是否添加杂点
        'imageW' => 120, // 验证码图片宽
        'imageH' => 40, // 验证码图片高
        'bg' => array(243, 251, 254), // 背景
    );

	private $_image = null; // 验证码图片实例
    private $_color = null; // 验证码字体颜色
	private $_noiseChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'; // 噪点字符

	public function __construct($config = array()) {
		$this->config = array_merge($this->config, $config);
	}
	
    /**
     * 输出验证码，可选直接返回验证码
     * @param string $code 验证码
     * @param bool $return 是否返回图片
     * @return bool|string 当$return==false时返回bool表示成功与否;
     * 当$return=true时，如果渲染成功将渲染的图片作为二进制字符串返回，否则返回false
     */
    public function render($code, $return = false) {
        $len = mb_strlen($code);
        $useZh = preg_match('/[\x{4e00}-\x{9fa5}]/u', $code);

        // 图片宽(px)
        $iw = $this->config['imageW'] ?: $len * $this->config['fontSize'] * 1.5 + $this->config['fontSize'] * 1.5;
        // 图片高(px)
        $ih = $this->config['imageH'] ?: $this->config['fontSize'] * 2;
        // 创建图片
        $this->_image = imagecreate ( $iw, $ih );
        // 设置背景
        imagecolorallocate ( $this->_image, $this->config['bg'] [0], $this->config['bg'] [1], $this->config['bg'] [2] );

        // 验证码字体随机颜色
        $this->_color = imagecolorallocate ( $this->_image, mt_rand ( 1, 120 ), mt_rand ( 1, 120 ), mt_rand ( 1, 120 ) );
        // 验证码使用随机字体
        $ttfPath = dirname ( __FILE__ ) . '/' . ($useZh ? 'zhttfs' : 'ttfs') . '/';

        $dir = dir ( $ttfPath );
        $ttfs = array ();
        while ( false !== ($file = $dir->read ()) ) {
            if ($file [0] != '.' && substr ( $file, - 4 ) == '.ttf') {
                $ttfs [] = $ttfPath . $file;
            }
        }
        $dir->close ();

        $ttf = $ttfs [array_rand ( $ttfs )];

        if ($this->config['useImgBg']) {
            $this->_background ();
        }

        if ($this->config['useNoise']) {
            // 绘杂点
            $this->_writeNoise ();
        }
        if ($this->config['useCurve']) {
            // 绘干扰线
            $this->_writeCurve ();
        }

        // 绘验证码
        if (! $useZh) {
            $codeNX = 0; // 验证码第N个字符的左边距
            for($i = 0; $i < $len; $i ++) {
                $codeNX += mt_rand ( $this->config['fontSize'] * 1.0, $this->config['fontSize'] * 1.3 );
                // 写一个验证码字符
                imagettftext (
                    $this->_image,
                    $this->config['fontSize'],
                    mt_rand ( - 40, 40 ),
                    $codeNX,
                    $this->config['fontSize'] * 1.5,
                    $this->_color,
                    $ttf, $code [$i]
                );
            }
        } else {
            imagettftext (
                $this->_image,
                $this->config['fontSize'],
                0,
                ($iw - $this->config['fontSize'] * $len * 1.2) / 3,
                $this->config['fontSize'] * 1.5,
                $this->_color,
                $ttf,
                $code
//                iconv ( "GB2312", "UTF-8", $code )
            );
        }

        if ($return) {
            ob_start();
            $image = imagepng ( $this->_image );
            imagedestroy ( $this->_image );
            $ret = $image ? ob_get_contents() : false;
            ob_end_clean();
            return $ret;
        }

        header ( 'Cache-Control: private, max-age=0, no-store, no-cache, must-revalidate' );
        header ( 'Cache-Control: post-check=0, pre-check=0', false );
        header ( 'Pragma: no-cache' );
        header ( "content-type: image/png" );

        // 输出图像
        $ret = imagepng ( $this->_image );
        imagedestroy ( $this->_image );

        return $ret;
    }

	/**
	 * 画一条由两条连在一起构成的随机正弦函数曲线作干扰线(你可以改成更帅的曲线函数)
	 *
	 * 高中的数学公式咋都忘了涅，写出来
	 * 正弦型函数解析式：y=Asin(ωx+φ)+b
	 * 各常数值对函数图像的影响：
	 * A：决定峰值（即纵向拉伸压缩的倍数）
	 * b：表示波形在Y轴的位置关系或纵向移动距离（上加下减）
	 * φ：决定波形与X轴位置关系或横向移动距离（左加右减）
	 * ω：决定周期（最小正周期T=2π/∣ω∣）
	 */
	private function _writeCurve() {
	    $iw = imagesx($this->_image);
        $ih = imagesy($this->_image);

		$px = $py = 0;
		
		// 曲线前部分
		$A = mt_rand ( 1, $ih / 2 ); // 振幅
		$b = mt_rand ( - $ih / 2, $ih / 2 ); // Y轴方向偏移量
		$f = mt_rand ( - $ih / 2, $ih / 2 ); // X轴方向偏移量
		$T = mt_rand ( $ih, $iw * 2 ); // 周期
		$w = (2 * M_PI) / $T;
		
		$px1 = 0; // 曲线横坐标起始位置
		$px2 = mt_rand ( $iw / 2, $iw * 0.8 ); // 曲线横坐标结束位置
		
		for($px = $px1; $px <= $px2; $px = $px + 0.9) {
			if ($w != 0) {
				$py = $A * sin ( $w * $px + $f ) + $b + $ih / 2; // y = Asin(ωx+φ) + b
				$i = ( int ) ($this->config['fontSize'] / 5);
				while ( $i > 0 ) {
					imagesetpixel ( $this->_image, $px, $py + $i, $this->_color ); // 这里(while)循环画像素点比imagettftext和imagestring用字体大小一次画出（不用这while循环）性能要好很多
					$i --;
				}
			}
		}
		
		// 曲线后部分
		$A = mt_rand ( 1, $ih / 2 ); // 振幅
		$f = mt_rand ( - $ih / 4, $ih / 4 ); // X轴方向偏移量
		$T = mt_rand ( $ih, $iw * 2 ); // 周期
		$w = (2 * M_PI) / $T;
		$b = $py - $A * sin ( $w * $px + $f ) - $ih / 2;
		$px1 = $px2;
		$px2 = $iw;
		
		for($px = $px1; $px <= $px2; $px = $px + 0.9) {
			if ($w != 0) {
				$py = $A * sin ( $w * $px + $f ) + $b + $ih / 2; // y = Asin(ωx+φ) + b
				$i = ( int ) ($this->config['fontSize'] / 5);
				while ( $i > 0 ) {
					imagesetpixel ( $this->_image, $px, $py + $i, $this->_color );
					$i --;
				}
			}
		}
	}
	
	/**
	 * 画杂点
	 * 往图片上写不同颜色的字母或数字
	 */
	private function _writeNoise() {
        $iw = imagesx($this->_image);
        $ih = imagesy($this->_image);

        for($i = 0; $i < 10; $i ++) {
			// 杂点颜色
			$noiseColor = imagecolorallocate ( $this->_image, mt_rand ( 150, 225 ), mt_rand ( 150, 225 ), mt_rand ( 150, 225 ) );
			for($j = 0; $j < 5; $j ++) {
				// 绘杂点
				imagestring ( $this->_image, 5, mt_rand ( - 10, $iw ), mt_rand ( - 10, $ih ), $this->_noiseChars [mt_rand ( 0, 27 )], 				// 杂点文本为随机的字母或数字
				$noiseColor );
			}
		}
	}
	
	/**
	 * 绘制背景图片
	 * 注：如果验证码输出图片比较大，将占用比较多的系统资源
	 */
	private function _background() {
        $iw = imagesx($this->_image);
        $ih = imagesy($this->_image);

        $path = dirname ( __FILE__ ) . '/bgs/';
		$dir = dir ( $path );
		
		$bgs = array ();
		while ( false !== ($file = $dir->read ()) ) {
			if ($file [0] != '.' && substr ( $file, - 4 ) == '.jpg') {
				$bgs [] = $path . $file;
			}
		}
		$dir->close ();
		
		$gb = $bgs [array_rand ( $bgs )];
		
		list ( $width, $height ) = @getimagesize ( $gb );
		// Resample
		$bgImage = @imagecreatefromjpeg ( $gb );
		@imagecopyresampled ( $this->_image, $bgImage, 0, 0, 0, 0, $iw, $ih, $width, $height );
		@imagedestroy ( $bgImage );
	}
}
