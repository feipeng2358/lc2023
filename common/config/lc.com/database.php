<?php
$active_group = 'default';
$active_record = TRUE;

// 主数据库（默认）
$db['default']['hostname'] = '43.136.13.254';
$db['default']['database'] = 'lc';
$db['default']['username'] = 'lc_dev';
$db['default']['password'] = 'lc1688';
$db['default']['port'] = 3306;
$db['default']['dbdriver'] = 'mysqli';
$db['default']['dbprefix'] = 'lc_';
$db['default']['pconnect'] = FALSE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = FALSE;
$db['default']['stricton'] = FALSE;

// 从数据库
$db ['slave'] ['hostname'] = '43.136.13.254';
$db ['slave'] ['database'] = 'lc';
$db ['slave'] ['username'] = 'lc_dev';
$db ['slave'] ['password'] = 'lc1688';
$db ['slave'] ['port'] = 3306;
$db ['slave'] ['dbdriver'] = 'mysqli';
$db ['slave'] ['dbprefix'] = 'lc_';
$db ['slave'] ['pconnect'] = FALSE;
$db ['slave'] ['db_debug'] = TRUE;
$db ['slave'] ['cache_on'] = FALSE;
$db ['slave'] ['cachedir'] = '';
$db ['slave'] ['char_set'] = 'utf8';
$db ['slave'] ['dbcollat'] = 'utf8_general_ci';
$db ['slave'] ['swap_pre'] = '';
$db ['slave'] ['autoinit'] = FALSE;
$db ['slave'] ['stricton'] = FALSE;

