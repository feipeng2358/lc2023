<?php
if (php_sapi_name() != 'cli' && ! defined('STDIN')) {
	die ( 'CLI access only' );
}

define ( 'CLI_DIR', dirname ( __FILE__ ) );
define ( 'PROJECT_ROOT', dirname ( CLI_DIR ) );

// use regex to retrive the ENVIRONMENT constant defined in common/root/index.php
$index_php = file_get_contents ( PROJECT_ROOT . '/common/root/index.php' );
$regex = '/^\s*define\s*\(\s*\'ENVIRONMENT\'\s*,\s*\'(.*)\'\s*\)/mu';
if (preg_match ( $regex, $index_php, $matches )) {
	$environment = $matches [1];
} else {
	$environment = 'lc.local';
}

define ( 'CONFIG', $environment );
define ( 'ENVIRONMENT', $environment );

unset ( $index_php, $regex, $matches, $environment );

define ( 'IS_WIN', strncasecmp ( PHP_OS, 'WIN', 3 ) == 0 );

// Detect shell encoding
$shell_encoding = 'utf-8';
if (IS_WIN) {
// Detect windows active code page
	$active_cp = rtrim ( `chcp`, PHP_EOL );
	if (preg_match ( '/\d+$/', $active_cp, $matches )) {
		$shell_encoding = 'cp' . $matches [0];
	}
}
define ( 'SHELL_ENCODING', $shell_encoding );

// Set the current directory correctly for CLI requests
// if (defined('STDIN')) {
//     chdir(dirname(__FILE__));
// }

$system_path = PROJECT_ROOT . '/libs/codeigniter/3.1.11/';

// The name of THIS file
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

// The PHP file extension
// this global constant is deprecated.
define('EXT', '.php');

// Path to the system folder
define('BASEPATH', str_replace("\\", "/", $system_path));

// Path to the front controller (this file)
define('FCPATH', str_replace(SELF, '', __FILE__));

// Path to the front controller (this file)
define('COMPATH', PROJECT_ROOT . '/common/');

// Name of the "system folder"
define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));

// The path to the "application" folder
define('APPPATH', CLI_DIR . '/app/');

$_SERVER['HTTP_HOST'] = 'localhost-cli';
$_SERVER['REQUEST_URI'] = implode(',', $_SERVER['argv']);

$view_folder = '';

if ( ! isset($view_folder[0]) && is_dir(APPPATH.'views'.DIRECTORY_SEPARATOR))
	{
		$view_folder = APPPATH.'views';
	}
	elseif (is_dir($view_folder))
	{
		if (($_temp = realpath($view_folder)) !== FALSE)
		{
			$view_folder = $_temp;
		}
		else
		{
			$view_folder = strtr(
				rtrim($view_folder, '/\\'),
				'/\\',
				DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
			);
		}
	}
	elseif (is_dir(APPPATH.$view_folder.DIRECTORY_SEPARATOR))
	{
		$view_folder = APPPATH.strtr(
			trim($view_folder, '/\\'),
			'/\\',
			DIRECTORY_SEPARATOR.DIRECTORY_SEPARATOR
		);
	}
	else
	{
		header('HTTP/1.1 503 Service Unavailable.', TRUE, 503);
		echo 'Your view folder path does not appear to be set correctly. Please open the following file and correct this: '.SELF;
		exit(3); // EXIT_CONFIG
	}

	define('VIEWPATH', $view_folder.DIRECTORY_SEPARATOR);

require_once BASEPATH . 'core/CodeIgniter.php';
