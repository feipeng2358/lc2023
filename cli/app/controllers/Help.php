<?php
class Help extends Cli_controller {
	public function index($class = '', $method = '') {
        $name = $_SERVER['argv'][0];
        if ($class === '') {
            pl(<<<TEXT
sk命令行工具
使用方法: 
    {$name} <控制器> <方法> [选项] [参数]
    <控制器>、<方法>对应 cli/app/controllers 中的控制器和方法。

可用控制器：
    app_cache
    config
    db
    privilege
    queue
    source
    user
    
获取帮助：
{$name} help <控制器> [方法]
    
使用例子：
    {$name} help db
        获取 'db' 控制器的帮助。
    {$name} help db dump
        获取 'db dump' 方法的帮助。
    {$name} db dump
        调用 db 控制器的 dump 方法（导出数据库结构）。
TEXT
            );
        } else if ($method === '') {
            $file = __DIR__ . '/' . $class . '.php';
            $doc = '';
            if (file_exists($file)) {
                include $file;
                $doc = get_class_help($class);
            }
            pl($doc ?: "找不到关于 '{$class}' 的帮助。");
        } else {
            $file = __DIR__ . '/' . $class . '.php';
            $doc = '';
            if (file_exists($file)) {
                include $file;
                $doc = get_method_help($class, $method);
            }
            pl($doc ?: "找不到关于 '{$class} {$method}' 的帮助。");
        }
	}
}
