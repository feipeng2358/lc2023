<?php
/**
 * 公用方法
 */
class Common extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 获取发送短信图形验证码
     */
    public function sms_captcha()
    {
        $for = $this->_param('for', false);
        $for = trim($for);
        if (!$for) {
            $this->_fail('MISSING_PARAM', '缺少参数：for');
        }
        $this->load->library('captcha/captcha_manager', array(
            'for' => $for,
            'captcha_id' => null,
            'use_session' => false,
            'renderer_config' => array('useCurve' => true),
        ));

        if ($image = $this->captcha_manager->show(true)) {
            $this->_ok(array(
                'sid' => $this->captcha_manager->captcha_id(),
                'image' => base64_encode($image),
            ));
        }

        $this->_fail($this->captcha_manager->error);
    }

}
