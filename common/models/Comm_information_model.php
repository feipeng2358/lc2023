<?php

/**
 * 公共资料模型
 */
class Comm_information_model extends CI_Model
{
	private $slave = NULL;
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->slave = $this->load->database('slave', TRUE);
	}

	//获取文章类型 和 文章列表
	public function get_information_text( $arr)
	{
		$this->slave->from('information_text');
		
		if (!empty($arr['articleTypeName'])) {
			$this->slave->like('articleTypeName', $arr['articleTypeName']);
		}

		if (!empty($arr['year'])) {
			$this->slave->like('year', $arr['year']);
		}
		
		if (!empty($arr['articleTypeId'] )) {
			$this->slave->where('articleTypeId', $arr['articleTypeId']);
			$this->slave->select('*');
		} else {
			$this->slave->select('articleTypeId,articleTypeName,lotteryType');
		}
		$data['list'] = $this->slave->get()->result_array();
		return $data;
	}

	// 获取对应文章详情
	public function get_article_detail($arr)
	{
		$this->slave->from('information_text');
		$this->slave->where('articleId', $arr['articleId']);
		$this->slave->select('*');
		$data['list'] = $this->slave->get()->row_array();
		return $data;
	}

	//获取图库类型列表
	public function get_information_img($arr)
	{
		$this->slave->from('information_img');

		if (!empty($arr['pictureName'])) {
			$this->slave->like('pictureName', $arr['pictureName']);
		}

		if (!empty($arr['year'])) {
			$this->slave->like('year', $arr['year']);
		}

		$this->slave->select('*');

		$data['list'] = $this->slave->get()->result_array();
		return $data;
	}


	// 获取对应图库详情
	public function get_picture_detail($arr)
	{
		$this->slave->from('information_img');
		$this->slave->where('pictureId', $arr['pictureId']);
		$this->slave->select('*');
		$data['list'] = $this->slave->get()->row_array();
		return $data;
	}
}
