<?php

/**
 * 首页接口控制器
 */
class Home extends My_controller {

	public $home_model;

	public function __construct(){
		parent::__construct();
		$this->home_model = $this->load_model('app_home_model');
	}

	/**
	 * 首页
	 */
	public function index(){

		$data = [];
		$this->_ok($data);
	}

}
