<?php

/**
 * shell encoding aware print
 */
function p() {
	$output = implode ( ' ', func_get_args () );
	if (IS_WIN) {
	    if (SHELL_ENCODING !== 'cp65001') {
            $output = iconv ( 'utf-8', SHELL_ENCODING, $output );
        }

	}
	echo $output;
}

/**
 * print line
 */
function pl() {
	call_user_func_array ( 'p', func_get_args () );
	echo PHP_EOL;
}

/**
 * print error info and exit
 */
function err($data = 'unknown error') {
	pl ( 'an error occured.' );
	if (is_array ( $data )) {
		foreach ($data as $key => $val) {
			pl ( "$key: " . ( is_array ( $val ) ? str_replace ( "\n", ' ', print_r ( $val, TRUE ) ) : $val ) );
		}
	} else {
		pl ( $data );
	}
	exit;
}

/**
 * rm -rf
 */
function rmdir_recursive($dir) {
	$files = array_diff(scandir($dir), array('.','..')); 
	foreach ($files as $file) { 
		(is_dir("$dir/$file")) ? rmdir_recursive("$dir/$file") : unlink("$dir/$file"); 
	} 
	return rmdir($dir); 
}

/**
 * remove files matching a pattern
 */
function clear_files($pattern) {
	array_map ( 'unlink', glob ( $pattern ) );	
}

/**
 * detect utf-8 bom
 */
function has_bom($filename) {
	$file = @fopen ( $filename, "r" ); 
	$bom = fread ( $file, 3 ); 
	@fclose ( $file );
	if ($bom != b"\xEF\xBB\xBF") { 
		return FALSE; 
	}
	return TRUE; 
}

/**
 * array_walk() for directories
 */
function dir_walk($dir, $callback, $recursive = TRUE, $excludes = NULL, $pattern = NULL, $file_only = TRUE) {
	$path = rtrim ( $dir, "/" ) . "/"; 
	$folder_handle = opendir ( $path );

	$subdirs = array ();
	while (FALSE !== ( $filename = readdir ( $folder_handle ) )) {
		if ($filename === '.' || $filename === '..') {
			continue;
		}
		if ($excludes && in_array ( $filename, $excludes )) {
			continue;
		}

		$is_dir = is_dir ( $path . $filename . "/" );
		if ($is_dir && $recursive) {
			$subdirs[] = $path. $filename;
		}
		if ($pattern === NULL || preg_match ( $pattern, strtolower ( $filename ) )) {
			if ($is_dir) {
				// Need to include full "path" or it's an infinite loop 
				if (! $file_only) {
					call_user_func ( $callback, $path . $filename . '/', TRUE );
				}
			} else {
				call_user_func ( $callback, $path . $filename, FALSE );
			}
		}
	}

	if ($recursive) {
		foreach ($subdirs as $subdir) {
			dir_walk ( $subdir, $callback, $recursive, $excludes, $pattern, $file_only );
		}
	}
}

function get_class_help($class) {
    if (!($class instanceof ReflectionClass)) {
        try {
            $class = new ReflectionClass($class);
        }
        catch(ReflectionException $e) {
            return;
        }
    }

    $doc = $class->getDocComment();

    $doc = trim($doc);
    $doc = substr($doc, 2, -2);  // remove /* */
    $doc = preg_replace('/^\s*\*\s/m', "",$doc);
    $doc = trim($doc);

    $doc .= "\n可用方法：";
    foreach ($class->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
        if ($method->isConstructor() || $method->isDestructor() || $method->isStatic()) {
            continue;
        }
        $method_brief = get_method_help($class, $method->name);
        $linebreak = strpos($method_brief, "\n");
        if ($linebreak !== false) $method_brief = substr($method_brief, 0, $linebreak);
        $doc .= "\n    " . $method->name . "\t" . $method_brief;
    }

    return $doc;
}

function get_method_help($class, $method) {
    try {
        if (!($class instanceof ReflectionClass)) {
            $class = new ReflectionClass($class);
        }
        $doc = $class->getMethod($method)->getDocComment();
    }
    catch(ReflectionException $e) {
        pl("找不到关于 {$method} 的帮助。");
        return;
    }

    $doc = trim($doc);
    $doc = substr($doc, 2, -2);  // remove /* */
    $doc = preg_replace('/^\s*\*\s/m', "",$doc);
    $doc = trim($doc);

    return $doc;
}
