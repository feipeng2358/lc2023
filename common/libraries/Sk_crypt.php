<?php
/**
 * crypt加/解密方法，包括：
 *  encode/decode (此方法是从淘发现延续过来的双重加/解密）
 * @version 19.10.21
 */
class Sk_crypt{
    protected $key;
    protected $iv;
    
     /**
     * $config['key'] string 密钥
     * $config['iv] string 密钥向量
     */
    function __construct($config)
    {
        $this->key = isset($config['key'])?$config['key']:die('缺少参数：key');
        $this->iv = isset($config['iv'])?$config['iv']:die('缺少参数：iv');
    }

    private function _openssl_encrypt($str)
    {
        $encrypted = openssl_encrypt($str, 'aes-256-cbc', base64_decode($this->key), 1, base64_decode($this -> iv));
        return base64_encode($encrypted);
    }
    
    private function _openssl_decrypt($str)
    {
        $encrypted = base64_decode($str);
        return openssl_decrypt($encrypted, 'aes-256-cbc', base64_decode($this->key), 1, base64_decode($this -> iv));
    }
    
    public function encode($str)
    {
        return self::_openssl_encrypt($str);
    }
    
    public function decode($str)
    {
        return self::_openssl_decrypt($str);
    }
}
