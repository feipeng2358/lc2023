<?php
/**
 * 页面 文案 配置文件
 *
 * 视图、页面展示的，使用 ：page_xx
 * 代码直接返回给用户端的 ：text_xx
 *
 */
$config ["lang"]  =  array(
    'project_name' => 'lc',
    'page_keywords' => '这里是keywords',
    'page_description' => '这里是page_description',
    'page_copyright' => 'Copyright © '.date('Y').' www.lc.com *****公司 版权所有 京ICP备2023008888号',
    'page_copyright_m' => 'Copyright © '.date('Y'). '   www.lc.com 版权所有',

    'page_title' => '这里是title',//通用页面的标题
    'page_title_404' => '页面不存在',//404 的标题
    'page_title_500_m' => '错误提示', //移动端 500 错误title

    'page_contact_mail'=>''//联系方式，邮箱
);
?>
