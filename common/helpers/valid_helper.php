<?php
/**
 * 验证帮助方法，验证是否是合法的手机号，邮箱等
 */

/**
 * 是否是合法的手机号码
 * @param  number  $phone_number [description]
 * @return boolean               [description]
 */
function is_mobile_phone($phone_number){
	 if (preg_match('/^(86){0,1}1[3-9]{1}\d{9}$/', $phone_number)){
	 	 return true;
	 }
	 return false;
}

/**
 * 是否是合法的电子邮箱地址
 * @param  [type]  $email [description]
 * @return boolean        [description]
 */
function is_email($email){
  if(filter_var($email, FILTER_VALIDATE_EMAIL)===false){
  	return false;
  }
  return true;
}

/**
 * 是否是合法的url
 * @param  [type]  $url [description]
 * @return boolean        [description]
 */
function is_url($url) {
    $replace_func = function($matches) {
        return rawurlencode($matches[0]);
    };

    // 对所有非ascii字符进行url encode
    $url_encoded = preg_replace_callback('/[^\x00-\x7F]+/', $replace_func, $url);
    return filter_var($url_encoded, FILTER_VALIDATE_URL);
}

/**
 * 获取URL中所给参数的值
 * @param string $url
 * @param string $param_name
 * @return string|bool 参数存在时返回该参数的值，否则返回false
 */
function get_url_param($url, $param_name){
    $p_url = parse_url($url);
    $query = $p_url && isset($p_url['query']) && $p_url['query'] ? $p_url['query']:'';
    if (!$query) {
        return false;
    }
    $params = array();
    parse_str($query, $params);
    if (!array_key_exists($param_name, $params)) {
        return false;
    }
    return $params[$param_name];
}

/**
 * 判断所传参数是否是正整数
 */
function is_positive_int($var){
	return is_numeric($var) && $var >=1 && round($var)==$var;
}

/**
 * 验证是否合法的QQ号码
 * @param $var
 * @return bool
 */
function is_qq_num($var){
    if(! preg_match('/^[1-9][0-9]{4,}$/', $var)){
        return false;
    }
    return true;
}

/**
 * 是否有重复项
 * @param $lists
 * @param $check_field
 * @return bool
 */
function is_repeated_item($lists, $check_field){
    if(empty($lists) || !is_array($lists) || empty($check_field))
        return false;

    if(is_string($check_field))
        $check_field = [$check_field];

    foreach($lists as $key => $item){
        foreach($lists as $i => $val){
            if($key == $i)
                continue;

            //匹配次数
            $match = 0;
            foreach($check_field as $field){
                if(isset($val[$field]) && $val[$field] == $item[$field])
                    $match++;
            }

            //全部匹配
            if($match == count($check_field))
                return true;
        }
    }

    return false;

}
