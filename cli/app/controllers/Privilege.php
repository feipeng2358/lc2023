<?php
class Privilege extends Cli_controller {
	public function __construct()
	{
		parent::__construct ();
	}

	protected function _load_config() {
		require_once(PROJECT_ROOT . '/sites/xadmin/config/privilege.php');
		return $config;
	}

	protected function _get_source_code($path, $rm) {
		$lines = explode("\n", $this->_load_file($path));
		return join("\n", array_slice($lines, $rm->getStartLine(), $rm->getEndLine() - $rm->getStartLine()));
	}

	protected function _check_variant($source_code, $variant) {
		// $regex = '/\W(has|check)_priv\s*\([^,]+,\s*\'' . $variant . '\'/';
		$regex = '/\W(has|check)_priv\s*\([^,]+,\s*\'\w+/';
		if (! preg_match($regex, $source_code)) {
			return false;
		}
		return true;
	}

	protected function _load_file($path, $ns = false, $use = '') {
		static $files = array();

		if (! isset($files[$path]) && $ns) {
			$file_content = rtrim(file_get_contents($path));

			$open = "namespace $ns { $use ?>";
			$close = substr($file_content, -2) === '?>' ? '<?php }' : '}';

			$file_content = $open . $file_content . $close;

			$files[$path] = $file_content;
			eval($file_content);
		}

		return $files[$path];
	}

	/**
	 * 加载管理后台控制器类到一个特殊的命名空间以避免类名冲突
	 *
	 * @param string $path 文件路径
	 * @param string $class 原类名
	 * @return string 返回新的类名
	 */
	protected function _load_admin_controller($path, $class) {
		$base_ns = '命名空间为中文应该不会有冲突了吧';
		static $flag = false; // 基类是否已加载
		static $use = ''; // 基类use语句
		static $count = 0; // 命名空间后缀，每个控制器加载到一个单独的命名空间以避免类名冲突
		static $required = array(); // 已加载的路径 => 类名

		if (! $flag) {
			// 加载基类
			eval("namespace $base_ns { class CI_Controller {} }");
			$use .= "use $base_ns\\CI_Controller;";

			$this->_load_file(PROJECT_ROOT . '/sites/xadmin/core/Admin_Controller.php', $base_ns, $use);

			foreach (array('Admin_Controller', 'Rcommend') as $base_class) {
				$use .= "use $base_ns\\$base_class;";
			}
			$flag = true;
		}

		if (! isset($required[$path])) {
			$ns = $base_ns . $count;

			$this->_load_file($path, $ns, $use);

			$required[$path] = $ns . '\\' . $class;
			$count++;
		}

		return $required[$path];
	}

	/**
	 * 检查code是否有效
	 * @param string $code 要检查的code
	 * @return bool
	 */
	protected function _check_code_valid($code, $check_variant) {
		list($path, $variant) = array_pad(explode('~', $code), 2, '');
		$segments = explode('-', $path);
		$len = count($segments);
		$basepath = PROJECT_ROOT . '/sites/xadmin/controllers/';
		if (file_exists($basepath . join('/', $segments))) {
			if ($variant) {
				pl('指向目录的权限方法不应有变体：' . $code);
			}
		}
		else if (file_exists($basepath . join('/', $segments) . '.php')) {
			if ($variant) {
				pl('指向控制器的权限方法不应有变体：' . $code);
			}
		}
		else if (file_exists($basepath . join('/', array_slice($segments, 0, -1)) . '.php')) {
			// require_once($basepath . join('/', array_slice($segments, 0, -1)) . '.php');
			$class = $segments[$len - 2];
			$method = $segments[$len - 1];
			$path = $basepath . join('/', array_slice($segments, 0, -1)) . '.php';
			$class = $this->_load_admin_controller($path, $class);
			$rc = new ReflectionClass($class);
			try {
				$rm = $rc->getMethod($method);
				if ($check_variant && $variant) {
					$source_code = $this->_get_source_code($path, $rm);
					if (! $this->_check_variant($source_code, $variant)) {
						pl('源代码没有检查权限方法中的变体：' . $code);
					}
				}
			} catch (ReflectionException $e) {
				pl('权限方法指向的方法不存在：' . $code);
			}
		}
		else {
			pl('权限方法指向的目录/控制器不存在：' . $code);
		}
	}

	protected function _handle_action($actions, $level = 1, $check_variant = false) {
		$ids = array_column($actions, '_id_');
		foreach ($actions as $action) {
			if (isset($action['codes'])) {
				if ($level <= 2) {
					pl("第{$level}层不应出现权限方法：" . $action['_id_']);
				}
				foreach ($action['codes'] as $code) {
					$this->_check_code_valid($code, $check_variant);
				}
			}
			if (isset($action['children'])) {
				if ($level == 3) {
					pl("第{$level}层不应出现children：" . $action['_id_']);
				}
				$ids = array_merge($ids, $this->_handle_action($action['children'], $level + 1, $check_variant));
			}
		}
		return $ids;
	}

    /**
     * 检查权限配置文件的正确性
     * 选项：
     *     -v 开启检查方法变体
     */
	public function check() {
        $check_variant = $this->_option ( ['check-variant', 'v'], false );

		$config = $this->_load_config();
		$ids = array();
		$action_ids = $this->_handle_action($config, 1, $check_variant);

		foreach (array_count_values($action_ids) as $action_id => $count) {
			if ($count > 1) {
				pl('重复id：' . $action_id);
			}
		}
	}
}
