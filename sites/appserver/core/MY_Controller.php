<?php


class MY_Controller extends CI_Controller
{
    /** 过滤器 */
    protected $_filters = array(/* 'login' */);

    /** 当前登录的用户 */
    protected $_user = NULL;
    // 登录签名
    protected $_sign = "";

    public $error;

    // 客户端版本
    protected $_client_version;
    // 要请求的接口版本
    protected $_api_version;

    /**
     * 构造函数
     */
    public function __construct()
    {
        parent::__construct();
        $this->_client_version = trim($this->_param('version'));
        $this->_api_version = trim($this->_param('v')) ?: $this->_client_version;
        $client_type = $this->_param('client_type'); //设备类型,1:安卓,2:IOS,3:小程序

        if (($client_type && !in_array($client_type, array(1, 2, 3))) ||
            ($this->_api_version && !preg_match('/^\d+\.\d+(\.\d+)?$/', $this->_api_version))
        ) {
            $this->_fail('VERSION_ERROR', '错误的版本，请您升级或重新安装~');
        }
    }

    /**
     * 将当前客户端版本于指定版本号比较
     * @param string $opt 比较操作符，如><=
     * @param string $version 比较目标版本号
     * @return bool
     */
    protected function _compare_version($opt, $version)
    {
        return version_compare($this->_api_version, $version, $opt);
    }

    /**
     * 获得get参数
     * @param string $index
     * @param string $default
     * @return string
     */
    protected function _param($index = NULL, $default = NULL, $required = false)
    {
        $val = $this->input->get_post($index);

        if ($val === NULL || $val === FALSE) {
            if ($required) {
                $this->_fail('MISSING_PARAM', '缺少参数：' . ($index ? $index : '参数不足') . '。');
            }
            return $default;
        }

        return $val;
    }

    /**
     * 获得int参数
     * @param string $index
     * @param number $default
     * @return int
     */
    protected function _param_int($index = NULL, $default = 0)
    {
        $val = $this->_param($index, $default);

        if (!is_numeric($val)) {
            $this->_fail('INVALID_PARAM', "参数 $index 不是数值");
        }

        return $val;
    }

    /**
     * json化返回结果
     * @param string $code
     * @param string $message
     * @param unknown $data
     */
    function _json_result($code, $message = '', $data = array())
    {
        header('Content-type:application/json;charset=utf-8');
        return json_encode(array('code' => $code, 'msg' => $message, 'data' => $data ? $data : array()));
    }

    /**
     * 返回成功信息
     * @param string $message
     * @param string $data
     */
    protected function _ok($data = array(), $msg = '')
    {
        die($this->_json_result('OK', $msg, $data));
    }

    /**
     * 返回失败消息
     *
     * 参数说明：
     * 无参：返回未知错误；
     * 一个数组：以模型类的标准错误构造返回结果；
     * 其他情况：第一个参数为 code， 第二个参数为 msg， 第三个参数为 data。
     */
    protected function _fail()
    {
        $args = func_get_args();
        $len = count($args);
        if ($len == 0 || $args[0] === NULL) {
            die($this->_json_result('UNKNOWN_ERROR', '未知错误。', ''));
        }
        if ($len == 1 && is_array($args[0])) {
            die($this->_json_result(
                isset($args[0]['code']) ? $args[0]['code'] : 'UNKNOWN_ERROR',
                isset($args[0]['data']) ? $args[0]['data'] : '未知错误。',
                ''
            ));
        }
        list($code, $msg, $data) = array_pad($args, 3, '');
        die($this->_json_result($code, $msg, $data));
    }

    /**
     * 获得分页信息
     * @param int $total_rows
     * @return array
     */
    protected function _pagination($total_rows)
    {
        $per_page = $this->_param_int('per_page', 10); // 每页记录数量
        $page = $this->_param_int('page', 1); // 页码
        $offset = $per_page * ($page - 1); // 偏移
        $total_pages = ceil($total_rows / $per_page); // 总页数

        $per_page = min($per_page, 20); //限制

        return array(
            'per_page' => $per_page,
            'page' => $page,
            'offset' => $offset,
            'total_pages' => $total_pages,
            'total_rows' => $total_rows,
        );
    }

    /**
     * 根据版本号引入model
     * @param $model
     * @param $version
     */
    protected function load_model($model)
    {
        $this->load->model($model);
        return $this->$model;
    }

}
