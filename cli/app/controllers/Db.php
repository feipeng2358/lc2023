<?php

/**
 * 数据库相关功能。
 */
class Db extends Cli_controller {
    public function __construct() {
        parent::__construct ();
    }

    protected function _get_config($group = null) {
        $file = PROJECT_ROOT . '/common/config/' . ENVIRONMENT . '/database.php';
        include $file;
        isset ( $db, $active_group, $db [$active_group] ) or err ( '数据库配置不正确:' . $file );
        $group and $active_group = $group;

        return $db [$active_group];
    }

    /**
     * list objects
     */
    protected function _list($object_type) {
        $sqls = [
            'table' => 'SHOW TABLE STATUS',
            'procedure' => "SHOW PROCEDURE STATUS WHERE `Db`=DATABASE()",
            'function' => "SHOW FUNCTION STATUS WHERE `Db`=DATABASE()",
            'trigger' => 'SHOW TRIGGERS',
            'event' => 'SHOW EVENTS',
        ];
        if (! isset ( $sqls [$object_type] )) return FALSE;
        return $this->db->query ( $sqls [$object_type] )->result_array ();
    }

    /**
     * list object names
     */
    protected function _list_names($object_type) {
        $column_names = [
            'table' => 'Name',
            'procedure' => 'Name',
            'function' => 'Name',
            'trigger' => 'Trigger',
            'event' => 'Name',
        ];
        if (! isset ( $column_names [$object_type] )) return FALSE;
        return array_column ( $this->_list($object_type), $column_names [$object_type] );
    }

    /**
     * get create sql
     */
    protected function _get_create($object_type, $object_name) {
        $column_names = [
            'table' => ['Create Table', 'Create View'],
            'procedure' => 'Create Procedure',
            'function' => 'Create Function',
            'trigger' => 'SQL Original Statement',
            'event' => 'Create Event',
        ];

        if (! isset ( $column_names [$object_type] )) return FALSE;
        $object_name = $this->db->protect_identifiers ( $object_name );
        $sql = "SHOW CREATE " . strtoupper ( $object_type ) . " {$object_name}";
        $create = $this->db->query ( $sql )->row_array ();
        if ($create) {
            if ($object_type === 'table') {
                if (isset ( $create [$column_names['table'][0]] )) {
                    // 表
                    $result = $create [$column_names['table'][0]];
                    $result = preg_replace ( '/ \bAUTO_INCREMENT=\d+\b /u', ' ', $result );
                } else {
                    // 视图
                    $result = $create [$column_names['table'][1]];
                }
            } else {
                $result = $create [$column_names [$object_type]];
            }
            $result = str_replace ( "\r\n", "\n", $result );
            return $result;
        }
        return FALSE;
    }

    /**
     * put content into file
     */
    protected function _put($filename, $content, $overwrite = TRUE) {
        return file_put_contents ( $filename, $content, $overwrite ? NULL : FILE_APPEND );
    }

    /**
     * 导出数据库结构。包括表、视图、存储过程、函数、触发器和事件。
     * 选项：
     *     -g <group> 指定使用哪一组数据库配置
     *     -h <hostname> 指定连接主机名，覆盖配置文件中的值
     *     -d <database> 指定连接数据库，覆盖配置文件中的值
     *     -u <username> 指定连接用户名，覆盖配置文件中的值
     *     -p <password> 指定连接密码，覆盖配置文件中的值
     *     -o <path> 指定输出目录
     */
    public function dump() {
        // 数据库配置组名
        $group = $this->_option ( ['group', 'g'], null );
        // 获取数据库配置
        $config = $this->_get_config($group);
        // 获取命令行选项，覆盖数据库配置
        $config['hostname'] = $this->_option ( ['hostname', 'h'], $config['hostname'] );
        $config['database'] = $this->_option ( ['database', 'd'], $config['database'] );
        $config['username'] = $this->_option ( ['username', 'u'], $config['username'] );
        $config['password'] = $this->_option ( ['password', 'p'], $config['password'] );

        // 输出目录，默认为项目根目录下的 db/<数据库名>/
        $output_directory = $this->_option ( ['output-directory', 'o'], PROJECT_ROOT . '/db/' . $config ['database'] );
        $output_directory = rtrim($output_directory, '/');

        $object_types = array (
            /* type => output directory */
            'table' => $output_directory . '/tables', // tables and views
            'procedure' => $output_directory . '/procedures',
            'function' => $output_directory . '/functions',
            'trigger' => $output_directory . '/triggers',
            'event' => $output_directory . '/events',
        );

        // load database
        $this->load->database ($config, 'db');

        foreach ($object_types as $object_type => $dir) {
            $names = $this->_list_names ( $object_type );
            if (is_dir ( $dir )) {
                clear_files ( $dir . '/*.sql' );
            } else {
                $names and mkdir ( $dir, 0777, TRUE );
            }
            if ($names) {
                foreach ($names as $name) {
                    $this->_put ( $dir . '/'. $name . '.sql', $this->_get_create ( $object_type, $name ) );
                }
            }
        }
    }

    /**
     * 导出数据库必要数据。
     * 选项：
     *     -g <group> 指定使用哪一组数据库配置
     *     -h <hostname> 指定连接主机名，覆盖配置文件中的值
     *     -d <database> 指定连接数据库，覆盖配置文件中的值
     *     -u <username> 指定连接用户名，覆盖配置文件中的值
     *     -p <password> 指定连接密码，覆盖配置文件中的值
     *     -o <path> 指定输出目录
     */
    public function dump_data() {
        // 数据库配置组名
        $group = $this->_option ( ['group', 'g'], null );
        // 获取数据库配置
        $config = $this->_get_config($group);
        // 获取命令行选项，覆盖数据库配置
        $config['hostname'] = $this->_option ( ['hostname', 'h'], $config['hostname'] );
        $config['database'] = $this->_option ( ['database', 'd'], $config['database'] );
        $config['username'] = $this->_option ( ['username', 'u'], $config['username'] );
        $config['password'] = $this->_option ( ['password', 'p'], $config['password'] );

        // 输出目录，默认为项目根目录下的 db/<数据库名>/
        $output_directory = $this->_option ( ['output-directory', 'o'], PROJECT_ROOT . '/db/' . $config ['database'] . '/data' );
        $output_directory = rtrim($output_directory, '/');

        // 表->筛选条件
        $tables = array(
            'coin_config' => array('where' => array(), 'order_by' => 'key asc'),
            'lt_config' => array('where' => array(), 'order_by' => 'key asc'),
            'system_config' => array('where' => array(), 'order_by' => 'key asc'),
            'system_config_business' => array('where' => array(), 'order_by' => 'key asc'),
        );

        // load database
        $this->load->database ($config, 'db');

        if (is_dir ( $output_directory )) {
            clear_files ( $output_directory . '/*.php' );
        } else {
            mkdir ( $output_directory, 0777, TRUE );
        }
        foreach ($tables as $table => $conds) {
            empty($conds['where']) or $this->db->where($conds['where']);
            empty($conds['order_by']) or $this->db->order_by($conds['order_by']);
            $rows = $this->db->select('*')->get($table)->result_array();
            if ($rows) {
                $content = "<?php\n";
                $content .= "return array(\n";
                foreach ($rows as $row) {
                    $content .= var_export($row, true) . ",\n";
                }
                $content .= ");\n";
                $this->_put ( $output_directory . '/'. $table . '.php', $content );
            }
        }
    }

    //mysql json 字段写入test
    function json_insert(){

        $this->db->query(
            "insert into sk_join_img_json (jid, uid, dateline, img, img_text) values ('1', '2', {time()},123 )"
        );
        exit();
        $this->db->insert('sk_join_img_json', array(
            'jid' =>1 ,
            'uid' =>2 ,
            'dateline' =>time() ,
            'img' =>3 ,
            'img_text' => json_encode(
                [
                    'shop_name'=>'店铺名',
                    'trade_no'=>123123123,
                    'order_actual_money'=>'1231231.00'
                ]
            )
        ));
        echo $this->db->last_query();

    }
}
