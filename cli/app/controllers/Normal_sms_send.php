<?php

/**
 * 作用: 普通发送短信
 * 运行：php tfx normal_sms_send send
 *
 */
class Normal_sms_send extends Cli_controller
{
	public function __construct()
	{
		parent::__construct();
		// 获得db
		$dsn = 'mysqli://root:123456@192.168.0.17/shikee_online';
		$this->db1 = $this->load->database($dsn, TRUE);
//		$this->db1 = $this->load->database('default', TRUE);
	}

	public function index()
	{
		echo '命令：				
	php tfx normal_sms_send send ';
		exit;
	}

	public function test($type = 'default')
	{
		echo 'test:', $type;
	}

	public function send()
	{
		set_time_limit(0);

		$stop_flag = 0;

		$doing_id = 0;
		$doing_count = 0;
		$doing_fail_arr = [];
		$doing_max = 10;
		while (!$stop_flag) {
			try {
				$this->db1->trans_begin();

				$table = 'sk_cli_sms_send_20210526';
				$sms_sql = 'SELECT * FROM  ' . $table . ' WHERE status in (0) ';
				if ($doing_fail_arr) {
					$sms_sql .= ' and id not in (' . join(',', $doing_fail_arr) . ') ';
				}
				$sms_sql .= ' LIMIT 1 FOR UPDATE ';
				echo $sms_sql . PHP_EOL;
				$sms = $this->db1->query($sms_sql)->row_array();

				if (!$sms) {
					$this->db1->trans_rollback();
					$stop_flag = 1;
					break;
				}

				$doing_count = ($doing_id == $sms['id']) ? $doing_count + 1 : 0;
				$doing_id = $sms['id'];

				//模板ID
				$temp_id = 965102;
				$content = []; //$sms['content']
				$mobile = isset($sms['tel']) && $sms['tel'] ? $sms['tel'] : '';
				if (!$mobile) {
					$this->db1->trans_rollback();
					$doing_fail_arr[$doing_id] = $doing_count;
					continue;
				}

				//确认发送短信
				$this->load->library("sk_ccp_rest_sdk");
				$send_ret = $this->sk_ccp_rest_sdk->sendTemplateSMS($mobile, $content, $temp_id);
				echo 'send：' . print_r($send_ret, true) . PHP_EOL;
				if ($send_ret->statusCode != 0) {
					if ($doing_count >= $doing_max) {
						$doing_fail_arr[$doing_id] = $doing_count;
					}
					echo 'ERROR-----------------------------' . PHP_EOL;
					$this->db1->where(['id' => $sms['id']])->update($table, ['status' => 3, 'result' => print_r($send_ret, true)]);

				} else {
					echo 'SUCCESS-----------------------------' . PHP_EOL;
					$this->db1->where(['id' => $sms['id']])->update($table, [
						'status' => 2,
						'result' => print_r($send_ret, true),
						'send_time' => time(),
					]);
				}
				$this->db1->trans_commit();

			} catch (\Exception $exception) {
				$this->db1->trans_rollback();
				echo 'Exception:' . $exception->getMessage();
			} finally {
				unset($sms, $send_ret);
			}
		}

		echo 'fail：' . print_r($doing_fail_arr, true) . PHP_EOL;

	}

}
