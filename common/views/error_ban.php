<?php
header("Content-type: text/html; charset=utf-8");
include COMPATH.'config/'.ENVIRONMENT. '/lc.php';
include COMPATH.'config/'.ENVIRONMENT.'/lang.php';
?>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" type="image/ico" href="<?php echo $this->config->item('domain_static'); ?>common/img/favicon.ico" />
<title></title>
 <style type="text/css">
        body,h1,h2,h3,h4,h5,h6,p,dl,dt,dd,ul,li,img,form,button,input{ margin: 0; padding: 0; }
        html,body{height: 100%; font: 12px/1.5 tahoma, \5b8b\4f53, arial; background-color:#FBFBFB;background:#FBFBFB;}/*font-family:\5fae\8f6f\96c5\9ed1;*/
        h1 { font-size: 100%; font-weight: normal; }
        a { text-decoration: none; color: #3e3e3e; }
        a:hover { text-decoration: underline; color: #c92a33; }
        /*==main==*/
        .header{background: url(<?php echo @$config['domain_static'] ?>common/img/404/header-bg.png) repeat-x; height: 30%; min-width: 1000px;}
        .Prompt h1,.back-home,.back-previous,.Phone,.Mail{background: url(<?php echo @$config['domain_static'] ?>common/img/404/bg.png) no-repeat;}
        .Prompt{ width: 510px; margin: auto; position: relative; line-height: 24px}
        .Prompt h1{ font-size: 24px; color: #5d5050; font-family:\5fae\8f6f\96c5\9ed1; background-position:-323px -237px; margin-bottom:15px; padding: 10px 0 10px 60px; height: 22px; }
        .cause{ font-size: 14px; color: #555;}
        .cause span{ color: #de402a; margin: 0 5px;}
        .link{ color: #666;margin-bottom: 20px;}
        .btn-p{ text-align: center;}
         a.btn-red,a.btn-red:hover{ cursor: pointer; height: 37px; padding: 6px 15px; color: #ffffff; font-family: \5fae\8f6f\96c5\9ed1; font-size: 16px; -moz-border-radius: 3px; -webkit-border-radius: 3px; border-radius: 3px;background-color: #ee5e4a; text-indent: none;}
        /*==footer==*/
        .footer{ height: 44px; width: 100%;min-width: 1000px; background:#EAEAEA; line-height: 44px; position: absolute; bottom:0;border-top: dotted 1px #d4d3d3; color: #858585;}
        .footer a{color: #666; padding: 0 10px; height: 35px;}
        .Copyright{ padding-left:15px;float: left;}
        .Contact{ padding-right:15px; padding-left: 32px;float: right;}
        .Contact strong{ padding:10px 25px; font-weight: normal;}
        .Phone{background-position:-175px -222px;}
        .Mail{background-position:-175px -260px; }
        .tips{ color: #fff; font-size:38px;left: 83px; position: absolute; top: 24px; width: 200px; text-align: center; font-weight:700; font-family: arial \5b8b\4f53 }
       
        /*==END footer==*/
        </style>
</head>

<body>
<div class="header"></div>
<div class="Prompt">
    <h1>您的账号已被系统锁定</h1>
    <p class="cause">您好，您的账号：<span><?php echo $name?></span>  因为：<span><?php echo $lock['reason_ban']?></span> 已被系统锁定，不能继续执行此项操作。</p>
    <p class="link">如有疑问，可发送邮件至：<?php echo $config['lang']['page_contact_mail'];?>  咨询。</p>
    <p class="btn-p">
     <a href="<?php echo $config['domain_www']?>" class="btn-red">返回首页</a>
     </p>
</div>


<div class="footer">
        <span class="Copyright">
       <?php echo $config['lang']['page_copyright'];?>
        </span>
</div>
</body>
</html>
