<?php
/**
 * 全站所使用的常见公共方法
 */

/**
 * 生成随机字符串
 * @param int $length
 * @return string
 */
function rand_code($length = 6) {
    // 密码字符集，可任意添加你需要的字符
    $chars = '123456789';
    $password = '';
    for($i = 0; $i < $length; $i ++) {
        $password .= $chars [mt_rand ( 0, strlen ( $chars ) - 1 )];
    }
    return $password;
}

/**
 * 生成随机字符串
 * @param int $randLength 长度
 * @return string
 */
function rand_str($randLength = 6){
    $chars = 'abcdefghijklmnopqrstuvwxyz123456789';
    $len = strlen($chars);
    $randStr = '';
    for ($i = 0; $i < $randLength; $i++) {
        $randStr .= $chars[mt_rand(0, $len - 1)];
    }
    return $randStr;
}

/**
 * 自定义的strlen, utf8的中文字符算两个
 * @param $str  字符串
 * @return int
 */
function custom_str_length($str,$seize=2){
    $length = 0;
    preg_match_all("/./us",$str,$arr);
    foreach($arr[0] as $v){
        $length += preg_match('#^['.chr(0x1).'-'.chr(0xff).']$#',$v) ? 1 : $seize;
    }
    return $length;
}

//获取分配给各平台的秘钥
function get_site_sign ($site){
    $site_config = array(
        ZHS_SITE => KEY_ZHS,
    );
    return isset($site_config[$site]) ? $site_config[$site] : false;

}

/*
 * 获取系统能够识别的换行符
 */
function get_line_break_for_system(){
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        return "\r\n";
    } else {
        return "\n";
    }
}

/**
 * 不做四舍五人的保留小数
 *
 * @param [type] $float
 * @param integer $accuracy 精度，保留n为小数
 * @return float
 */
function slice_float($float,$accuracy=2)
{
    $str_ret = 0;
    if (empty($float) === false) {
        $str_ret = sprintf("%.".$accuracy."f", substr(sprintf("%.".($accuracy+1)."f", floatval($float)), 0, -1));
    }

    return floatval($str_ret);
}


if ( ! function_exists('array_any')) {
    function array_any(array $input, $callback = null)
    {
        foreach ($input as $it){
            if(call_user_func($callback, $it))
                return $it;
        }
        return null;
    }
}

if(!function_exists('num2cn')){
    /**
     * 数字转中文 如101转成一百零一
     * @param $number
     * @return mixed|string
     */
    function num2cn($number) {
        $number = intval ( $number );
        $capnum = array (
            "零",
            "一",
            "二",
            "三",
            "四",
            "五",
            "六",
            "七",
            "八",
            "九"
        );
        $capdigit = array (
            "",
            "十",
            "百",
            "千",
            "万"
        );

        $data_arr = str_split ( $number );
        $count = count ( $data_arr );
        for($i = 0; $i < $count; $i ++) {
            $d = $capnum [$data_arr [$i]];
            $arr [] = $d != '零' ? $d . $capdigit [$count - $i - 1] : $d;
        }
        $cncap = implode ( "", $arr );

        $cncap = preg_replace ( "/(零)+/", "0", $cncap ); // 合并连续“零”
        $cncap = trim ( $cncap, '0' );
        $cncap = str_replace ( "0", "零", $cncap ); // 合并连续“零”
        $cncap == '一十' && $cncap = '十';
        $cncap == '' && $cncap = '零';

        return $cncap;
    }
}

/**
 * 驼峰转小写
 * @param $subject
 * @param string $separator
 * @return string
 */
function string_hump_to_underscore($subject, $separator = '_')
{
	return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $subject));
}

/**
 * 重建数据 - 键名修改为下划线
 * @param $data
 * @return string
 */
function array_hump_to_underscore($data)
{
	if (is_string($data))
		return string_hump_to_underscore($data);

	foreach ($data as $key => $item)
	{
		unset($data[$key]);
		$key = string_hump_to_underscore($key);

		if (is_array($item))
			$item = array_hump_to_underscore($item);

		$data[$key] = $item;
	}

	return $data;
}
