<?php
/**
 * 全站所使用的常见公共方法
 *
 * 备注：
 *  1.建议默认载入此文件，加快网站开发速度。
 *  2.此文件的方法以精简为主，避免过于臃肿而造成系统缓慢及维护困难。
 */

/**
 * 获取登录或指定用户信息
 * 依赖类库：crypt/cache_memcached
 * 依赖常量：COOKIE_NAME/COOKIE_DOMAIN/KEY_COOKIE_CRYPT/KEY_COOKIE_CRYPT_IV
 *
 * @param
 *        	uid 想要获取的用户编号，留空则为获取当前登录用户
 * @param
 *        	user_cache 是否使用缓存，如果设为false，同时意味着更新缓存
 * @return 成功返回用户信息数组array(id,name,type,status,is_active,bind,[illegal_reason])，找不到则返回false type:0（管理员）、1（买家）、2（商家）
 *         status：0正常；1屏蔽；2封号；
  *        bind:绑定其他平台，0 未绑定 1 微信 2 微信公众号 4 qq 8 微博，可位操作，表示同时绑定多个账号
 *         is_active:是否已激活
 *         当用户状态不为0时，会有以下字段
 *         is_auto_block：bool；是否是自动屏蔽
 *         auto_block_type：自动屏蔽类型；1为审批单号有误
 *         illegal_reason:屏蔽或封号的原因（仅当status不为0时存在）
 *        
 */
function get_user($uid = NULL, $use_cache = TRUE) {
	$ci = &get_instance ();
    $cache_exipre_sec = 86400; // 缓存一天
	// 1 如果$uid为空，则从cookie中读取当前用户
	if (! $uid) {
	    // 1.1 读取cookie中的cl字段, 类似session_id
		$cookie = isset ( $_COOKIE [COOKIE_NAME] ) ? $_COOKIE [COOKIE_NAME] : '';
		if (! $cookie) {
			return FALSE;
		}

		// 1.2 直接从缓存读cookie对应的用户: 缓存key为 'user_cookie_' . $cookie
		$cache_key_cookie = 'user_cookie_' . $cookie;
		if ($use_cache) {
			$user = cache ( $cache_key_cookie );
			if ($user) {
				return $user;
			}
		}

        // 1.3 如果没有缓存，则解密cl字段： 没有校验随机串，没有过期检查
		$decode_cookie = decode_cookie( $cookie, $cache_exipre_sec );
		if( !$decode_cookie ){
			return false;
		}else{
			$uid = $decode_cookie;
		}
	}

	// 2 如果$uid不为空，则直接获得用户
    // 2.1 从缓存中读指定uid对应的用户： 缓存key为 'user_' . $uid
	$uid = intval ( $uid );
	$cache_key_uid = 'user_' . $uid;
	if ($use_cache) {
		$user = cache ( $cache_key_uid );
		if ($user) {
			return $user;
		}
	}
	
	// 2.2 如果没有缓存，则直接查db，并更新到缓存
    // 查db
	$ci->load->database ( 'default' );
	$row_user = $ci->db->select ( 'uid,uname,utype,status,email,mobile,is_active')->get_where ( 'user', array (
			'uid' => $uid 
	), 1 )->row_array ();
	if (! $row_user) {
		return FALSE;
	}
	
	// 组装用户信息
	$user = array (
			'id' => $row_user ['uid'] + 0,
			'name' => $row_user ['uname'],
			'type' => $row_user ['utype'] + 0,
			'is_active' => $row_user ['is_active'] > 0,
			'status' => 0,
			'email'=>$row_user['email'],
			'mobile'=>$row_user['mobile']
	);
	
	cache ( $cache_key_uid, $user, $cache_exipre_sec );

	// 2.3 更新在线用户的缓存（通过cookie）
    // 根据uid从缓存中获得登录用户的cookie： 缓存key为 'user_cookie_' . $uid，用来记录已登录的用户对应的cookie，在登录时写
	$user_cookie = cache ( 'user_cookie_' . $uid );
	if ($user_cookie) {
	    // 再更新cookie的缓存： 缓存key为 'user_cookie_' . $cookie
		$cache_key_cookie = 'user_cookie_' . $user_cookie;
		cache ( $cache_key_cookie, $user, $cache_exipre_sec );
	}
	
	return $user;
}

/**
 * 保存日志到数据库
 *
 * @param $message 日志信息        	
 * @param $level info（信息-默认）、debug（提示）、warn（警告）、error（错误）
 * @param $source task（定时器）、hlpay（互联支付），其它的统一记录到主表中（默认为NULL）        	
 * @param $extra 根据记录表的不同，可附加其它数据（默认为空数组）        	
 * @return 日志记录编号
 */
function db_log($message, $level = 'info', $source = NULL, $extra = array()) {
	static $level_config = array (
			'info',
			'debug',
			'warn',
			'error' 
	);
	static $source_config = array (
			'hlpay' => 'system_log_hlpay',
			'task' => 'system_tasktimer_log',
			'queue' => 'system_queue_log',
			'user' => 'user_log' 
	);
	static $_db = NULL;
    $ci = &get_instance ();
	if ($_db === NULL) {
		$_db = $ci->load->database ( 'default', TRUE );
	}

    if (defined('WRK_DIR')) { // workerman环境
	    $url = 'workerman://localhost';
	    $param = json_encode($ci->workerman_request);
    }else { // web环境
        $url = isset ($_SERVER ['SCRIPT_URI']) ? $_SERVER ['SCRIPT_URI'] : ('http://' . $_SERVER ['SERVER_NAME'] . strtok($_SERVER ['REQUEST_URI'], '?'));
        $param = curr_curl_cmd(); // 通过 php://input -- 获得请求的原始数据, 但在 enctype="multipart/form-data" 时无效
    }

    $data = array ();
	$data ['level'] = in_array ( $level, $level_config ) ? $level : 'other';
	$data ['dateline'] = time ();
	$data ['url'] = $url;
	$data ['param'] = $param ? $param : '';
	$data ['content'] = $message;
	$data ['ip_client'] = ip ( 'int', 'client' );
	$data ['ip_server'] = ip ( 'int', 'server' );
	$data = array_merge ( $data, $extra );
	
	$table = isset ( $source_config [$source] ) ? $source_config [$source] : 'system_log';
	$_db->insert ( $table, $data );
	return $_db->insert_id ();
}

/**
 * 获取couchbase操作类
 *
 * @return object
 */
function get_redis() {
	static $_cb = NULL;
	if ($_cb === NULL) {
		$CI = &get_instance ();
		$CI->load->library('Sk_redis');
		$_cb = $CI->sk_redis->connect();
        if (!($_cb instanceof Redis)) {
            show_error('redis连接错误。');
        }
	}
	
	return $_cb;
}

/**
 * 全局缓存方法 - redis
 *
 * @param string $key 缓存key值
 * @param mixed $data 默认为NULL，表示读取；若为FALSE，表示删除；其它表示设置缓存
 * @param int $expire 缓存时间。单位：秒，默认1800秒，若为0/null/false则表示永久保存。
 */
function cache($key, $data = NULL, $expire = 1800) {
	static $_cache = NULL;
	if ($_cache === NULL) {
		$_cache = get_redis ();
	}
	
	if ($data === NULL) {
		return unserialize($_cache->get ( $key ));
	}
	
	if ($data === FALSE) {
		return $_cache->del ( $key );
	}

	// redis永久缓存的$expire值为null
	$expire = intval($expire);
	if(!$expire)
		$expire = null;
	return $_cache->set ( $key, serialize($data), $expire );
}

/**
 * 读缓存，如果没有缓存，则读db并写缓存
 *
 * @param string $key 缓存key值
 * @param callable $callable 读db数据的回调
 * @param int $expire 缓存时间。单位：秒，默认1800秒，若为0则表示永久保存。
 */
function cache_or_put($key, Closure $callable, $expire = 1800){
    // 读缓存
    $val = cache($key);
    if(!$val){
        $val = $callable(); // 调用回调，来读db数据
        cache($key, $val, $expire); // 写缓存
    }
    return $val;
}

/**
 * 判断是否为get请求
 *
 * @return boolean
 */
function is_get() {
	$CI = &get_instance ();
	return $CI->input->method()=='get';
}

/**
 * 判断是否为post请求
 *
 * @return boolean
 */
function is_post() {
	$CI = &get_instance ();
	return $CI->input->method()=='post';
}

/**
 * 判断是否为ajax请求，常用于判断请求类型，输出不同类型的结果
 *
 * @return boolean
 */
function is_ajax() {
	static $_ret = NULL;
	if ($_ret === NULL) {
		$_ret = isset ( $_SERVER ['HTTP_X_REQUESTED_WITH'] ) && strtolower ( $_SERVER ['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest';
	}
	return $_ret;
}

/**
 * ajax返回：成功
 *
 * @param mixed $data 成功信息
 * @return void 输出并终止执行
 */
function ajax_success($data = NULL) {
	$ret = array (
			'success' => TRUE 
	);
	if ($data !== NULL)
		$ret ['data'] = $data;
	die ( json_encode ( $ret ) );
}

/**
 * ajax返回：错误
 *
 * @param mixed $data 错误信息
 * @return void 输出并终止执行
 */
function ajax_error($data = NULL) {
	$ret = array (
			'success' => FALSE 
	);
	if ($data !== NULL)
		$ret ['data'] = $data;
	die ( json_encode ( $ret ) );
}

/**
 * 新版ajax返回 (输出并终止执行)
 *
 * @param bool $success
 * @param string $code 错误代码
 * @param mixed $data 附加数据
 * @return void
 */
function ajax_response($success, $code = NULL, $data = NULL) {
	$success = ( bool ) $success;
	if ($data === NULL && is_array ( $code )) {
		isset ( $code ['data'] ) and $data = $code ['data'];
		isset ( $code ['code'] ) and $code = $code ['code'];
	}
	$code === NULL and $code = '';
	die ( json_encode ( compact ( 'success', 'code', 'data' ) ) );
}

if ( ! function_exists('show_error_m'))
{
	/**
	 * 错误页面展示 用于微信等移动端浏览器展示错误
	 * @param null $error
	 * @param string $page
	 */
	function show_error_m($message, $status_code = 500, $page = 'error_500_m', $heading = 'An Error Was Encountered'){
		$_error =& load_class('Exceptions', 'core');
		echo $_error->show_error($heading, $message, $page, $status_code);
		exit;
	}
}

/**
 * 上传的ajax成功响应
 * @param $plugin 上传插件类型: normal vs kindeditor
 * @param $img 图片路径
 */
function upload_ajax_success($plugin, $img){
    upload_ajax($plugin, true, $img);
}

/**
 * 上传的ajax错误响应
 * @param $plugin 上传插件类型: normal vs kindeditor
 * @param $msg 错误信息
 */
function upload_ajax_error($plugin, $msg){
    upload_ajax($plugin, false, $msg);
}

/**
 * 上传的ajax响应
 * @param $plugin 上传插件类型: normal vs kindeditor
 * @param $sucess 是否成功
 * @param $img 图片路径 或 错误消息
 */
function upload_ajax($plugin, $sucess, $img){
    // kindeditor 上传插件要固定响应
    if($plugin == 'kindeditor') {
        if($sucess) {
            // kindeditor要返回绝对地址
            if(!start_with($img, 'http'))
                $img = config_item ( 'image_servers' )[0].$img;
            die(json_encode((array("error" => 0, "url" => $img))));
        }
        die(json_encode((array("error" => 1, "message" => $img))));
    }

    if($sucess)
        ajax_success($img);
    ajax_error($img);
}

/**
 * 获取IP地址
 *
 * @param string $format 返回IP格式
 *        	string（默认）表示传统的127.0.0.1，int或其它表示转化为整型，便于存放到数据库字段
 * @param string $side IP来源
 *        	client（默认）表示客户端，server或其它表示服务端
 * @return string or int
 */
function ip($format = 'string', $side = 'client') {
    if (defined('WRK_DIR')) { // 1 workerman环境
        $ip = getenv ( 'SERVER_ADDR' );
    }else if ($side === 'client') {  // 2 web环境 - client
		static $_client_ip = NULL;
		if ($_client_ip === NULL) {
			// 获取客户端IP地址
			$ci = &get_instance ();
			$_client_ip = $ci->input->ip_address ();
		}
		$ip = $_client_ip;
	} else { // 3 web环境 - server
		static $_server_ip = NULL;
		if ($_server_ip === NULL) {
			// 获取服务器IP地址
			if (isset ( $_SERVER )) {
				if ($_SERVER ['SERVER_ADDR']) {
					$_server_ip = $_SERVER ['SERVER_ADDR'];
				} else {
					$_server_ip = $_SERVER ['LOCAL_ADDR'];
				}
			} else {
				$_server_ip = getenv ( 'SERVER_ADDR' );
			}
		}
		$ip = $_server_ip;
	}
	
	return $format === 'string' ? $ip : bindec ( decbin ( ip2long ( $ip ) ) );
}

/**
 * 获得图片服务器
 * @param $path
 * @return array
 */
if (!function_exists('get_img_server'))
{
    function get_img_server($path)
    {
        // 随机挑个图片服务器
        $ci = &get_instance();
        $servers = $ci->config->item('image_servers') or show_error('请先配置图片服务器！');
        $pos = intval(substr($path, -5, 1) )% count($servers);
        return $servers [$pos];
    }
}

/**
 * 获取上传图片链接
 *   注意: appserver有改写该方法 sk/sites/appserver.shikee.com/root/index.php
 *
 * @param
 *        	path 图片相对路径
 * @param
 *        	size 要显示的图片大小（字符串，例如'350x350'），默认为空表示原图
 * @param
 *        	dir_when_timestamp 当需要时间戳时，需要传递图片存放的根目录，用以获得时间戳，如果为false，则不需要时间戳
 * @return string
 */
function image_url($path, $size = '', $dir_when_timestamp = false)
{
    if (!$path)
        return '';
    // 1 绝对url：直接返回
    if (stripos($path, 'http://') === 0 || stripos($path, 'https://') === 0)
        return $path;

    // 2 相对url：加图片服务器域名
    // 随机挑个图片服务器
    $server = get_img_server($path);

    // 构建size
    if ($size) {
        $path = "{$path}_$size.jpg";
    }
    // 构建时间戳参数
    if ($dir_when_timestamp) {
        $path = "{$path}?ts=" . filemtime($dir_when_timestamp . $path);
    }
    return $server . $path;
}

/**
 * 获取上传图片基准URL
 *
 * @return string
 */
function image_base_url() {
    $ci = &get_instance ();
    $servers = $ci->config->item ( 'image_servers' ) or show_error ( '请先配置图片服务器！' );
    return isset($servers[0]) ? $servers[0] : '';
}


/**
 * 返回uc头像地址
 *
 * @param
 *        	uid 用户编号
 * @param
 *        	size 头像尺寸, 值为3者之一: big / middle / small
 * @param
 *        	type 类型，默认为php，表示动态地址，其它为真实物理路径（可能不存在）
 * @return string 用户头像完整链接
 */
function avatar($uid, $size = 'middle', $type = 'php') {
	/*if ($type == 'php') {
		static $uc = NULL;
		if ($uc === NULL) {
			$ci = &get_instance ();
			$uc = $ci->config->item ( 'domain_uc' );
		}
		return $uc . 'avatar.php?uid=' . $uid . '&size=' . $size;
	} else {
		$uid = str_pad ( $uid, 11, '0', STR_PAD_LEFT );
		$str1 = substr ( $uid, 0, 3 );
		$str2 = substr ( $uid, 3, 3 );
		$str3 = substr ( $uid, 6, 3 );
		$str4 = substr ( $uid, 9, 2 );
		$avatar = "http://userimg.shikee.com/data/avatar/" . $str1 . "/" . $str2 . "/" . $str3 . "/" . $str4 . "_avatar_" . $size . ".jpg";
		return $avatar;
	}*/
    $ci = &get_instance ();
    $ci->load->library ( 'sk_upload_avatar' );
    return $ci->sk_upload_avatar->avatar_url($uid, $size);
}

/**
 * 判断 $str 是否以 $substr 开头
 *
 * @param string $str 被找的字符串
 * @param string $substr 要找的子字符串
 * @return boolean
 */
function start_with($str, $substr)
{
    return strpos($str, $substr) === 0; // pos为0
}

/**
 * 判断 $str 是否以 $substr 结尾
 *
 * @param string $str 被找的字符串
 * @param string $substr 要找的子字符串
 * @return boolean
 */
function end_with($str, $substr)
{
    $end_pos = strlen($str) - strlen($substr);
    return strpos($str, $substr) === $end_pos; // pos为$end_pos
}

/**
 * 判断URL是否属于所给出的域名
 * @param string $url
 * @param string|array $domains
 * @return bool
 */
function is_url_under_domain($url, $domains){
    if (! is_array($domains)) {
        $domains = array($domains);
    }

    foreach ($domains as $domain) {
        $host = parse_url($url, PHP_URL_HOST);
        if (!$host) {
            continue;
        }
        if ($host === $domain || end_with($host, ".$domain")) {
            return true;
        }
    }
    return false;
}

/**
 * 检查URL是否可信任。登录成功跳转时使用。
 * 只有本网站内的URL才可信。
 *
 * @$url	string		要跳转的URL
 * @return 	boolean
 */
function is_url_trusted($url){
    return is_url_under_domain($url, config_item ('domain_root'));
}

/**
 * 替换字符串
 *
 * @param string $str 字符串模板，如 http://www.zdb2.com/login/sign_on?uid=:uid
 * @param array $params 参数，如 array('uid' => 1)
 * @return string 对 :xx 样式的参数进行替换，如 http://www.zdb2.com/login/sign_on?uid=1
 */
function str_replace_with_params($str, array $params)
{
    return preg_replace_callback('/:([\w\d]+)/', function($mathes) use($params) {
        return $params[$mathes[1]];
    }, $str);
}

/**
 * 为当前请求生成curl命令
 * @param string $url
 * @return string
 */
function curr_curl_cmd(){
    $curl = empty($_POST) ? 'curl　-G' : 'curl';
    $url = "http://{$_SERVER['SERVER_NAME']}{$_SERVER['REQUEST_URI']}?{$_SERVER['QUERY_STRING']}";
    $url = rtrim($url, '?');
    //$data = http_build_query($_POST);
    //$data = build_query_string($_POST);
    $data = file_get_contents('php://input', 'r'); // php://input -- 获得请求的原始数据, 但在 enctype="multipart/form-data" 时无效
    return "$curl -d '$data' '$url'";
}

/**
 * 生成curl命令
 * @param string $url
 * @param array $headers
 * @param array $data
 * @param string $method
 * @return string
 */
function build_curl_cmd($url, $headers = array(), $data = array(), $method = 'GET'){
    $curl = $method == 'GET' ? 'curl　-G' : 'curl';
    $headers = build_curl_header($headers);
    $data = '-d '.build_curl_query($data);
    return "$curl $headers $data $url";
}

/**
 * 构建query string
 * @param array $data
 * @return string
 */
function build_curl_query($data = array()){
    $str = "'";
    foreach ($data as $k => $v){
        $str .= "$k=$v&";
    }
    return rtrim($str, '&')."'";
}

/**
 * 构建header
 *
 * @param array $data
 * @return string
 */
function build_curl_header($data = array()){
    $str = "";
    foreach ($data as $k => $v){
        $str .= "-H '$k:$v' ";
    }
    return $str;
}

/**
 * 构建query string
 *    排序 + 拼接uri参数
 * @param $data
 * @param $ksort 是否按key来对参数排序
 * @return string
 */
function build_query_string(&$data, $ksort = FALSE){
    // 1 排序
    if($ksort)
        ksort($data);

    // 2 拼接uri参数
    // 会转义
    //return http_build_query($data);
    // 不转义
    $str = "";
    foreach ($data as $k => $v){
        $str .= "$k=$v&";
    }
    return rtrim($str, '&');
}

/**
 * 解码cookie
 *
 * @param $cookie cookie中的shikee字段, 类似session_id
 * @param $cache_exipre_sec 登录有效持续时间
 * @param $is_login_state 是否返回登录时间
 * @return string
 */
function decode_cookie( $cookie, $cache_exipre_sec, $is_login_return = false ){
	$cookie = explode ( '|', $cookie );
	$ci = &get_instance ();
	$ci->load->library ( 'sk_crypt', array (
			'key' => KEY_COOKIE_CRYPT,
			'iv' => KEY_COOKIE_CRYPT_IV
	) );
	if (count ( $cookie ) >= 3 && $auth = $ci->sk_crypt->decode ( $cookie [2] )) {
		$auths = explode ( '|', $auth ); // uid + 用户类型 + 登录时间
		$uid = (int) $auths [0];
		$login_time = (int) $auths [2];
		if($is_login_return) return $login_time;
		//登录时间只能在一天内有效
		$now = time();
		if($login_time < $now - $cache_exipre_sec || $login_time > $now)
			return false;
	} else {
		return FALSE;
	}

	return $uid;
}


/**
 * 将unicode码解码为中文
 * @param string $str
 * @return string
 */
function decode_unicode($str)
{
    return preg_replace_callback(
        '/\\\\u([0-9a-f]{4})/i', // unicode码
        function ($matches){
            return mb_convert_encoding(pack("H*", $matches[1]), "UTF-8", "UCS-2BE"); // 解码为中文
        },
        $str);
}

/**
 * 获得子域名
 * @return
 */
function sub_domain(){
    static $sub_domain = false;
    if($sub_domain === false) {
        $domain = $_SERVER['SERVER_NAME'];
        $sub_domain = substr($domain, 0, strpos($sub_domain, '.'));
    }
    return $sub_domain;
}

function is_win(){
    return strncasecmp ( PHP_OS, 'WIN', 3 ) == 0;
}

function long_ip($proper_address){
    return long2ip(intval($proper_address));
}
