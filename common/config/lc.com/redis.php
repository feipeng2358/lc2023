<?php
$config['default'] = array(
    'host'=> '43.136.13.254',
    'port'=>8888,
    'auth'=>NULL,    //是否有用户验证，默认无密码验证。如果不是为null，则为验证密码
    'timeout'=>0,   //连接超时,0为不超时
    'reserved'=>null,
    'retry_interval'=>100,  //单位是 ms 毫秒
    'reconnect'=>FALSE, //连接超时是否重连  默认不重连
    'pconnect' => FALSE, // 是否长连接：web是false短连接，cli是true长连接
    'prefix'=>'lc_' //键值前缀
);
